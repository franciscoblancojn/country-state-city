"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_118 = require("../country/144_afganistan/citys");
const citys_129 = require("../country/114_albania/citys");
const citys_146 = require("../country/18_alemania/citys");
const citys_148 = require("../country/98_argelia/citys");
const citys_150 = require("../country/145_andorra/citys");
const citys_153 = require("../country/119_angola/citys");
const citys_160 = require("../country/4_anguila/citys");
const citys_161 = require("../country/147_antigua_y_barbuda/citys");
const citys_162 = require("../country/207_antillas_holandesas/citys");
const citys_163 = require("../country/91_arabia_saudita/citys");
const citys_167 = require("../country/5_argentina/citys");
const citys_170 = require("../country/6_armenia/citys");
const citys_174 = require("../country/142_aruba/citys");
const citys_177 = require("../country/1_australia/citys");
const citys_179 = require("../country/2_austria/citys");
const citys_182 = require("../country/3_azerbaiyan/citys");
const citys_183 = require("../country/80_bahamas/citys");
const citys_190 = require("../country/127_barein/citys");
const citys_191 = require("../country/149_bangladesh/citys");
const citys_192 = require("../country/128_barbados/citys");
const citys_194 = require("../country/9_belgica/citys");
const citys_199 = require("../country/8_belice/citys");
const citys_203 = require("../country/151_benin/citys");
const citys_204 = require("../country/10_bermudas/citys");
const citys_211 = require("../country/7_bielorrusia/citys");
const citys_214 = require("../country/123_bolivia/citys");
const citys_230 = require("../country/79_bosnia_y_herzegovina/citys");
const citys_231 = require("../country/100_botsuana/citys");
const citys_233 = require("../country/12_brasil/citys");
const citys_238 = require("../country/155_brunei/citys");
const citys_244 = require("../country/11_bulgaria/citys");
const citys_245 = require("../country/156_burkina_faso/citys");
const citys_247 = require("../country/157_burundi/citys");
const citys_248 = require("../country/152_butan/citys");
const citys_249 = require("../country/159_cabo_verde/citys");
const citys_250 = require("../country/158_camboya/citys");
const citys_251 = require("../country/31_camerun/citys");
const citys_252 = require("../country/32_canada/citys");
const citys_253 = require("../country/130_chad/citys");
const citys_254 = require("../country/81_chile/citys");
const citys_255 = require("../country/35_china/citys");
const citys_256 = require("../country/33_chipre/citys");
const citys_257 = require("../country/82_colombia/citys");
const citys_258 = require("../country/164_comores/citys");
const citys_259 = require("../country/112_congo_(brazzaville)/citys");
const citys_260 = require("../country/165_congo_(kinshasa)/citys");
const citys_261 = require("../country/166_cook_islas/citys");
const citys_262 = require("../country/84_corea_del_norte/citys");
const citys_263 = require("../country/69_corea_del_sur/citys");
const citys_264 = require("../country/168_costa_de_marfil/citys");
const citys_265 = require("../country/36_costa_rica/citys");
const citys_266 = require("../country/71_croacia/citys");
const citys_267 = require("../country/113_cuba/citys");
const citys_268 = require("../country/22_dinamarca/citys");
const citys_269 = require("../country/169_djibouti_yibuti/citys");
const citys_270 = require("../country/103_ecuador/citys");
const citys_271 = require("../country/23_egipto/citys");
const citys_272 = require("../country/51_el_salvador/citys");
const citys_273 = require("../country/93_emiratos_arabes_unidos/citys");
const citys_274 = require("../country/173_eritrea/citys");
const citys_275 = require("../country/52_eslovaquia/citys");
const citys_276 = require("../country/53_eslovenia/citys");
const citys_277 = require("../country/28_espana/citys");
const citys_278 = require("../country/55_estados_unidos/citys");
const citys_279 = require("../country/68_estonia/citys");
const citys_280 = require("../country/121_etiopia/citys");
const citys_281 = require("../country/175_feroe_islas/citys");
const citys_282 = require("../country/90_filipinas/citys");
const citys_283 = require("../country/63_finlandia/citys");
const citys_284 = require("../country/176_fiyi/citys");
const citys_285 = require("../country/64_francia/citys");
const citys_286 = require("../country/180_gabon/citys");
const citys_287 = require("../country/181_gambia/citys");
const citys_288 = require("../country/21_georgia/citys");
const citys_289 = require("../country/105_ghana/citys");
const citys_290 = require("../country/143_gibraltar/citys");
const citys_291 = require("../country/184_granada/citys");
const citys_292 = require("../country/20_grecia/citys");
const citys_293 = require("../country/94_groenlandia/citys");
const citys_294 = require("../country/17_guadalupe/citys");
const citys_295 = require("../country/185_guatemala/citys");
const citys_296 = require("../country/186_guernsey/citys");
const citys_297 = require("../country/187_guinea/citys");
const citys_298 = require("../country/172_guinea_ecuatorial/citys");
const citys_299 = require("../country/188_guinea-bissau/citys");
const citys_300 = require("../country/189_guyana/citys");
const citys_301 = require("../country/16_haiti/citys");
const citys_302 = require("../country/137_honduras/citys");
const citys_303 = require("../country/73_hong_kong/citys");
const citys_304 = require("../country/14_hungria/citys");
const citys_305 = require("../country/25_india/citys");
const citys_306 = require("../country/74_indonesia/citys");
const citys_307 = require("../country/140_irak/citys");
const citys_308 = require("../country/26_iran/citys");
const citys_309 = require("../country/27_irlanda/citys");
const citys_310 = require("../country/215_isla_pitcairn/citys");
const citys_311 = require("../country/83_islandia/citys");
const citys_312 = require("../country/228_islas_salomon/citys");
const citys_313 = require("../country/58_islas_turcas_y_caicos/citys");
const citys_314 = require("../country/154_islas_virgenes_britanicas/citys");
const citys_315 = require("../country/24_israel/citys");
const citys_316 = require("../country/29_italia/citys");
const citys_317 = require("../country/132_jamaica/citys");
const citys_318 = require("../country/70_japon/citys");
const citys_319 = require("../country/193_jersey/citys");
const citys_320 = require("../country/75_jordania/citys");
const citys_321 = require("../country/30_kazajstan/citys");
const citys_322 = require("../country/97_kenia/citys");
const citys_323 = require("../country/34_kirguistan/citys");
const citys_324 = require("../country/195_kiribati/citys");
const citys_325 = require("../country/37_kuwait/citys");
const citys_326 = require("../country/196_laos/citys");
const citys_327 = require("../country/197_lesotho/citys");
const citys_328 = require("../country/38_letonia/citys");
const citys_329 = require("../country/99_libano/citys");
const citys_330 = require("../country/198_liberia/citys");
const citys_331 = require("../country/39_libia/citys");
const citys_332 = require("../country/126_liechtenstein/citys");
const citys_333 = require("../country/40_lituania/citys");
const citys_334 = require("../country/41_luxemburgo/citys");
const citys_335 = require("../country/85_macedonia/citys");
const citys_336 = require("../country/134_madagascar/citys");
const citys_337 = require("../country/76_malasia/citys");
const citys_338 = require("../country/125_malaui/citys");
const citys_339 = require("../country/200_maldivas/citys");
const citys_340 = require("../country/133_mali/citys");
const citys_341 = require("../country/86_malta/citys");
const citys_342 = require("../country/131_isla_de_man/citys");
const citys_343 = require("../country/104_marruecos/citys");
const citys_344 = require("../country/201_martinica/citys");
const citys_345 = require("../country/202_mauricio/citys");
const citys_346 = require("../country/108_mauritania/citys");
const citys_347 = require("../country/42_mexico/citys");
const citys_348 = require("../country/43_moldavia/citys");
const citys_349 = require("../country/44_monaco/citys");
const citys_350 = require("../country/139_mongolia/citys");
const citys_351 = require("../country/117_mozambique/citys");
const citys_352 = require("../country/205_myanmar/citys");
const citys_353 = require("../country/102_namibia/citys");
const citys_354 = require("../country/206_nauru/citys");
const citys_355 = require("../country/107_nepal/citys");
const citys_356 = require("../country/209_nicaragua/citys");
const citys_357 = require("../country/210_niger/citys");
const citys_358 = require("../country/115_nigeria/citys");
const citys_359 = require("../country/212_norfolk_island/citys");
const citys_360 = require("../country/46_noruega/citys");
const citys_361 = require("../country/208_nueva_caledonia/citys");
const citys_362 = require("../country/45_nueva_zelanda/citys");
const citys_363 = require("../country/213_oman/citys");
const citys_364 = require("../country/19_paises_bajos_holanda/citys");
const citys_365 = require("../country/87_pakistan/citys");
const citys_366 = require("../country/124_panama/citys");
const citys_367 = require("../country/88_papua-nueva_guinea/citys");
const citys_368 = require("../country/110_paraguay/citys");
const citys_369 = require("../country/89_peru/citys");
const citys_370 = require("../country/178_polinesia_francesa/citys");
const citys_371 = require("../country/47_polonia/citys");
const citys_372 = require("../country/48_portugal/citys");
const citys_373 = require("../country/246_puerto_rico/citys");
const citys_374 = require("../country/216_qatar/citys");
const citys_375 = require("../country/13_reino_unido/citys");
const citys_376 = require("../country/65_republica_checa/citys");
const citys_377 = require("../country/138_republica_dominicana/citys");
const citys_378 = require("../country/49_reunion/citys");
const citys_379 = require("../country/217_ruanda/citys");
const citys_380 = require("../country/72_rumania/citys");
const citys_381 = require("../country/50_rusia/citys");
const citys_382 = require("../country/242_sahara_occidental/citys");
const citys_383 = require("../country/223_samoa/citys");
const citys_384 = require("../country/219_san_cristobal_y_nieves/citys");
const citys_385 = require("../country/224_san_marino/citys");
const citys_386 = require("../country/221_san_pedro_y_miquelon/citys");
const citys_387 = require("../country/225_san_tome_y_principe/citys");
const citys_388 = require("../country/222_san_vicente_y_las_granadinas/citys");
const citys_389 = require("../country/218_santa_elena/citys");
const citys_390 = require("../country/220_santa_lucia/citys");
const citys_391 = require("../country/135_senegal/citys");
const citys_392 = require("../country/226_serbia_y_montenegro/citys");
const citys_393 = require("../country/109_seychelles/citys");
const citys_394 = require("../country/227_sierra_leona/citys");
const citys_395 = require("../country/77_singapur/citys");
const citys_396 = require("../country/106_siria/citys");
const citys_397 = require("../country/229_somalia/citys");
const citys_398 = require("../country/120_sri_lanka/citys");
const citys_399 = require("../country/141_sudafrica/citys");
const citys_400 = require("../country/232_sudan/citys");
const citys_401 = require("../country/67_suecia/citys");
const citys_402 = require("../country/66_suiza/citys");
const citys_403 = require("../country/54_surinam/citys");
const citys_404 = require("../country/234_suazilandia/citys");
const citys_405 = require("../country/56_tayikistan/citys");
const citys_406 = require("../country/92_tailandia/citys");
const citys_407 = require("../country/78_taiwan/citys");
const citys_408 = require("../country/101_tanzania/citys");
const citys_409 = require("../country/171_timor_oriental/citys");
const citys_410 = require("../country/136_togo/citys");
const citys_411 = require("../country/235_tokelau/citys");
const citys_412 = require("../country/236_tonga/citys");
const citys_413 = require("../country/237_trinidad_y_tobago/citys");
const citys_414 = require("../country/122_tunez/citys");
const citys_415 = require("../country/57_turkmenistan/citys");
const citys_416 = require("../country/59_turquia/citys");
const citys_417 = require("../country/239_tuvalu/citys");
const citys_418 = require("../country/62_ucrania/citys");
const citys_419 = require("../country/60_uganda/citys");
const citys_420 = require("../country/111_uruguay/citys");
const citys_421 = require("../country/61_uzbekistan/citys");
const citys_422 = require("../country/240_vanuatu/citys");
const citys_423 = require("../country/95_venezuela/citys");
const citys_424 = require("../country/15_vietnam/citys");
const citys_425 = require("../country/241_wallis_y_futuna/citys");
const citys_426 = require("../country/243_yemen/citys");
const citys_427 = require("../country/116_zambia/citys");
const citys_428 = require("../country/96_zimbabue/citys");
exports.citys = [
    ...citys_118.citys,
    ...citys_129.citys,
    ...citys_146.citys,
    ...citys_148.citys,
    ...citys_150.citys,
    ...citys_153.citys,
    ...citys_160.citys,
    ...citys_161.citys,
    ...citys_162.citys,
    ...citys_163.citys,
    ...citys_167.citys,
    ...citys_170.citys,
    ...citys_174.citys,
    ...citys_177.citys,
    ...citys_179.citys,
    ...citys_182.citys,
    ...citys_183.citys,
    ...citys_190.citys,
    ...citys_191.citys,
    ...citys_192.citys,
    ...citys_194.citys,
    ...citys_199.citys,
    ...citys_203.citys,
    ...citys_204.citys,
    ...citys_211.citys,
    ...citys_214.citys,
    ...citys_230.citys,
    ...citys_231.citys,
    ...citys_233.citys,
    ...citys_238.citys,
    ...citys_244.citys,
    ...citys_245.citys,
    ...citys_247.citys,
    ...citys_248.citys,
    ...citys_249.citys,
    ...citys_250.citys,
    ...citys_251.citys,
    ...citys_252.citys,
    ...citys_253.citys,
    ...citys_254.citys,
    ...citys_255.citys,
    ...citys_256.citys,
    ...citys_257.citys,
    ...citys_258.citys,
    ...citys_259.citys,
    ...citys_260.citys,
    ...citys_261.citys,
    ...citys_262.citys,
    ...citys_263.citys,
    ...citys_264.citys,
    ...citys_265.citys,
    ...citys_266.citys,
    ...citys_267.citys,
    ...citys_268.citys,
    ...citys_269.citys,
    ...citys_270.citys,
    ...citys_271.citys,
    ...citys_272.citys,
    ...citys_273.citys,
    ...citys_274.citys,
    ...citys_275.citys,
    ...citys_276.citys,
    ...citys_277.citys,
    ...citys_278.citys,
    ...citys_279.citys,
    ...citys_280.citys,
    ...citys_281.citys,
    ...citys_282.citys,
    ...citys_283.citys,
    ...citys_284.citys,
    ...citys_285.citys,
    ...citys_286.citys,
    ...citys_287.citys,
    ...citys_288.citys,
    ...citys_289.citys,
    ...citys_290.citys,
    ...citys_291.citys,
    ...citys_292.citys,
    ...citys_293.citys,
    ...citys_294.citys,
    ...citys_295.citys,
    ...citys_296.citys,
    ...citys_297.citys,
    ...citys_298.citys,
    ...citys_299.citys,
    ...citys_300.citys,
    ...citys_301.citys,
    ...citys_302.citys,
    ...citys_303.citys,
    ...citys_304.citys,
    ...citys_305.citys,
    ...citys_306.citys,
    ...citys_307.citys,
    ...citys_308.citys,
    ...citys_309.citys,
    ...citys_310.citys,
    ...citys_311.citys,
    ...citys_312.citys,
    ...citys_313.citys,
    ...citys_314.citys,
    ...citys_315.citys,
    ...citys_316.citys,
    ...citys_317.citys,
    ...citys_318.citys,
    ...citys_319.citys,
    ...citys_320.citys,
    ...citys_321.citys,
    ...citys_322.citys,
    ...citys_323.citys,
    ...citys_324.citys,
    ...citys_325.citys,
    ...citys_326.citys,
    ...citys_327.citys,
    ...citys_328.citys,
    ...citys_329.citys,
    ...citys_330.citys,
    ...citys_331.citys,
    ...citys_332.citys,
    ...citys_333.citys,
    ...citys_334.citys,
    ...citys_335.citys,
    ...citys_336.citys,
    ...citys_337.citys,
    ...citys_338.citys,
    ...citys_339.citys,
    ...citys_340.citys,
    ...citys_341.citys,
    ...citys_342.citys,
    ...citys_343.citys,
    ...citys_344.citys,
    ...citys_345.citys,
    ...citys_346.citys,
    ...citys_347.citys,
    ...citys_348.citys,
    ...citys_349.citys,
    ...citys_350.citys,
    ...citys_351.citys,
    ...citys_352.citys,
    ...citys_353.citys,
    ...citys_354.citys,
    ...citys_355.citys,
    ...citys_356.citys,
    ...citys_357.citys,
    ...citys_358.citys,
    ...citys_359.citys,
    ...citys_360.citys,
    ...citys_361.citys,
    ...citys_362.citys,
    ...citys_363.citys,
    ...citys_364.citys,
    ...citys_365.citys,
    ...citys_366.citys,
    ...citys_367.citys,
    ...citys_368.citys,
    ...citys_369.citys,
    ...citys_370.citys,
    ...citys_371.citys,
    ...citys_372.citys,
    ...citys_373.citys,
    ...citys_374.citys,
    ...citys_375.citys,
    ...citys_376.citys,
    ...citys_377.citys,
    ...citys_378.citys,
    ...citys_379.citys,
    ...citys_380.citys,
    ...citys_381.citys,
    ...citys_382.citys,
    ...citys_383.citys,
    ...citys_384.citys,
    ...citys_385.citys,
    ...citys_386.citys,
    ...citys_387.citys,
    ...citys_388.citys,
    ...citys_389.citys,
    ...citys_390.citys,
    ...citys_391.citys,
    ...citys_392.citys,
    ...citys_393.citys,
    ...citys_394.citys,
    ...citys_395.citys,
    ...citys_396.citys,
    ...citys_397.citys,
    ...citys_398.citys,
    ...citys_399.citys,
    ...citys_400.citys,
    ...citys_401.citys,
    ...citys_402.citys,
    ...citys_403.citys,
    ...citys_404.citys,
    ...citys_405.citys,
    ...citys_406.citys,
    ...citys_407.citys,
    ...citys_408.citys,
    ...citys_409.citys,
    ...citys_410.citys,
    ...citys_411.citys,
    ...citys_412.citys,
    ...citys_413.citys,
    ...citys_414.citys,
    ...citys_415.citys,
    ...citys_416.citys,
    ...citys_417.citys,
    ...citys_418.citys,
    ...citys_419.citys,
    ...citys_420.citys,
    ...citys_421.citys,
    ...citys_422.citys,
    ...citys_423.citys,
    ...citys_424.citys,
    ...citys_425.citys,
    ...citys_426.citys,
    ...citys_427.citys,
    ...citys_428.citys,
];
//# sourceMappingURL=index.js.map