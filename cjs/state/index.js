"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.states = void 0;
const states_118 = require("../country/144_afganistan/states");
const states_129 = require("../country/114_albania/states");
const states_146 = require("../country/18_alemania/states");
const states_148 = require("../country/98_argelia/states");
const states_150 = require("../country/145_andorra/states");
const states_153 = require("../country/119_angola/states");
const states_160 = require("../country/4_anguila/states");
const states_161 = require("../country/147_antigua_y_barbuda/states");
const states_162 = require("../country/207_antillas_holandesas/states");
const states_163 = require("../country/91_arabia_saudita/states");
const states_167 = require("../country/5_argentina/states");
const states_170 = require("../country/6_armenia/states");
const states_174 = require("../country/142_aruba/states");
const states_177 = require("../country/1_australia/states");
const states_179 = require("../country/2_austria/states");
const states_182 = require("../country/3_azerbaiyan/states");
const states_183 = require("../country/80_bahamas/states");
const states_190 = require("../country/127_barein/states");
const states_191 = require("../country/149_bangladesh/states");
const states_192 = require("../country/128_barbados/states");
const states_194 = require("../country/9_belgica/states");
const states_199 = require("../country/8_belice/states");
const states_203 = require("../country/151_benin/states");
const states_204 = require("../country/10_bermudas/states");
const states_211 = require("../country/7_bielorrusia/states");
const states_214 = require("../country/123_bolivia/states");
const states_230 = require("../country/79_bosnia_y_herzegovina/states");
const states_231 = require("../country/100_botsuana/states");
const states_233 = require("../country/12_brasil/states");
const states_238 = require("../country/155_brunei/states");
const states_244 = require("../country/11_bulgaria/states");
const states_245 = require("../country/156_burkina_faso/states");
const states_247 = require("../country/157_burundi/states");
const states_248 = require("../country/152_butan/states");
const states_249 = require("../country/159_cabo_verde/states");
const states_250 = require("../country/158_camboya/states");
const states_251 = require("../country/31_camerun/states");
const states_252 = require("../country/32_canada/states");
const states_253 = require("../country/130_chad/states");
const states_254 = require("../country/81_chile/states");
const states_255 = require("../country/35_china/states");
const states_256 = require("../country/33_chipre/states");
const states_257 = require("../country/82_colombia/states");
const states_258 = require("../country/164_comores/states");
const states_259 = require("../country/112_congo_(brazzaville)/states");
const states_260 = require("../country/165_congo_(kinshasa)/states");
const states_261 = require("../country/166_cook_islas/states");
const states_262 = require("../country/84_corea_del_norte/states");
const states_263 = require("../country/69_corea_del_sur/states");
const states_264 = require("../country/168_costa_de_marfil/states");
const states_265 = require("../country/36_costa_rica/states");
const states_266 = require("../country/71_croacia/states");
const states_267 = require("../country/113_cuba/states");
const states_268 = require("../country/22_dinamarca/states");
const states_269 = require("../country/169_djibouti_yibuti/states");
const states_270 = require("../country/103_ecuador/states");
const states_271 = require("../country/23_egipto/states");
const states_272 = require("../country/51_el_salvador/states");
const states_273 = require("../country/93_emiratos_arabes_unidos/states");
const states_274 = require("../country/173_eritrea/states");
const states_275 = require("../country/52_eslovaquia/states");
const states_276 = require("../country/53_eslovenia/states");
const states_277 = require("../country/28_espana/states");
const states_278 = require("../country/55_estados_unidos/states");
const states_279 = require("../country/68_estonia/states");
const states_280 = require("../country/121_etiopia/states");
const states_281 = require("../country/175_feroe_islas/states");
const states_282 = require("../country/90_filipinas/states");
const states_283 = require("../country/63_finlandia/states");
const states_284 = require("../country/176_fiyi/states");
const states_285 = require("../country/64_francia/states");
const states_286 = require("../country/180_gabon/states");
const states_287 = require("../country/181_gambia/states");
const states_288 = require("../country/21_georgia/states");
const states_289 = require("../country/105_ghana/states");
const states_290 = require("../country/143_gibraltar/states");
const states_291 = require("../country/184_granada/states");
const states_292 = require("../country/20_grecia/states");
const states_293 = require("../country/94_groenlandia/states");
const states_294 = require("../country/17_guadalupe/states");
const states_295 = require("../country/185_guatemala/states");
const states_296 = require("../country/186_guernsey/states");
const states_297 = require("../country/187_guinea/states");
const states_298 = require("../country/172_guinea_ecuatorial/states");
const states_299 = require("../country/188_guinea-bissau/states");
const states_300 = require("../country/189_guyana/states");
const states_301 = require("../country/16_haiti/states");
const states_302 = require("../country/137_honduras/states");
const states_303 = require("../country/73_hong_kong/states");
const states_304 = require("../country/14_hungria/states");
const states_305 = require("../country/25_india/states");
const states_306 = require("../country/74_indonesia/states");
const states_307 = require("../country/140_irak/states");
const states_308 = require("../country/26_iran/states");
const states_309 = require("../country/27_irlanda/states");
const states_310 = require("../country/215_isla_pitcairn/states");
const states_311 = require("../country/83_islandia/states");
const states_312 = require("../country/228_islas_salomon/states");
const states_313 = require("../country/58_islas_turcas_y_caicos/states");
const states_314 = require("../country/154_islas_virgenes_britanicas/states");
const states_315 = require("../country/24_israel/states");
const states_316 = require("../country/29_italia/states");
const states_317 = require("../country/132_jamaica/states");
const states_318 = require("../country/70_japon/states");
const states_319 = require("../country/193_jersey/states");
const states_320 = require("../country/75_jordania/states");
const states_321 = require("../country/30_kazajstan/states");
const states_322 = require("../country/97_kenia/states");
const states_323 = require("../country/34_kirguistan/states");
const states_324 = require("../country/195_kiribati/states");
const states_325 = require("../country/37_kuwait/states");
const states_326 = require("../country/196_laos/states");
const states_327 = require("../country/197_lesotho/states");
const states_328 = require("../country/38_letonia/states");
const states_329 = require("../country/99_libano/states");
const states_330 = require("../country/198_liberia/states");
const states_331 = require("../country/39_libia/states");
const states_332 = require("../country/126_liechtenstein/states");
const states_333 = require("../country/40_lituania/states");
const states_334 = require("../country/41_luxemburgo/states");
const states_335 = require("../country/85_macedonia/states");
const states_336 = require("../country/134_madagascar/states");
const states_337 = require("../country/76_malasia/states");
const states_338 = require("../country/125_malaui/states");
const states_339 = require("../country/200_maldivas/states");
const states_340 = require("../country/133_mali/states");
const states_341 = require("../country/86_malta/states");
const states_342 = require("../country/131_isla_de_man/states");
const states_343 = require("../country/104_marruecos/states");
const states_344 = require("../country/201_martinica/states");
const states_345 = require("../country/202_mauricio/states");
const states_346 = require("../country/108_mauritania/states");
const states_347 = require("../country/42_mexico/states");
const states_348 = require("../country/43_moldavia/states");
const states_349 = require("../country/44_monaco/states");
const states_350 = require("../country/139_mongolia/states");
const states_351 = require("../country/117_mozambique/states");
const states_352 = require("../country/205_myanmar/states");
const states_353 = require("../country/102_namibia/states");
const states_354 = require("../country/206_nauru/states");
const states_355 = require("../country/107_nepal/states");
const states_356 = require("../country/209_nicaragua/states");
const states_357 = require("../country/210_niger/states");
const states_358 = require("../country/115_nigeria/states");
const states_359 = require("../country/212_norfolk_island/states");
const states_360 = require("../country/46_noruega/states");
const states_361 = require("../country/208_nueva_caledonia/states");
const states_362 = require("../country/45_nueva_zelanda/states");
const states_363 = require("../country/213_oman/states");
const states_364 = require("../country/19_paises_bajos_holanda/states");
const states_365 = require("../country/87_pakistan/states");
const states_366 = require("../country/124_panama/states");
const states_367 = require("../country/88_papua-nueva_guinea/states");
const states_368 = require("../country/110_paraguay/states");
const states_369 = require("../country/89_peru/states");
const states_370 = require("../country/178_polinesia_francesa/states");
const states_371 = require("../country/47_polonia/states");
const states_372 = require("../country/48_portugal/states");
const states_373 = require("../country/246_puerto_rico/states");
const states_374 = require("../country/216_qatar/states");
const states_375 = require("../country/13_reino_unido/states");
const states_376 = require("../country/65_republica_checa/states");
const states_377 = require("../country/138_republica_dominicana/states");
const states_378 = require("../country/49_reunion/states");
const states_379 = require("../country/217_ruanda/states");
const states_380 = require("../country/72_rumania/states");
const states_381 = require("../country/50_rusia/states");
const states_382 = require("../country/242_sahara_occidental/states");
const states_383 = require("../country/223_samoa/states");
const states_384 = require("../country/219_san_cristobal_y_nieves/states");
const states_385 = require("../country/224_san_marino/states");
const states_386 = require("../country/221_san_pedro_y_miquelon/states");
const states_387 = require("../country/225_san_tome_y_principe/states");
const states_388 = require("../country/222_san_vicente_y_las_granadinas/states");
const states_389 = require("../country/218_santa_elena/states");
const states_390 = require("../country/220_santa_lucia/states");
const states_391 = require("../country/135_senegal/states");
const states_392 = require("../country/226_serbia_y_montenegro/states");
const states_393 = require("../country/109_seychelles/states");
const states_394 = require("../country/227_sierra_leona/states");
const states_395 = require("../country/77_singapur/states");
const states_396 = require("../country/106_siria/states");
const states_397 = require("../country/229_somalia/states");
const states_398 = require("../country/120_sri_lanka/states");
const states_399 = require("../country/141_sudafrica/states");
const states_400 = require("../country/232_sudan/states");
const states_401 = require("../country/67_suecia/states");
const states_402 = require("../country/66_suiza/states");
const states_403 = require("../country/54_surinam/states");
const states_404 = require("../country/234_suazilandia/states");
const states_405 = require("../country/56_tayikistan/states");
const states_406 = require("../country/92_tailandia/states");
const states_407 = require("../country/78_taiwan/states");
const states_408 = require("../country/101_tanzania/states");
const states_409 = require("../country/171_timor_oriental/states");
const states_410 = require("../country/136_togo/states");
const states_411 = require("../country/235_tokelau/states");
const states_412 = require("../country/236_tonga/states");
const states_413 = require("../country/237_trinidad_y_tobago/states");
const states_414 = require("../country/122_tunez/states");
const states_415 = require("../country/57_turkmenistan/states");
const states_416 = require("../country/59_turquia/states");
const states_417 = require("../country/239_tuvalu/states");
const states_418 = require("../country/62_ucrania/states");
const states_419 = require("../country/60_uganda/states");
const states_420 = require("../country/111_uruguay/states");
const states_421 = require("../country/61_uzbekistan/states");
const states_422 = require("../country/240_vanuatu/states");
const states_423 = require("../country/95_venezuela/states");
const states_424 = require("../country/15_vietnam/states");
const states_425 = require("../country/241_wallis_y_futuna/states");
const states_426 = require("../country/243_yemen/states");
const states_427 = require("../country/116_zambia/states");
const states_428 = require("../country/96_zimbabue/states");
exports.states = [
    ...states_118.states,
    ...states_129.states,
    ...states_146.states,
    ...states_148.states,
    ...states_150.states,
    ...states_153.states,
    ...states_160.states,
    ...states_161.states,
    ...states_162.states,
    ...states_163.states,
    ...states_167.states,
    ...states_170.states,
    ...states_174.states,
    ...states_177.states,
    ...states_179.states,
    ...states_182.states,
    ...states_183.states,
    ...states_190.states,
    ...states_191.states,
    ...states_192.states,
    ...states_194.states,
    ...states_199.states,
    ...states_203.states,
    ...states_204.states,
    ...states_211.states,
    ...states_214.states,
    ...states_230.states,
    ...states_231.states,
    ...states_233.states,
    ...states_238.states,
    ...states_244.states,
    ...states_245.states,
    ...states_247.states,
    ...states_248.states,
    ...states_249.states,
    ...states_250.states,
    ...states_251.states,
    ...states_252.states,
    ...states_253.states,
    ...states_254.states,
    ...states_255.states,
    ...states_256.states,
    ...states_257.states,
    ...states_258.states,
    ...states_259.states,
    ...states_260.states,
    ...states_261.states,
    ...states_262.states,
    ...states_263.states,
    ...states_264.states,
    ...states_265.states,
    ...states_266.states,
    ...states_267.states,
    ...states_268.states,
    ...states_269.states,
    ...states_270.states,
    ...states_271.states,
    ...states_272.states,
    ...states_273.states,
    ...states_274.states,
    ...states_275.states,
    ...states_276.states,
    ...states_277.states,
    ...states_278.states,
    ...states_279.states,
    ...states_280.states,
    ...states_281.states,
    ...states_282.states,
    ...states_283.states,
    ...states_284.states,
    ...states_285.states,
    ...states_286.states,
    ...states_287.states,
    ...states_288.states,
    ...states_289.states,
    ...states_290.states,
    ...states_291.states,
    ...states_292.states,
    ...states_293.states,
    ...states_294.states,
    ...states_295.states,
    ...states_296.states,
    ...states_297.states,
    ...states_298.states,
    ...states_299.states,
    ...states_300.states,
    ...states_301.states,
    ...states_302.states,
    ...states_303.states,
    ...states_304.states,
    ...states_305.states,
    ...states_306.states,
    ...states_307.states,
    ...states_308.states,
    ...states_309.states,
    ...states_310.states,
    ...states_311.states,
    ...states_312.states,
    ...states_313.states,
    ...states_314.states,
    ...states_315.states,
    ...states_316.states,
    ...states_317.states,
    ...states_318.states,
    ...states_319.states,
    ...states_320.states,
    ...states_321.states,
    ...states_322.states,
    ...states_323.states,
    ...states_324.states,
    ...states_325.states,
    ...states_326.states,
    ...states_327.states,
    ...states_328.states,
    ...states_329.states,
    ...states_330.states,
    ...states_331.states,
    ...states_332.states,
    ...states_333.states,
    ...states_334.states,
    ...states_335.states,
    ...states_336.states,
    ...states_337.states,
    ...states_338.states,
    ...states_339.states,
    ...states_340.states,
    ...states_341.states,
    ...states_342.states,
    ...states_343.states,
    ...states_344.states,
    ...states_345.states,
    ...states_346.states,
    ...states_347.states,
    ...states_348.states,
    ...states_349.states,
    ...states_350.states,
    ...states_351.states,
    ...states_352.states,
    ...states_353.states,
    ...states_354.states,
    ...states_355.states,
    ...states_356.states,
    ...states_357.states,
    ...states_358.states,
    ...states_359.states,
    ...states_360.states,
    ...states_361.states,
    ...states_362.states,
    ...states_363.states,
    ...states_364.states,
    ...states_365.states,
    ...states_366.states,
    ...states_367.states,
    ...states_368.states,
    ...states_369.states,
    ...states_370.states,
    ...states_371.states,
    ...states_372.states,
    ...states_373.states,
    ...states_374.states,
    ...states_375.states,
    ...states_376.states,
    ...states_377.states,
    ...states_378.states,
    ...states_379.states,
    ...states_380.states,
    ...states_381.states,
    ...states_382.states,
    ...states_383.states,
    ...states_384.states,
    ...states_385.states,
    ...states_386.states,
    ...states_387.states,
    ...states_388.states,
    ...states_389.states,
    ...states_390.states,
    ...states_391.states,
    ...states_392.states,
    ...states_393.states,
    ...states_394.states,
    ...states_395.states,
    ...states_396.states,
    ...states_397.states,
    ...states_398.states,
    ...states_399.states,
    ...states_400.states,
    ...states_401.states,
    ...states_402.states,
    ...states_403.states,
    ...states_404.states,
    ...states_405.states,
    ...states_406.states,
    ...states_407.states,
    ...states_408.states,
    ...states_409.states,
    ...states_410.states,
    ...states_411.states,
    ...states_412.states,
    ...states_413.states,
    ...states_414.states,
    ...states_415.states,
    ...states_416.states,
    ...states_417.states,
    ...states_418.states,
    ...states_419.states,
    ...states_420.states,
    ...states_421.states,
    ...states_422.states,
    ...states_423.states,
    ...states_424.states,
    ...states_425.states,
    ...states_426.states,
    ...states_427.states,
    ...states_428.states,
];
//# sourceMappingURL=index.js.map