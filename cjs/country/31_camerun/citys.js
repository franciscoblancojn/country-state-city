"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./98_littoral/citys");
const citys_2 = require("./99_southwest_region/citys");
const citys_3 = require("./100_north/citys");
const citys_4 = require("./101_central/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
];
//# sourceMappingURL=citys.js.map