"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./609_las_palmas/citys");
const citys_2 = require("./610_soria/citys");
const citys_3 = require("./611_palencia/citys");
const citys_4 = require("./612_zamora/citys");
const citys_5 = require("./613_cadiz/citys");
const citys_6 = require("./614_navarra/citys");
const citys_7 = require("./615_ourense/citys");
const citys_8 = require("./616_segovia/citys");
const citys_9 = require("./617_guipuzcoa/citys");
const citys_10 = require("./618_ciudad_real/citys");
const citys_11 = require("./619_vizcaya/citys");
const citys_12 = require("./620_alava/citys");
const citys_13 = require("./621_a_coruna/citys");
const citys_14 = require("./622_cantabria/citys");
const citys_15 = require("./623_almeria/citys");
const citys_16 = require("./624_zaragoza/citys");
const citys_17 = require("./625_santa_cruz_de_tenerife/citys");
const citys_18 = require("./626_caceres/citys");
const citys_19 = require("./627_guadalajara/citys");
const citys_20 = require("./628_avila/citys");
const citys_21 = require("./629_toledo/citys");
const citys_22 = require("./630_castellon/citys");
const citys_23 = require("./631_tarragona/citys");
const citys_24 = require("./632_lugo/citys");
const citys_25 = require("./633_la_rioja/citys");
const citys_26 = require("./634_ceuta/citys");
const citys_27 = require("./635_murcia/citys");
const citys_28 = require("./636_salamanca/citys");
const citys_29 = require("./637_valladolid/citys");
const citys_30 = require("./638_jaen/citys");
const citys_31 = require("./639_girona/citys");
const citys_32 = require("./640_granada/citys");
const citys_33 = require("./641_alacant/citys");
const citys_34 = require("./642_cordoba/citys");
const citys_35 = require("./643_albacete/citys");
const citys_36 = require("./644_cuenca/citys");
const citys_37 = require("./645_pontevedra/citys");
const citys_38 = require("./646_teruel/citys");
const citys_39 = require("./647_melilla/citys");
const citys_40 = require("./648_barcelona/citys");
const citys_41 = require("./649_badajoz/citys");
const citys_42 = require("./650_madrid/citys");
const citys_43 = require("./651_sevilla/citys");
const citys_44 = require("./652_valencia/citys");
const citys_45 = require("./653_huelva/citys");
const citys_46 = require("./654_lleida/citys");
const citys_47 = require("./655_leon/citys");
const citys_48 = require("./656_illes_balears/citys");
const citys_49 = require("./657_burgos/citys");
const citys_50 = require("./658_huesca/citys");
const citys_51 = require("./659_asturias/citys");
const citys_52 = require("./660_malaga/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
    ...citys_52.citys,
];
//# sourceMappingURL=citys.js.map