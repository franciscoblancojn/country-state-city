"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./13_dong_bang_song_cuu_long/citys");
const citys_2 = require("./14_dong_bang_song_hong/citys");
const citys_3 = require("./15_dong_nam_bo/citys");
const citys_4 = require("./16_duyen_hai_mien_trung/citys");
const citys_5 = require("./17_khu_bon_cu/citys");
const citys_6 = require("./18_mien_nui_va_trung_du/citys");
const citys_7 = require("./19_thai_nguyen/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
];
//# sourceMappingURL=citys.js.map