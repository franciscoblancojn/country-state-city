"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1152_balzers/citys");
const citys_2 = require("./1153_eschen/citys");
const citys_3 = require("./1154_gamprin/citys");
const citys_4 = require("./1155_mauren/citys");
const citys_5 = require("./1156_planken/citys");
const citys_6 = require("./1157_ruggell/citys");
const citys_7 = require("./1158_schaan/citys");
const citys_8 = require("./1159_schellenberg/citys");
const citys_9 = require("./1160_triesen/citys");
const citys_10 = require("./1161_triesenberg/citys");
const citys_11 = require("./1162_vaduz/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
];
//# sourceMappingURL=citys.js.map