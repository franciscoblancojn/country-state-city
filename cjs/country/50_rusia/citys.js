"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./133_altaiskii_krai/citys");
const citys_2 = require("./134_amurskaya_obl./citys");
const citys_3 = require("./135_arhangelskaya_obl./citys");
const citys_4 = require("./136_astrahanskaya_obl./citys");
const citys_5 = require("./137_bashkiriya_obl./citys");
const citys_6 = require("./138_belgorodskaya_obl./citys");
const citys_7 = require("./139_bryanskaya_obl./citys");
const citys_8 = require("./140_buryatiya/citys");
const citys_9 = require("./141_vladimirskaya_obl./citys");
const citys_10 = require("./142_volgogradskaya_obl./citys");
const citys_11 = require("./143_vologodskaya_obl./citys");
const citys_12 = require("./144_voronezhskaya_obl./citys");
const citys_13 = require("./145_nizhegorodskaya_obl./citys");
const citys_14 = require("./146_dagestan/citys");
const citys_15 = require("./147_evreiskaya_obl./citys");
const citys_16 = require("./148_ivanovskaya_obl./citys");
const citys_17 = require("./149_irkutskaya_obl./citys");
const citys_18 = require("./150_kabardino-balkariya/citys");
const citys_19 = require("./151_kaliningradskaya_obl./citys");
const citys_20 = require("./152_tverskaya_obl./citys");
const citys_21 = require("./153_kalmykiya/citys");
const citys_22 = require("./154_kaluzhskaya_obl./citys");
const citys_23 = require("./155_kamchatskaya_obl./citys");
const citys_24 = require("./156_kareliya/citys");
const citys_25 = require("./157_kemerovskaya_obl./citys");
const citys_26 = require("./158_kirovskaya_obl./citys");
const citys_27 = require("./159_komi/citys");
const citys_28 = require("./160_kostromskaya_obl./citys");
const citys_29 = require("./161_krasnodarskii_krai/citys");
const citys_30 = require("./162_krasnoyarskii_krai/citys");
const citys_31 = require("./163_kurganskaya_obl./citys");
const citys_32 = require("./164_kurskaya_obl./citys");
const citys_33 = require("./165_lipetskaya_obl./citys");
const citys_34 = require("./166_magadanskaya_obl./citys");
const citys_35 = require("./167_marii_el/citys");
const citys_36 = require("./168_mordoviya/citys");
const citys_37 = require("./169_moscow_y_moscow_region/citys");
const citys_38 = require("./170_murmanskaya_obl./citys");
const citys_39 = require("./171_novgorodskaya_obl./citys");
const citys_40 = require("./172_novosibirskaya_obl./citys");
const citys_41 = require("./173_omskaya_obl./citys");
const citys_42 = require("./174_orenburgskaya_obl./citys");
const citys_43 = require("./175_orlovskaya_obl./citys");
const citys_44 = require("./176_penzenskaya_obl./citys");
const citys_45 = require("./177_permskiy_krai/citys");
const citys_46 = require("./178_primorskii_krai/citys");
const citys_47 = require("./179_pskovskaya_obl./citys");
const citys_48 = require("./180_rostovskaya_obl./citys");
const citys_49 = require("./181_ryazanskaya_obl./citys");
const citys_50 = require("./182_samarskaya_obl./citys");
const citys_51 = require("./183_saint-petersburg_and_region/citys");
const citys_52 = require("./184_saratovskaya_obl./citys");
const citys_53 = require("./185_saha_(yakutiya)/citys");
const citys_54 = require("./186_sahalin/citys");
const citys_55 = require("./187_sverdlovskaya_obl./citys");
const citys_56 = require("./188_severnaya_osetiya/citys");
const citys_57 = require("./189_smolenskaya_obl./citys");
const citys_58 = require("./190_stavropolskii_krai/citys");
const citys_59 = require("./191_tambovskaya_obl./citys");
const citys_60 = require("./192_tatarstan/citys");
const citys_61 = require("./193_tomskaya_obl./citys");
const citys_62 = require("./195_tulskaya_obl./citys");
const citys_63 = require("./196_tyumenskaya_obl._i_hanty-mansiiskii_ao/citys");
const citys_64 = require("./197_udmurtiya/citys");
const citys_65 = require("./198_ulyanovskaya_obl./citys");
const citys_66 = require("./199_uralskaya_obl./citys");
const citys_67 = require("./200_habarovskii_krai/citys");
const citys_68 = require("./201_chelyabinskaya_obl./citys");
const citys_69 = require("./202_checheno-ingushetiya/citys");
const citys_70 = require("./203_chitinskaya_obl./citys");
const citys_71 = require("./204_chuvashiya/citys");
const citys_72 = require("./205_yaroslavskaya_obl./citys");
const citys_73 = require("./381_adygeya/citys");
const citys_74 = require("./382_hakasiya/citys");
const citys_75 = require("./384_chukotskii_ao/citys");
const citys_76 = require("./396_yamalo-nenetskii_ao/citys");
const citys_77 = require("./2044_karachaeva-cherkesskaya_respublica/citys");
const citys_78 = require("./2045_raimirskii_(dolgano-nenetskii)_ao/citys");
const citys_79 = require("./2046_respublica_tiva/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
    ...citys_52.citys,
    ...citys_53.citys,
    ...citys_54.citys,
    ...citys_55.citys,
    ...citys_56.citys,
    ...citys_57.citys,
    ...citys_58.citys,
    ...citys_59.citys,
    ...citys_60.citys,
    ...citys_61.citys,
    ...citys_62.citys,
    ...citys_63.citys,
    ...citys_64.citys,
    ...citys_65.citys,
    ...citys_66.citys,
    ...citys_67.citys,
    ...citys_68.citys,
    ...citys_69.citys,
    ...citys_70.citys,
    ...citys_71.citys,
    ...citys_72.citys,
    ...citys_73.citys,
    ...citys_74.citys,
    ...citys_75.citys,
    ...citys_76.citys,
    ...citys_77.citys,
    ...citys_78.citys,
    ...citys_79.citys,
];
//# sourceMappingURL=citys.js.map