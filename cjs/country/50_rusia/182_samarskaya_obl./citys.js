"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
exports.citys = [
    {
        id: 2830,
        id_state: 182,
        text: "Alekseevka (Samarskaya obl.)",
        id_country: 50,
    },
    { id: 2832, id_state: 182, text: "Bogatoe", id_country: 50 },
    { id: 2833, id_state: 182, text: "Bogatyr", id_country: 50 },
    { id: 2847, id_state: 182, text: "Kuibyshev", id_country: 50 },
    { id: 2849, id_state: 182, text: "Novokuibyshevsk", id_country: 50 },
    { id: 2855, id_state: 182, text: "Samara", id_country: 50 },
    { id: 2856, id_state: 182, text: "Syzran", id_country: 50 },
    { id: 2857, id_state: 182, text: "Tolyatti", id_country: 50 },
];
//# sourceMappingURL=citys.js.map