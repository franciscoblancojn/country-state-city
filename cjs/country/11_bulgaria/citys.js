"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1007_mikhaylovgrad/citys");
const citys_2 = require("./1008_blagoevgrad/citys");
const citys_3 = require("./1009_burgas/citys");
const citys_4 = require("./1010_dobrich/citys");
const citys_5 = require("./1011_gabrovo/citys");
const citys_6 = require("./1012_grad_sofiya/citys");
const citys_7 = require("./1013_khaskovo/citys");
const citys_8 = require("./1014_kurdzhali/citys");
const citys_9 = require("./1015_kyustendil/citys");
const citys_10 = require("./1016_lovech/citys");
const citys_11 = require("./1017_montana/citys");
const citys_12 = require("./1018_pazardzhik/citys");
const citys_13 = require("./1019_pernik/citys");
const citys_14 = require("./1020_pleven/citys");
const citys_15 = require("./1021_plovdiv/citys");
const citys_16 = require("./1022_razgrad/citys");
const citys_17 = require("./1023_ruse/citys");
const citys_18 = require("./1024_shumen/citys");
const citys_19 = require("./1025_silistra/citys");
const citys_20 = require("./1026_sliven/citys");
const citys_21 = require("./1027_smolyan/citys");
const citys_22 = require("./1028_sofiya/citys");
const citys_23 = require("./1029_stara_zagora/citys");
const citys_24 = require("./1030_turgovishte/citys");
const citys_25 = require("./1031_varna/citys");
const citys_26 = require("./1032_veliko_turnovo/citys");
const citys_27 = require("./1033_vidin/citys");
const citys_28 = require("./1034_vratsa/citys");
const citys_29 = require("./1035_yambol/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
];
//# sourceMappingURL=citys.js.map