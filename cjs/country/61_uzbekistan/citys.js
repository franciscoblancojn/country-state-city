"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./266_andijon_region/citys");
const citys_2 = require("./267_buxoro_region/citys");
const citys_3 = require("./268_jizzac_region/citys");
const citys_4 = require("./269_qaraqalpaqstan/citys");
const citys_5 = require("./270_qashqadaryo_region/citys");
const citys_6 = require("./271_navoiy_region/citys");
const citys_7 = require("./272_namangan_region/citys");
const citys_8 = require("./273_samarqand_region/citys");
const citys_9 = require("./274_surxondaryo_region/citys");
const citys_10 = require("./275_sirdaryo_region/citys");
const citys_11 = require("./276_tashkent_region/citys");
const citys_12 = require("./277_fergana_region/citys");
const citys_13 = require("./278_xorazm_region/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
];
//# sourceMappingURL=citys.js.map