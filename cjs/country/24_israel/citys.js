"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./38_southern_district/citys");
const citys_2 = require("./39_central_district/citys");
const citys_3 = require("./40_northern_district/citys");
const citys_4 = require("./41_haifa/citys");
const citys_5 = require("./42_tel_aviv/citys");
const citys_6 = require("./43_jerusalem/citys");
const citys_7 = require("./409_ramat_hagolan/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
];
//# sourceMappingURL=citys.js.map