"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./913_akershus/citys");
const citys_2 = require("./914_aust-agder/citys");
const citys_3 = require("./915_buskerud/citys");
const citys_4 = require("./916_finnmark/citys");
const citys_5 = require("./917_hedmark/citys");
const citys_6 = require("./918_hordaland/citys");
const citys_7 = require("./919_more_og_romsdal/citys");
const citys_8 = require("./920_nordland/citys");
const citys_9 = require("./921_nord-trondelag/citys");
const citys_10 = require("./922_oppland/citys");
const citys_11 = require("./923_oslo/citys");
const citys_12 = require("./924_ostfold/citys");
const citys_13 = require("./925_rogaland/citys");
const citys_14 = require("./926_sogn_og_fjordane/citys");
const citys_15 = require("./927_sor-trondelag/citys");
const citys_16 = require("./928_telemark/citys");
const citys_17 = require("./929_troms/citys");
const citys_18 = require("./930_vest-agder/citys");
const citys_19 = require("./931_vestfold/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
];
//# sourceMappingURL=citys.js.map