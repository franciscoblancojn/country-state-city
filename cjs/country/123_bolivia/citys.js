"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1777_chuquisaca/citys");
const citys_2 = require("./1778_cochabamba/citys");
const citys_3 = require("./1779_el_beni/citys");
const citys_4 = require("./1780_la_paz/citys");
const citys_5 = require("./1781_oruro/citys");
const citys_6 = require("./1782_pando/citys");
const citys_7 = require("./1783_potosi/citys");
const citys_8 = require("./1784_santa_cruz/citys");
const citys_9 = require("./1785_tarija/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
];
//# sourceMappingURL=citys.js.map