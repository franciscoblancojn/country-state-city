"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1642_artigas/citys");
const citys_2 = require("./1643_canelones/citys");
const citys_3 = require("./1644_cerro_largo/citys");
const citys_4 = require("./1645_colonia/citys");
const citys_5 = require("./1646_durazno/citys");
const citys_6 = require("./1647_flores/citys");
const citys_7 = require("./1648_florida/citys");
const citys_8 = require("./1649_lavalleja/citys");
const citys_9 = require("./1650_maldonado/citys");
const citys_10 = require("./1651_montevideo/citys");
const citys_11 = require("./1652_paysandu/citys");
const citys_12 = require("./1653_rio_negro/citys");
const citys_13 = require("./1654_rivera/citys");
const citys_14 = require("./1655_rocha/citys");
const citys_15 = require("./1656_salto/citys");
const citys_16 = require("./1657_san_jose/citys");
const citys_17 = require("./1658_soriano/citys");
const citys_18 = require("./1659_tacuarembo/citys");
const citys_19 = require("./1660_treinta_y_tres/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
];
//# sourceMappingURL=citys.js.map