"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1596_amazonas/citys");
const citys_2 = require("./1597_ancash/citys");
const citys_3 = require("./1598_apurimac/citys");
const citys_4 = require("./1599_arequipa/citys");
const citys_5 = require("./1600_ayacucho/citys");
const citys_6 = require("./1601_cajamarca/citys");
const citys_7 = require("./1602_callao/citys");
const citys_8 = require("./1603_cusco/citys");
const citys_9 = require("./1604_huancavelica/citys");
const citys_10 = require("./1605_huanuco/citys");
const citys_11 = require("./1606_ica/citys");
const citys_12 = require("./1607_junin/citys");
const citys_13 = require("./1608_la_libertad/citys");
const citys_14 = require("./1609_lambayeque/citys");
const citys_15 = require("./1610_lima/citys");
const citys_16 = require("./1611_loreto/citys");
const citys_17 = require("./1612_madre_de_dios/citys");
const citys_18 = require("./1613_moquegua/citys");
const citys_19 = require("./1614_pasco/citys");
const citys_20 = require("./1615_piura/citys");
const citys_21 = require("./1616_puno/citys");
const citys_22 = require("./1617_san_martin/citys");
const citys_23 = require("./1618_tacna/citys");
const citys_24 = require("./1619_tumbes/citys");
const citys_25 = require("./1620_ucayali/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
];
//# sourceMappingURL=citys.js.map