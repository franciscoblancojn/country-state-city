"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1404_acquaviva/citys");
const citys_2 = require("./1405_chiesanuova/citys");
const citys_3 = require("./1406_domagnano/citys");
const citys_4 = require("./1407_faetano/citys");
const citys_5 = require("./1408_fiorentino/citys");
const citys_6 = require("./1409_borgo_maggiore/citys");
const citys_7 = require("./1410_san_marino/citys");
const citys_8 = require("./1411_monte_giardino/citys");
const citys_9 = require("./1412_serravalle/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
];
//# sourceMappingURL=citys.js.map