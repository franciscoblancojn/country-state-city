"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1966_anhui/citys");
const citys_2 = require("./1967_zhejiang/citys");
const citys_3 = require("./1968_jiangxi/citys");
const citys_4 = require("./1969_jiangsu/citys");
const citys_5 = require("./1970_jilin/citys");
const citys_6 = require("./1971_qinghai/citys");
const citys_7 = require("./1972_fujian/citys");
const citys_8 = require("./1973_heilongjiang/citys");
const citys_9 = require("./1974_henan/citys");
const citys_10 = require("./1975_hebei/citys");
const citys_11 = require("./1976_hunan/citys");
const citys_12 = require("./1977_hubei/citys");
const citys_13 = require("./1978_xinjiang/citys");
const citys_14 = require("./1979_xizang/citys");
const citys_15 = require("./1980_gansu/citys");
const citys_16 = require("./1981_guangxi/citys");
const citys_17 = require("./1982_guizhou/citys");
const citys_18 = require("./1983_liaoning/citys");
const citys_19 = require("./1984_nei_mongol/citys");
const citys_20 = require("./1985_ningxia/citys");
const citys_21 = require("./1986_beijing/citys");
const citys_22 = require("./1987_shanghai/citys");
const citys_23 = require("./1988_shanxi/citys");
const citys_24 = require("./1989_shandong/citys");
const citys_25 = require("./1990_shaanxi/citys");
const citys_26 = require("./1991_sichuan/citys");
const citys_27 = require("./1992_tianjin/citys");
const citys_28 = require("./1993_yunnan/citys");
const citys_29 = require("./1994_guangdong/citys");
const citys_30 = require("./1995_hainan/citys");
const citys_31 = require("./1996_chongqing/citys");
const citys_32 = require("./2108_schanghai/citys");
const citys_33 = require("./2109_hongkong/citys");
const citys_34 = require("./2110_neimenggu/citys");
const citys_35 = require("./2111_aomen/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
];
//# sourceMappingURL=citys.js.map