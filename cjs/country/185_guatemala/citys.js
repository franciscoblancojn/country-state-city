"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1676_alta_verapaz/citys");
const citys_2 = require("./1677_baja_verapaz/citys");
const citys_3 = require("./1678_chimaltenango/citys");
const citys_4 = require("./1679_chiquimula/citys");
const citys_5 = require("./1680_el_progreso/citys");
const citys_6 = require("./1681_escuintla/citys");
const citys_7 = require("./1682_guatemala/citys");
const citys_8 = require("./1683_huehuetenango/citys");
const citys_9 = require("./1684_izabal/citys");
const citys_10 = require("./1685_jalapa/citys");
const citys_11 = require("./1686_jutiapa/citys");
const citys_12 = require("./1687_peten/citys");
const citys_13 = require("./1688_quetzaltenango/citys");
const citys_14 = require("./1689_quiche/citys");
const citys_15 = require("./1690_retalhuleu/citys");
const citys_16 = require("./1691_sacatepequez/citys");
const citys_17 = require("./1692_san_marcos/citys");
const citys_18 = require("./1693_santa_rosa/citys");
const citys_19 = require("./1694_solola/citys");
const citys_20 = require("./1695_suchitepequez/citys");
const citys_21 = require("./1696_totonicapan/citys");
const citys_22 = require("./1697_zacapa/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
];
//# sourceMappingURL=citys.js.map