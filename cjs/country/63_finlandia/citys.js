"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./933_\u00F0\u2022land/citys");
const citys_2 = require("./934_lapland/citys");
const citys_3 = require("./935_oulu/citys");
const citys_4 = require("./936_southern_finland/citys");
const citys_5 = require("./937_eastern_finland/citys");
const citys_6 = require("./938_western_finland/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
];
//# sourceMappingURL=citys.js.map