"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./383_dubai/citys");
const citys_2 = require("./406_sharjah/citys");
const citys_3 = require("./407_abu_dhabi/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys, ...citys_3.citys];
//# sourceMappingURL=citys.js.map