"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./221_ashgabat_region/citys");
const citys_2 = require("./222_krasnovodsk_region/citys");
const citys_3 = require("./223_mary_region/citys");
const citys_4 = require("./224_tashauz_region/citys");
const citys_5 = require("./225_chardzhou_region/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
];
//# sourceMappingURL=citys.js.map