"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./887_blekinge_lan/citys");
const citys_2 = require("./888_gavleborgs_lan/citys");
const citys_3 = require("./890_gotlands_lan/citys");
const citys_4 = require("./891_hallands_lan/citys");
const citys_5 = require("./892_jamtlands_lan/citys");
const citys_6 = require("./893_jonkopings_lan/citys");
const citys_7 = require("./894_kalmar_lan/citys");
const citys_8 = require("./895_dalarnas_lan/citys");
const citys_9 = require("./897_kronobergs_lan/citys");
const citys_10 = require("./899_norrbottens_lan/citys");
const citys_11 = require("./900_orebro_lan/citys");
const citys_12 = require("./901_ostergotlands_lan/citys");
const citys_13 = require("./903_sodermanlands_lan/citys");
const citys_14 = require("./904_uppsala_lan/citys");
const citys_15 = require("./905_varmlands_lan/citys");
const citys_16 = require("./906_vasterbottens_lan/citys");
const citys_17 = require("./907_vasternorrlands_lan/citys");
const citys_18 = require("./908_vastmanlands_lan/citys");
const citys_19 = require("./909_stockholms_lan/citys");
const citys_20 = require("./910_skane_lan/citys");
const citys_21 = require("./911_vastra_gotaland/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
];
//# sourceMappingURL=citys.js.map