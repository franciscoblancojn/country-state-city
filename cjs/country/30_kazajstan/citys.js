"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./81_aktyubinskaya_obl./citys");
const citys_2 = require("./82_alma-atinskaya_obl./citys");
const citys_3 = require("./83_vostochno-kazahstanskaya_obl./citys");
const citys_4 = require("./84_gurevskaya_obl./citys");
const citys_5 = require("./85_zhambylskaya_obl._(dzhambulskaya_obl.)/citys");
const citys_6 = require("./86_dzhezkazganskaya_obl./citys");
const citys_7 = require("./87_karagandinskaya_obl./citys");
const citys_8 = require("./88_kzyl-ordinskaya_obl./citys");
const citys_9 = require("./89_kokchetavskaya_obl./citys");
const citys_10 = require("./90_kustanaiskaya_obl./citys");
const citys_11 = require("./91_mangystauskaya_(mangyshlakskaya_obl.)/citys");
const citys_12 = require("./92_pavlodarskaya_obl./citys");
const citys_13 = require("./93_severo-kazahstanskaya_obl./citys");
const citys_14 = require("./94_taldy-kurganskaya_obl./citys");
const citys_15 = require("./95_turgaiskaya_obl./citys");
const citys_16 = require("./96_akmolinskaya_obl._(tselinogradskaya_obl.)/citys");
const citys_17 = require("./97_chimkentskaya_obl./citys");
const citys_18 = require("./374_kazahstan/citys");
const citys_19 = require("./380_zapadno-kazahstanskaya_obl./citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
];
//# sourceMappingURL=citys.js.map