"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1818_buenos_aires/citys");
const citys_2 = require("./1819_catamarca/citys");
const citys_3 = require("./1820_chaco/citys");
const citys_4 = require("./1821_chubut/citys");
const citys_5 = require("./1822_cordoba/citys");
const citys_6 = require("./1823_corrientes/citys");
const citys_7 = require("./1824_distrito_federal/citys");
const citys_8 = require("./1825_entre_rios/citys");
const citys_9 = require("./1826_formosa/citys");
const citys_10 = require("./1827_jujuy/citys");
const citys_11 = require("./1828_la_pampa/citys");
const citys_12 = require("./1829_la_rioja/citys");
const citys_13 = require("./1830_mendoza/citys");
const citys_14 = require("./1831_misiones/citys");
const citys_15 = require("./1832_neuquen/citys");
const citys_16 = require("./1833_rio_negro/citys");
const citys_17 = require("./1834_salta/citys");
const citys_18 = require("./1835_san_juan/citys");
const citys_19 = require("./1836_san_luis/citys");
const citys_20 = require("./1837_santa_cruz/citys");
const citys_21 = require("./1838_santa_fe/citys");
const citys_22 = require("./1839_santiago_del_estero/citys");
const citys_23 = require("./1840_tierra_del_fuego/citys");
const citys_24 = require("./1841_tucuman/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
];
//# sourceMappingURL=citys.js.map