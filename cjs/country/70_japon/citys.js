"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./321_aichi/citys");
const citys_2 = require("./322_akita/citys");
const citys_3 = require("./323_aomori/citys");
const citys_4 = require("./324_wakayama/citys");
const citys_5 = require("./325_gifu/citys");
const citys_6 = require("./326_gunma/citys");
const citys_7 = require("./327_ibaraki/citys");
const citys_8 = require("./328_iwate/citys");
const citys_9 = require("./329_ishikawa/citys");
const citys_10 = require("./330_kagawa/citys");
const citys_11 = require("./331_kagoshima/citys");
const citys_12 = require("./332_kanagawa/citys");
const citys_13 = require("./333_kyoto/citys");
const citys_14 = require("./334_kochi/citys");
const citys_15 = require("./335_kumamoto/citys");
const citys_16 = require("./336_mie/citys");
const citys_17 = require("./337_miyagi/citys");
const citys_18 = require("./338_miyazaki/citys");
const citys_19 = require("./339_nagano/citys");
const citys_20 = require("./340_nagasaki/citys");
const citys_21 = require("./341_nara/citys");
const citys_22 = require("./342_niigata/citys");
const citys_23 = require("./343_okayama/citys");
const citys_24 = require("./344_okinawa/citys");
const citys_25 = require("./345_osaka/citys");
const citys_26 = require("./346_saga/citys");
const citys_27 = require("./347_saitama/citys");
const citys_28 = require("./348_shiga/citys");
const citys_29 = require("./349_shizuoka/citys");
const citys_30 = require("./350_shimane/citys");
const citys_31 = require("./351_tiba/citys");
const citys_32 = require("./352_tokyo/citys");
const citys_33 = require("./353_tokushima/citys");
const citys_34 = require("./354_tochigi/citys");
const citys_35 = require("./355_tottori/citys");
const citys_36 = require("./356_toyama/citys");
const citys_37 = require("./357_fukui/citys");
const citys_38 = require("./358_fukuoka/citys");
const citys_39 = require("./359_fukushima/citys");
const citys_40 = require("./360_hiroshima/citys");
const citys_41 = require("./361_hokkaido/citys");
const citys_42 = require("./362_hyogo/citys");
const citys_43 = require("./363_yoshimi/citys");
const citys_44 = require("./364_yamagata/citys");
const citys_45 = require("./365_yamaguchi/citys");
const citys_46 = require("./366_yamanashi/citys");
const citys_47 = require("./2103_chiba/citys");
const citys_48 = require("./2104_ehime/citys");
const citys_49 = require("./2105_oita/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
];
//# sourceMappingURL=citys.js.map