"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./2047_newfoundland/citys");
const citys_2 = require("./2048_nova_scotia/citys");
const citys_3 = require("./2049_prince_edward_island/citys");
const citys_4 = require("./2050_new_brunswick/citys");
const citys_5 = require("./2051_quebec/citys");
const citys_6 = require("./2052_ontario/citys");
const citys_7 = require("./2053_manitoba/citys");
const citys_8 = require("./2054_saskatchewan/citys");
const citys_9 = require("./2055_alberta/citys");
const citys_10 = require("./2056_british_columbia/citys");
const citys_11 = require("./2057_nunavut/citys");
const citys_12 = require("./2058_northwest_territories/citys");
const citys_13 = require("./2059_yukon_territory/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
];
//# sourceMappingURL=citys.js.map