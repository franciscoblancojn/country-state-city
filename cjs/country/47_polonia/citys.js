"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1295_biala_podlaska/citys");
const citys_2 = require("./1296_bialystok/citys");
const citys_3 = require("./1297_bielsko/citys");
const citys_4 = require("./1298_bydgoszcz/citys");
const citys_5 = require("./1299_chelm/citys");
const citys_6 = require("./1300_ciechanow/citys");
const citys_7 = require("./1301_czestochowa/citys");
const citys_8 = require("./1302_elblag/citys");
const citys_9 = require("./1303_gdansk/citys");
const citys_10 = require("./1304_gorzow/citys");
const citys_11 = require("./1305_jelenia_gora/citys");
const citys_12 = require("./1306_kalisz/citys");
const citys_13 = require("./1307_katowice/citys");
const citys_14 = require("./1308_kielce/citys");
const citys_15 = require("./1309_konin/citys");
const citys_16 = require("./1310_koszalin/citys");
const citys_17 = require("./1311_krakow/citys");
const citys_18 = require("./1312_krosno/citys");
const citys_19 = require("./1313_legnica/citys");
const citys_20 = require("./1314_leszno/citys");
const citys_21 = require("./1315_lodz/citys");
const citys_22 = require("./1316_lomza/citys");
const citys_23 = require("./1317_lublin/citys");
const citys_24 = require("./1318_nowy_sacz/citys");
const citys_25 = require("./1319_olsztyn/citys");
const citys_26 = require("./1320_opole/citys");
const citys_27 = require("./1321_ostroleka/citys");
const citys_28 = require("./1322_pila/citys");
const citys_29 = require("./1323_piotrkow/citys");
const citys_30 = require("./1324_plock/citys");
const citys_31 = require("./1325_poznan/citys");
const citys_32 = require("./1326_przemysl/citys");
const citys_33 = require("./1327_radom/citys");
const citys_34 = require("./1328_rzeszow/citys");
const citys_35 = require("./1329_siedlce/citys");
const citys_36 = require("./1330_sieradz/citys");
const citys_37 = require("./1331_skierniewice/citys");
const citys_38 = require("./1332_slupsk/citys");
const citys_39 = require("./1333_suwalki/citys");
const citys_40 = require("./1335_tarnobrzeg/citys");
const citys_41 = require("./1336_tarnow/citys");
const citys_42 = require("./1337_torun/citys");
const citys_43 = require("./1338_walbrzych/citys");
const citys_44 = require("./1339_warszawa/citys");
const citys_45 = require("./1340_wloclawek/citys");
const citys_46 = require("./1341_wroclaw/citys");
const citys_47 = require("./1342_zamosc/citys");
const citys_48 = require("./1343_zielona_gora/citys");
const citys_49 = require("./1344_dolnoslaskie/citys");
const citys_50 = require("./1345_kujawsko-pomorskie/citys");
const citys_51 = require("./1346_lodzkie/citys");
const citys_52 = require("./1347_lubelskie/citys");
const citys_53 = require("./1348_lubuskie/citys");
const citys_54 = require("./1349_malopolskie/citys");
const citys_55 = require("./1350_mazowieckie/citys");
const citys_56 = require("./1351_opolskie/citys");
const citys_57 = require("./1352_podkarpackie/citys");
const citys_58 = require("./1353_podlaskie/citys");
const citys_59 = require("./1354_pomorskie/citys");
const citys_60 = require("./1355_slaskie/citys");
const citys_61 = require("./1356_swietokrzyskie/citys");
const citys_62 = require("./1357_warminsko-mazurskie/citys");
const citys_63 = require("./1358_wielkopolskie/citys");
const citys_64 = require("./1359_zachodniopomorskie/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
    ...citys_52.citys,
    ...citys_53.citys,
    ...citys_54.citys,
    ...citys_55.citys,
    ...citys_56.citys,
    ...citys_57.citys,
    ...citys_58.citys,
    ...citys_59.citys,
    ...citys_60.citys,
    ...citys_61.citys,
    ...citys_62.citys,
    ...citys_63.citys,
    ...citys_64.citys,
];
//# sourceMappingURL=citys.js.map