"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1037_bjelovarsko-bilogorska/citys");
const citys_2 = require("./1038_brodsko-posavska/citys");
const citys_3 = require("./1039_dubrovacko-neretvanska/citys");
const citys_4 = require("./1040_istarska/citys");
const citys_5 = require("./1041_karlovacka/citys");
const citys_6 = require("./1042_koprivnicko-krizevacka/citys");
const citys_7 = require("./1043_krapinsko-zagorska/citys");
const citys_8 = require("./1044_licko-senjska/citys");
const citys_9 = require("./1045_medimurska/citys");
const citys_10 = require("./1046_osjecko-baranjska/citys");
const citys_11 = require("./1047_pozesko-slavonska/citys");
const citys_12 = require("./1048_primorsko-goranska/citys");
const citys_13 = require("./1049_sibensko-kninska/citys");
const citys_14 = require("./1050_sisacko-moslavacka/citys");
const citys_15 = require("./1051_splitsko-dalmatinska/citys");
const citys_16 = require("./1052_varazdinska/citys");
const citys_17 = require("./1053_viroviticko-podravska/citys");
const citys_18 = require("./1054_vukovarsko-srijemska/citys");
const citys_19 = require("./1055_zadarska/citys");
const citys_20 = require("./1056_zagrebacka/citys");
const citys_21 = require("./1057_grad_zagreb/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
];
//# sourceMappingURL=citys.js.map