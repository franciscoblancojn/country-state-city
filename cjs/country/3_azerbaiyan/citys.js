"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1_azerbaijan/citys");
const citys_2 = require("./2_nargorni_karabakh/citys");
const citys_3 = require("./3_nakhichevanskaya_region/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys, ...citys_3.citys];
//# sourceMappingURL=citys.js.map