"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1292_la_condamine/citys");
const citys_2 = require("./1293_monaco/citys");
const citys_3 = require("./1294_monte-carlo/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys, ...citys_3.citys];
//# sourceMappingURL=citys.js.map