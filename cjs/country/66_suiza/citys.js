"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./722_aargau/citys");
const citys_2 = require("./723_appenzell_innerrhoden/citys");
const citys_3 = require("./724_appenzell_ausserrhoden/citys");
const citys_4 = require("./725_bern/citys");
const citys_5 = require("./726_basel-landschaft/citys");
const citys_6 = require("./727_basel-stadt/citys");
const citys_7 = require("./728_fribourg/citys");
const citys_8 = require("./729_geneve/citys");
const citys_9 = require("./730_glarus/citys");
const citys_10 = require("./731_graubunden/citys");
const citys_11 = require("./732_jura/citys");
const citys_12 = require("./733_luzern/citys");
const citys_13 = require("./734_neuchatel/citys");
const citys_14 = require("./735_nidwalden/citys");
const citys_15 = require("./736_obwalden/citys");
const citys_16 = require("./737_sankt_gallen/citys");
const citys_17 = require("./738_schaffhausen/citys");
const citys_18 = require("./739_solothurn/citys");
const citys_19 = require("./740_schwyz/citys");
const citys_20 = require("./741_thurgau/citys");
const citys_21 = require("./742_ticino/citys");
const citys_22 = require("./743_uri/citys");
const citys_23 = require("./744_vaud/citys");
const citys_24 = require("./745_valais/citys");
const citys_25 = require("./746_zug/citys");
const citys_26 = require("./747_zurich/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
];
//# sourceMappingURL=citys.js.map