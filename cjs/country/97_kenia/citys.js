"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1997_central/citys");
const citys_2 = require("./1998_coast/citys");
const citys_3 = require("./1999_eastern/citys");
const citys_4 = require("./2000_nairobi_area/citys");
const citys_5 = require("./2001_north-eastern/citys");
const citys_6 = require("./2002_nyanza/citys");
const citys_7 = require("./2003_rift_valley/citys");
const citys_8 = require("./2004_western/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
];
//# sourceMappingURL=citys.js.map