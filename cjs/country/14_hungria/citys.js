"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1112_bacs-kiskun/citys");
const citys_2 = require("./1113_baranya/citys");
const citys_3 = require("./1114_bekes/citys");
const citys_4 = require("./1115_borsod-abauj-zemplen/citys");
const citys_5 = require("./1116_budapest/citys");
const citys_6 = require("./1117_csongrad/citys");
const citys_7 = require("./1118_debrecen/citys");
const citys_8 = require("./1119_fejer/citys");
const citys_9 = require("./1120_gyor-moson-sopron/citys");
const citys_10 = require("./1121_hajdu-bihar/citys");
const citys_11 = require("./1122_heves/citys");
const citys_12 = require("./1123_komarom-esztergom/citys");
const citys_13 = require("./1124_miskolc/citys");
const citys_14 = require("./1125_nograd/citys");
const citys_15 = require("./1126_pecs/citys");
const citys_16 = require("./1127_pest/citys");
const citys_17 = require("./1128_somogy/citys");
const citys_18 = require("./1129_szabolcs-szatmar-bereg/citys");
const citys_19 = require("./1130_szeged/citys");
const citys_20 = require("./1131_jasz-nagykun-szolnok/citys");
const citys_21 = require("./1132_tolna/citys");
const citys_22 = require("./1133_vas/citys");
const citys_23 = require("./1134_veszprem/citys");
const citys_24 = require("./1135_zala/citys");
const citys_25 = require("./1136_gyor/citys");
const citys_26 = require("./1150_veszprem/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
];
//# sourceMappingURL=citys.js.map