"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./32_al_qa\u201Ea\uFFFDhira/citys");
const citys_2 = require("./33_aswan/citys");
const citys_3 = require("./34_asyut/citys");
const citys_4 = require("./35_beni_suef/citys");
const citys_5 = require("./36_gharbia/citys");
const citys_6 = require("./37_damietta/citys");
const citys_7 = require("./377_egypt/citys");
const citys_8 = require("./389_sinai/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
];
//# sourceMappingURL=citys.js.map