"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./398_thailand/citys");
const citys_2 = require("./2112_amnat_charoen/citys");
const citys_3 = require("./2113_ang_thong/citys");
const citys_4 = require("./2114_bangkok/citys");
const citys_5 = require("./2115_buri_ram/citys");
const citys_6 = require("./2116_chachoengsao/citys");
const citys_7 = require("./2117_chai_nat/citys");
const citys_8 = require("./2118_chaiyaphum/citys");
const citys_9 = require("./2119_chanthaburi/citys");
const citys_10 = require("./2120_chiang_mai/citys");
const citys_11 = require("./2121_chiang_rai/citys");
const citys_12 = require("./2122_chon_buri/citys");
const citys_13 = require("./2124_kalasin/citys");
const citys_14 = require("./2126_kanchanaburi/citys");
const citys_15 = require("./2127_khon_kaen/citys");
const citys_16 = require("./2128_krabi/citys");
const citys_17 = require("./2129_lampang/citys");
const citys_18 = require("./2131_loei/citys");
const citys_19 = require("./2132_lop_buri/citys");
const citys_20 = require("./2133_mae_hong_son/citys");
const citys_21 = require("./2134_maha_sarakham/citys");
const citys_22 = require("./2137_nakhon_pathom/citys");
const citys_23 = require("./2139_nakhon_ratchasima/citys");
const citys_24 = require("./2140_nakhon_sawan/citys");
const citys_25 = require("./2141_nakhon_si_thammarat/citys");
const citys_26 = require("./2143_narathiwat/citys");
const citys_27 = require("./2144_nong_bua_lam_phu/citys");
const citys_28 = require("./2145_nong_khai/citys");
const citys_29 = require("./2146_nonthaburi/citys");
const citys_30 = require("./2147_pathum_thani/citys");
const citys_31 = require("./2148_pattani/citys");
const citys_32 = require("./2149_phangnga/citys");
const citys_33 = require("./2150_phatthalung/citys");
const citys_34 = require("./2154_phichit/citys");
const citys_35 = require("./2155_phitsanulok/citys");
const citys_36 = require("./2156_phra_nakhon_si_ayutthaya/citys");
const citys_37 = require("./2157_phrae/citys");
const citys_38 = require("./2158_phuket/citys");
const citys_39 = require("./2159_prachin_buri/citys");
const citys_40 = require("./2160_prachuap_khiri_khan/citys");
const citys_41 = require("./2162_ratchaburi/citys");
const citys_42 = require("./2163_rayong/citys");
const citys_43 = require("./2164_roi_et/citys");
const citys_44 = require("./2165_sa_kaeo/citys");
const citys_45 = require("./2166_sakon_nakhon/citys");
const citys_46 = require("./2167_samut_prakan/citys");
const citys_47 = require("./2168_samut_sakhon/citys");
const citys_48 = require("./2169_samut_songkhran/citys");
const citys_49 = require("./2170_saraburi/citys");
const citys_50 = require("./2172_si_sa_ket/citys");
const citys_51 = require("./2173_sing_buri/citys");
const citys_52 = require("./2174_songkhla/citys");
const citys_53 = require("./2175_sukhothai/citys");
const citys_54 = require("./2176_suphan_buri/citys");
const citys_55 = require("./2177_surat_thani/citys");
const citys_56 = require("./2178_surin/citys");
const citys_57 = require("./2180_trang/citys");
const citys_58 = require("./2182_ubon_ratchathani/citys");
const citys_59 = require("./2183_udon_thani/citys");
const citys_60 = require("./2184_uthai_thani/citys");
const citys_61 = require("./2185_uttaradit/citys");
const citys_62 = require("./2186_yala/citys");
const citys_63 = require("./2187_yasothon/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
    ...citys_52.citys,
    ...citys_53.citys,
    ...citys_54.citys,
    ...citys_55.citys,
    ...citys_56.citys,
    ...citys_57.citys,
    ...citys_58.citys,
    ...citys_59.citys,
    ...citys_60.citys,
    ...citys_61.citys,
    ...citys_62.citys,
    ...citys_63.citys,
];
//# sourceMappingURL=citys.js.map