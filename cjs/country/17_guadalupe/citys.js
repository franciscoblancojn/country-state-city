"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./26_grande-terre/citys");
const citys_2 = require("./27_basse-terre/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys];
//# sourceMappingURL=citys.js.map