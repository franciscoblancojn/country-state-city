"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1699_amazonas/citys");
const citys_2 = require("./1700_antioquia/citys");
const citys_3 = require("./1701_arauca/citys");
const citys_4 = require("./1702_atlantico/citys");
const citys_5 = require("./1703_caqueta/citys");
const citys_6 = require("./1704_cauca/citys");
const citys_7 = require("./1705_cesar/citys");
const citys_8 = require("./1706_choco/citys");
const citys_9 = require("./1707_cordoba/citys");
const citys_10 = require("./1708_guaviare/citys");
const citys_11 = require("./1709_guainia/citys");
const citys_12 = require("./1710_huila/citys");
const citys_13 = require("./1711_la_guajira/citys");
const citys_14 = require("./1712_meta/citys");
const citys_15 = require("./1713_narino/citys");
const citys_16 = require("./1714_norte_de_santander/citys");
const citys_17 = require("./1715_putumayo/citys");
const citys_18 = require("./1716_quindio/citys");
const citys_19 = require("./1717_risaralda/citys");
const citys_20 = require("./1718_san_andres_y_providencia/citys");
const citys_21 = require("./1719_santander/citys");
const citys_22 = require("./1720_sucre/citys");
const citys_23 = require("./1721_tolima/citys");
const citys_24 = require("./1722_valle_del_cauca/citys");
const citys_25 = require("./1723_vaupes/citys");
const citys_26 = require("./1724_vichada/citys");
const citys_27 = require("./1725_casanare/citys");
const citys_28 = require("./1726_cundinamarca/citys");
const citys_29 = require("./1727_distrito_capital/citys");
const citys_30 = require("./1730_caldas/citys");
const citys_31 = require("./1731_magdalena/citys");
const citys_32 = require("./2202_bolivar/citys");
const citys_33 = require("./2203_boyaca/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
];
//# sourceMappingURL=citys.js.map