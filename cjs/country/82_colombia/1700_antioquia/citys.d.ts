export declare const citys: ({
    id: number;
    id_state: number;
    text: string;
    id_country: number;
    nameAve: string;
} | {
    id: number;
    id_state: number;
    text: string;
    id_country: number;
    nameAve?: undefined;
})[];
