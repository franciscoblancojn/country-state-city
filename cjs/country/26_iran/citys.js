"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./67_azarbayjan-e_khavari/citys");
const citys_2 = require("./68_esfahan/citys");
const citys_3 = require("./69_hamadan/citys");
const citys_4 = require("./70_kordestan/citys");
const citys_5 = require("./71_markazi/citys");
const citys_6 = require("./72_sistan-e_baluches/citys");
const citys_7 = require("./73_yazd/citys");
const citys_8 = require("./74_kerman/citys");
const citys_9 = require("./75_kermanshakhan/citys");
const citys_10 = require("./76_mazenderan/citys");
const citys_11 = require("./77_tehran/citys");
const citys_12 = require("./78_fars/citys");
const citys_13 = require("./79_horasan/citys");
const citys_14 = require("./80_husistan/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
];
//# sourceMappingURL=citys.js.map