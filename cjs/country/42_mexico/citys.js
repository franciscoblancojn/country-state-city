"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1733_aguascalientes/citys");
const citys_2 = require("./1734_baja_california/citys");
const citys_3 = require("./1735_baja_california_sur/citys");
const citys_4 = require("./1736_campeche/citys");
const citys_5 = require("./1737_chiapas/citys");
const citys_6 = require("./1738_chihuahua/citys");
const citys_7 = require("./1739_coahuila_de_zaragoza/citys");
const citys_8 = require("./1740_colima/citys");
const citys_9 = require("./1741_distrito_federal/citys");
const citys_10 = require("./1742_durango/citys");
const citys_11 = require("./1743_guanajuato/citys");
const citys_12 = require("./1744_guerrero/citys");
const citys_13 = require("./1745_hidalgo/citys");
const citys_14 = require("./1746_jalisco/citys");
const citys_15 = require("./1747_mexico/citys");
const citys_16 = require("./1748_michoacan_de_ocampo/citys");
const citys_17 = require("./1749_morelos/citys");
const citys_18 = require("./1750_nayarit/citys");
const citys_19 = require("./1751_nuevo_leon/citys");
const citys_20 = require("./1752_oaxaca/citys");
const citys_21 = require("./1753_puebla/citys");
const citys_22 = require("./1754_queretaro_de_arteaga/citys");
const citys_23 = require("./1755_quintana_roo/citys");
const citys_24 = require("./1756_san_luis_potosi/citys");
const citys_25 = require("./1757_sinaloa/citys");
const citys_26 = require("./1758_sonora/citys");
const citys_27 = require("./1759_tabasco/citys");
const citys_28 = require("./1760_tamaulipas/citys");
const citys_29 = require("./1761_tlaxcala/citys");
const citys_30 = require("./1762_veracruz-llave/citys");
const citys_31 = require("./1763_yucatan/citys");
const citys_32 = require("./1764_zacatecas/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
];
//# sourceMappingURL=citys.js.map