"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./874_bruxelles/citys");
const citys_2 = require("./875_west-vlaanderen/citys");
const citys_3 = require("./876_oost-vlaanderen/citys");
const citys_4 = require("./877_limburg/citys");
const citys_5 = require("./878_vlaams_brabant/citys");
const citys_6 = require("./879_antwerpen/citys");
const citys_7 = require("./880_lia\u201Ea\uFFFDge/citys");
const citys_8 = require("./881_namur/citys");
const citys_9 = require("./882_hainaut/citys");
const citys_10 = require("./883_luxembourg/citys");
const citys_11 = require("./884_brabant_wallon/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
];
//# sourceMappingURL=citys.js.map