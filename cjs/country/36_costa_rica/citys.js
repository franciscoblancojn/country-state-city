"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1787_alajuela/citys");
const citys_2 = require("./1788_cartago/citys");
const citys_3 = require("./1789_guanacaste/citys");
const citys_4 = require("./1790_heredia/citys");
const citys_5 = require("./1791_limon/citys");
const citys_6 = require("./1792_puntarenas/citys");
const citys_7 = require("./1793_san_jose/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
];
//# sourceMappingURL=citys.js.map