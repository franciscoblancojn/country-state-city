"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./865_burgenland/citys");
const citys_2 = require("./866_karnten/citys");
const citys_3 = require("./867_niederosterreich/citys");
const citys_4 = require("./868_oberosterreich/citys");
const citys_5 = require("./869_salzburg/citys");
const citys_6 = require("./870_steiermark/citys");
const citys_7 = require("./871_tirol/citys");
const citys_8 = require("./872_vorarlberg/citys");
const citys_9 = require("./873_wien/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
];
//# sourceMappingURL=citys.js.map