"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./984_canillo/citys");
const citys_2 = require("./985_encamp/citys");
const citys_3 = require("./986_la_massana/citys");
const citys_4 = require("./987_ordino/citys");
const citys_5 = require("./988_sant_julia_de_loria/citys");
const citys_6 = require("./989_andorra_la_vella/citys");
const citys_7 = require("./990_escaldes-engordany/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
];
//# sourceMappingURL=citys.js.map