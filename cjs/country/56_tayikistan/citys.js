"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./216_gorno-badakhshan_region/citys");
const citys_2 = require("./217_kuljabsk_region/citys");
const citys_3 = require("./218_kurgan-tjube_region/citys");
const citys_4 = require("./219_sughd_region/citys");
const citys_5 = require("./220_tajikistan/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
];
//# sourceMappingURL=citys.js.map