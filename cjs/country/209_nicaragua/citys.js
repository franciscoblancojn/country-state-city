"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1869_boaco/citys");
const citys_2 = require("./1870_carazo/citys");
const citys_3 = require("./1871_chinandega/citys");
const citys_4 = require("./1872_chontales/citys");
const citys_5 = require("./1873_esteli/citys");
const citys_6 = require("./1874_granada/citys");
const citys_7 = require("./1875_jinotega/citys");
const citys_8 = require("./1876_leon/citys");
const citys_9 = require("./1877_madriz/citys");
const citys_10 = require("./1878_managua/citys");
const citys_11 = require("./1879_masaya/citys");
const citys_12 = require("./1880_matagalpa/citys");
const citys_13 = require("./1881_nueva_segovia/citys");
const citys_14 = require("./1882_rio_san_juan/citys");
const citys_15 = require("./1883_rivas/citys");
const citys_16 = require("./1884_zelaya/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
];
//# sourceMappingURL=citys.js.map