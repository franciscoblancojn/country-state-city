"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1901_acre/citys");
const citys_2 = require("./1902_alagoas/citys");
const citys_3 = require("./1903_amapa/citys");
const citys_4 = require("./1904_amazonas/citys");
const citys_5 = require("./1905_bahia/citys");
const citys_6 = require("./1906_ceara/citys");
const citys_7 = require("./1907_distrito_federal/citys");
const citys_8 = require("./1908_espirito_santo/citys");
const citys_9 = require("./1909_mato_grosso_do_sul/citys");
const citys_10 = require("./1910_maranhao/citys");
const citys_11 = require("./1911_mato_grosso/citys");
const citys_12 = require("./1912_minas_gerais/citys");
const citys_13 = require("./1913_para/citys");
const citys_14 = require("./1914_paraiba/citys");
const citys_15 = require("./1915_parana/citys");
const citys_16 = require("./1916_piaui/citys");
const citys_17 = require("./1917_rio_de_janeiro/citys");
const citys_18 = require("./1918_rio_grande_do_norte/citys");
const citys_19 = require("./1919_rio_grande_do_sul/citys");
const citys_20 = require("./1920_rondonia/citys");
const citys_21 = require("./1921_roraima/citys");
const citys_22 = require("./1922_santa_catarina/citys");
const citys_23 = require("./1923_sao_paulo/citys");
const citys_24 = require("./1924_sergipe/citys");
const citys_25 = require("./1925_goias/citys");
const citys_26 = require("./1926_pernambuco/citys");
const citys_27 = require("./1927_tocantins/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
];
//# sourceMappingURL=citys.js.map