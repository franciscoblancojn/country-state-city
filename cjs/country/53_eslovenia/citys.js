"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1423_beltinci/citys");
const citys_2 = require("./1425_bohinj/citys");
const citys_3 = require("./1426_borovnica/citys");
const citys_4 = require("./1427_bovec/citys");
const citys_5 = require("./1428_brda/citys");
const citys_6 = require("./1429_brezice/citys");
const citys_7 = require("./1430_brezovica/citys");
const citys_8 = require("./1432_cerklje_na_gorenjskem/citys");
const citys_9 = require("./1434_cerkno/citys");
const citys_10 = require("./1436_crna_na_koroskem/citys");
const citys_11 = require("./1437_crnomelj/citys");
const citys_12 = require("./1438_divaca/citys");
const citys_13 = require("./1439_dobrepolje/citys");
const citys_14 = require("./1440_dol_pri_ljubljani/citys");
const citys_15 = require("./1443_duplek/citys");
const citys_16 = require("./1447_gornji_grad/citys");
const citys_17 = require("./1450_hrastnik/citys");
const citys_18 = require("./1451_hrpelje-kozina/citys");
const citys_19 = require("./1452_idrija/citys");
const citys_20 = require("./1453_ig/citys");
const citys_21 = require("./1454_ilirska_bistrica/citys");
const citys_22 = require("./1455_ivancna_gorica/citys");
const citys_23 = require("./1462_komen/citys");
const citys_24 = require("./1463_koper-capodistria/citys");
const citys_25 = require("./1464_kozje/citys");
const citys_26 = require("./1465_kranj/citys");
const citys_27 = require("./1466_kranjska_gora/citys");
const citys_28 = require("./1467_krsko/citys");
const citys_29 = require("./1469_lasko/citys");
const citys_30 = require("./1470_ljubljana/citys");
const citys_31 = require("./1471_ljubno/citys");
const citys_32 = require("./1472_logatec/citys");
const citys_33 = require("./1475_medvode/citys");
const citys_34 = require("./1476_menges/citys");
const citys_35 = require("./1478_mezica/citys");
const citys_36 = require("./1480_moravce/citys");
const citys_37 = require("./1482_mozirje/citys");
const citys_38 = require("./1483_murska_sobota/citys");
const citys_39 = require("./1487_nova_gorica/citys");
const citys_40 = require("./1489_ormoz/citys");
const citys_41 = require("./1491_pesnica/citys");
const citys_42 = require("./1494_postojna/citys");
const citys_43 = require("./1497_radece/citys");
const citys_44 = require("./1498_radenci/citys");
const citys_45 = require("./1500_radovljica/citys");
const citys_46 = require("./1502_rogaska_slatina/citys");
const citys_47 = require("./1505_sencur/citys");
const citys_48 = require("./1506_sentilj/citys");
const citys_49 = require("./1508_sevnica/citys");
const citys_50 = require("./1509_sezana/citys");
const citys_51 = require("./1511_skofja_loka/citys");
const citys_52 = require("./1513_slovenj_gradec/citys");
const citys_53 = require("./1514_slovenske_konjice/citys");
const citys_54 = require("./1515_smarje_pri_jelsah/citys");
const citys_55 = require("./1521_tolmin/citys");
const citys_56 = require("./1522_trbovlje/citys");
const citys_57 = require("./1524_trzic/citys");
const citys_58 = require("./1526_velenje/citys");
const citys_59 = require("./1528_vipava/citys");
const citys_60 = require("./1531_vrhnika/citys");
const citys_61 = require("./1532_vuzenica/citys");
const citys_62 = require("./1533_zagorje_ob_savi/citys");
const citys_63 = require("./1535_zelezniki/citys");
const citys_64 = require("./1536_ziri/citys");
const citys_65 = require("./1537_zrece/citys");
const citys_66 = require("./1539_domzale/citys");
const citys_67 = require("./1540_jesenice/citys");
const citys_68 = require("./1541_kamnik/citys");
const citys_69 = require("./1542_kocevje/citys");
const citys_70 = require("./1544_lenart/citys");
const citys_71 = require("./1545_litija/citys");
const citys_72 = require("./1546_ljutomer/citys");
const citys_73 = require("./1550_maribor/citys");
const citys_74 = require("./1552_novo_mesto/citys");
const citys_75 = require("./1553_piran/citys");
const citys_76 = require("./1554_preddvor/citys");
const citys_77 = require("./1555_ptuj/citys");
const citys_78 = require("./1556_ribnica/citys");
const citys_79 = require("./1558_sentjur_pri_celju/citys");
const citys_80 = require("./1559_slovenska_bistrica/citys");
const citys_81 = require("./1560_videm/citys");
const citys_82 = require("./1562_zalec/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
    ...citys_52.citys,
    ...citys_53.citys,
    ...citys_54.citys,
    ...citys_55.citys,
    ...citys_56.citys,
    ...citys_57.citys,
    ...citys_58.citys,
    ...citys_59.citys,
    ...citys_60.citys,
    ...citys_61.citys,
    ...citys_62.citys,
    ...citys_63.citys,
    ...citys_64.citys,
    ...citys_65.citys,
    ...citys_66.citys,
    ...citys_67.citys,
    ...citys_68.citys,
    ...citys_69.citys,
    ...citys_70.citys,
    ...citys_71.citys,
    ...citys_72.citys,
    ...citys_73.citys,
    ...citys_74.citys,
    ...citys_75.citys,
    ...citys_76.citys,
    ...citys_77.citys,
    ...citys_78.citys,
    ...citys_79.citys,
    ...citys_80.citys,
    ...citys_81.citys,
    ...citys_82.citys,
];
//# sourceMappingURL=citys.js.map