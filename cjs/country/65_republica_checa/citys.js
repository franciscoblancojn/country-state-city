"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./956_hlavni_mesto_praha/citys");
const citys_2 = require("./957_jihomoravsky_kraj/citys");
const citys_3 = require("./958_jihocesky_kraj/citys");
const citys_4 = require("./959_vysocina/citys");
const citys_5 = require("./960_karlovarsky_kraj/citys");
const citys_6 = require("./961_kralovehradecky_kraj/citys");
const citys_7 = require("./962_liberecky_kraj/citys");
const citys_8 = require("./963_olomoucky_kraj/citys");
const citys_9 = require("./964_moravskoslezsky_kraj/citys");
const citys_10 = require("./965_pardubicky_kraj/citys");
const citys_11 = require("./966_plzensky_kraj/citys");
const citys_12 = require("./967_stredocesky_kraj/citys");
const citys_13 = require("./968_ustecky_kraj/citys");
const citys_14 = require("./969_zlinsky_kraj/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
];
//# sourceMappingURL=citys.js.map