"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1843_amazonas/citys");
const citys_2 = require("./1844_anzoategui/citys");
const citys_3 = require("./1845_apure/citys");
const citys_4 = require("./1846_aragua/citys");
const citys_5 = require("./1847_barinas/citys");
const citys_6 = require("./1848_bolivar/citys");
const citys_7 = require("./1849_carabobo/citys");
const citys_8 = require("./1850_cojedes/citys");
const citys_9 = require("./1851_delta_amacuro/citys");
const citys_10 = require("./1852_falcon/citys");
const citys_11 = require("./1853_guarico/citys");
const citys_12 = require("./1854_lara/citys");
const citys_13 = require("./1855_merida/citys");
const citys_14 = require("./1856_miranda/citys");
const citys_15 = require("./1857_monagas/citys");
const citys_16 = require("./1858_nueva_esparta/citys");
const citys_17 = require("./1859_portuguesa/citys");
const citys_18 = require("./1860_sucre/citys");
const citys_19 = require("./1861_tachira/citys");
const citys_20 = require("./1862_trujillo/citys");
const citys_21 = require("./1863_yaracuy/citys");
const citys_22 = require("./1864_zulia/citys");
const citys_23 = require("./1865_dependencias_federales/citys");
const citys_24 = require("./1866_distrito_federal/citys");
const citys_25 = require("./1867_vargas/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
];
//# sourceMappingURL=citys.js.map