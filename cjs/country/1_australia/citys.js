"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./2010_australian_capital_territory/citys");
const citys_2 = require("./2011_new_south_wales/citys");
const citys_3 = require("./2012_northern_territory/citys");
const citys_4 = require("./2013_queensland/citys");
const citys_5 = require("./2014_south_australia/citys");
const citys_6 = require("./2015_tasmania/citys");
const citys_7 = require("./2016_victoria/citys");
const citys_8 = require("./2017_western_australia/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
];
//# sourceMappingURL=citys.js.map