"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./116_auckland/citys");
const citys_2 = require("./117_bay_of_plenty/citys");
const citys_3 = require("./118_canterbury/citys");
const citys_4 = require("./119_gisborne/citys");
const citys_5 = require("./120_hawkes_bay/citys");
const citys_6 = require("./121_manawatu-wanganui/citys");
const citys_7 = require("./122_marlborough/citys");
const citys_8 = require("./123_nelson/citys");
const citys_9 = require("./124_northland/citys");
const citys_10 = require("./125_otago/citys");
const citys_11 = require("./126_southland/citys");
const citys_12 = require("./127_taranaki/citys");
const citys_13 = require("./128_tasman/citys");
const citys_14 = require("./129_waikato/citys");
const citys_15 = require("./130_wellington/citys");
const citys_16 = require("./131_west_coast/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
];
//# sourceMappingURL=citys.js.map