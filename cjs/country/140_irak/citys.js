"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./390_baghdad/citys");
const citys_2 = require("./391_basra/citys");
const citys_3 = require("./392_mosul/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys, ...citys_3.citys];
//# sourceMappingURL=citys.js.map