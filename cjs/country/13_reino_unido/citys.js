"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./848_scotland_north/citys");
const citys_2 = require("./849_england_-_east/citys");
const citys_3 = require("./850_england_-_west_midlands/citys");
const citys_4 = require("./851_england_-_south_west/citys");
const citys_5 = require("./852_england_-_north_west/citys");
const citys_6 = require("./853_england_-_yorks_y_humber/citys");
const citys_7 = require("./854_england_-_south_east/citys");
const citys_8 = require("./855_england_-_london/citys");
const citys_9 = require("./856_northern_ireland/citys");
const citys_10 = require("./857_england_-_north_east/citys");
const citys_11 = require("./858_wales_south/citys");
const citys_12 = require("./859_wales_north/citys");
const citys_13 = require("./860_england_-_east_midlands/citys");
const citys_14 = require("./861_scotland_central/citys");
const citys_15 = require("./862_scotland_south/citys");
const citys_16 = require("./863_channel_islands/citys");
const citys_17 = require("./864_isle_of_man/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
];
//# sourceMappingURL=citys.js.map