"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
exports.citys = [
    { id: 282886, id_state: 1093, text: "Chalcis", id_country: 20 },
    { id: 282898, id_state: 1093, text: "Chalkída", id_country: 20 },
    { id: 282902, id_state: 1093, text: "Chalkís", id_country: 20 },
    { id: 286660, id_state: 1093, text: "Káristos", id_country: 20 },
    { id: 287882, id_state: 1093, text: "Khalkís", id_country: 20 },
    { id: 297196, id_state: 1093, text: "Skíros", id_country: 20 },
    { id: 300142, id_state: 1093, text: "Xirokhórion", id_country: 20 },
];
//# sourceMappingURL=citys.js.map