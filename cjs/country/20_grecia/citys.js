"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1060_evros/citys");
const citys_2 = require("./1061_rodhopi/citys");
const citys_3 = require("./1062_xanthi/citys");
const citys_4 = require("./1063_drama/citys");
const citys_5 = require("./1064_serrai/citys");
const citys_6 = require("./1065_kilkis/citys");
const citys_7 = require("./1066_pella/citys");
const citys_8 = require("./1067_florina/citys");
const citys_9 = require("./1068_kastoria/citys");
const citys_10 = require("./1069_grevena/citys");
const citys_11 = require("./1070_kozani/citys");
const citys_12 = require("./1071_imathia/citys");
const citys_13 = require("./1072_thessaloniki/citys");
const citys_14 = require("./1073_kavala/citys");
const citys_15 = require("./1074_khalkidhiki/citys");
const citys_16 = require("./1075_pieria/citys");
const citys_17 = require("./1076_ioannina/citys");
const citys_18 = require("./1077_thesprotia/citys");
const citys_19 = require("./1078_preveza/citys");
const citys_20 = require("./1079_arta/citys");
const citys_21 = require("./1080_larisa/citys");
const citys_22 = require("./1081_trikala/citys");
const citys_23 = require("./1082_kardhitsa/citys");
const citys_24 = require("./1083_magnisia/citys");
const citys_25 = require("./1084_kerkira/citys");
const citys_26 = require("./1085_levkas/citys");
const citys_27 = require("./1086_kefallinia/citys");
const citys_28 = require("./1087_zakinthos/citys");
const citys_29 = require("./1088_fthiotis/citys");
const citys_30 = require("./1089_evritania/citys");
const citys_31 = require("./1090_aitolia_kai_akarnania/citys");
const citys_32 = require("./1091_fokis/citys");
const citys_33 = require("./1092_voiotia/citys");
const citys_34 = require("./1093_evvoia/citys");
const citys_35 = require("./1094_attiki/citys");
const citys_36 = require("./1095_argolis/citys");
const citys_37 = require("./1096_korinthia/citys");
const citys_38 = require("./1097_akhaia/citys");
const citys_39 = require("./1098_ilia/citys");
const citys_40 = require("./1099_messinia/citys");
const citys_41 = require("./1100_arkadhia/citys");
const citys_42 = require("./1101_lakonia/citys");
const citys_43 = require("./1102_khania/citys");
const citys_44 = require("./1103_rethimni/citys");
const citys_45 = require("./1104_iraklion/citys");
const citys_46 = require("./1105_lasithi/citys");
const citys_47 = require("./1106_dhodhekanisos/citys");
const citys_48 = require("./1107_samos/citys");
const citys_49 = require("./1108_kikladhes/citys");
const citys_50 = require("./1109_khios/citys");
const citys_51 = require("./1110_lesvos/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
];
//# sourceMappingURL=citys.js.map