"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
exports.citys = [
    { id: 280040, id_state: 1075, text: "Ágios Dimítrios", id_country: 20 },
    { id: 280262, id_state: 1075, text: "Aikateríni", id_country: 20 },
    { id: 282839, id_state: 1075, text: "Buráya", id_country: 20 },
    { id: 287012, id_state: 1075, text: "Kateríni", id_country: 20 },
    { id: 290138, id_state: 1075, text: "Leptokarja", id_country: 20 },
    { id: 292509, id_state: 1075, text: "Moschopótamos", id_country: 20 },
    { id: 292963, id_state: 1075, text: "Néoi Póroi", id_country: 20 },
    { id: 294176, id_state: 1075, text: "Paralía Skotínis", id_country: 20 },
];
//# sourceMappingURL=citys.js.map