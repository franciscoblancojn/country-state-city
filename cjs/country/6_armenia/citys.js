"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./992_aragatsotn/citys");
const citys_2 = require("./993_ararat/citys");
const citys_3 = require("./994_armavir/citys");
const citys_4 = require("./995_gegharkunik/citys");
const citys_5 = require("./996_kotayk/citys");
const citys_6 = require("./997_lorri/citys");
const citys_7 = require("./998_shirak/citys");
const citys_8 = require("./999_syunik/citys");
const citys_9 = require("./1000_tavush/citys");
const citys_10 = require("./1001_vayots_dzor/citys");
const citys_11 = require("./1002_yerevan/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
];
//# sourceMappingURL=citys.js.map