"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1361_alba/citys");
const citys_2 = require("./1362_arad/citys");
const citys_3 = require("./1363_arges/citys");
const citys_4 = require("./1364_bacau/citys");
const citys_5 = require("./1365_bihor/citys");
const citys_6 = require("./1366_bistrita-nasaud/citys");
const citys_7 = require("./1367_botosani/citys");
const citys_8 = require("./1368_braila/citys");
const citys_9 = require("./1369_brasov/citys");
const citys_10 = require("./1370_bucuresti/citys");
const citys_11 = require("./1371_buzau/citys");
const citys_12 = require("./1372_caras-severin/citys");
const citys_13 = require("./1373_cluj/citys");
const citys_14 = require("./1374_constanta/citys");
const citys_15 = require("./1375_covasna/citys");
const citys_16 = require("./1376_dambovita/citys");
const citys_17 = require("./1377_dolj/citys");
const citys_18 = require("./1378_galati/citys");
const citys_19 = require("./1379_gorj/citys");
const citys_20 = require("./1380_harghita/citys");
const citys_21 = require("./1381_hunedoara/citys");
const citys_22 = require("./1382_ialomita/citys");
const citys_23 = require("./1383_iasi/citys");
const citys_24 = require("./1384_maramures/citys");
const citys_25 = require("./1385_mehedinti/citys");
const citys_26 = require("./1386_mures/citys");
const citys_27 = require("./1387_neamt/citys");
const citys_28 = require("./1388_olt/citys");
const citys_29 = require("./1389_prahova/citys");
const citys_30 = require("./1390_salaj/citys");
const citys_31 = require("./1391_satu_mare/citys");
const citys_32 = require("./1392_sibiu/citys");
const citys_33 = require("./1393_suceava/citys");
const citys_34 = require("./1394_teleorman/citys");
const citys_35 = require("./1395_timis/citys");
const citys_36 = require("./1396_tulcea/citys");
const citys_37 = require("./1397_vaslui/citys");
const citys_38 = require("./1398_valcea/citys");
const citys_39 = require("./1399_vrancea/citys");
const citys_40 = require("./1400_calarasi/citys");
const citys_41 = require("./1401_giurgiu/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
];
//# sourceMappingURL=citys.js.map