"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./28_abkhazia/citys");
const citys_2 = require("./29_ajaria/citys");
const citys_3 = require("./30_georgia/citys");
const citys_4 = require("./31_south_ossetia/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
];
//# sourceMappingURL=citys.js.map