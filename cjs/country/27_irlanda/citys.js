"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./2018_dublin/citys");
const citys_2 = require("./2019_galway/citys");
const citys_3 = require("./2020_kildare/citys");
const citys_4 = require("./2021_leitrim/citys");
const citys_5 = require("./2022_limerick/citys");
const citys_6 = require("./2023_mayo/citys");
const citys_7 = require("./2024_meath/citys");
const citys_8 = require("./2025_carlow/citys");
const citys_9 = require("./2026_kilkenny/citys");
const citys_10 = require("./2027_laois/citys");
const citys_11 = require("./2028_longford/citys");
const citys_12 = require("./2029_louth/citys");
const citys_13 = require("./2030_offaly/citys");
const citys_14 = require("./2031_westmeath/citys");
const citys_15 = require("./2032_wexford/citys");
const citys_16 = require("./2033_wicklow/citys");
const citys_17 = require("./2034_roscommon/citys");
const citys_18 = require("./2035_sligo/citys");
const citys_19 = require("./2036_clare/citys");
const citys_20 = require("./2037_cork/citys");
const citys_21 = require("./2038_kerry/citys");
const citys_22 = require("./2039_tipperary/citys");
const citys_23 = require("./2040_waterford/citys");
const citys_24 = require("./2041_cavan/citys");
const citys_25 = require("./2042_donegal/citys");
const citys_26 = require("./2043_monaghan/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
];
//# sourceMappingURL=citys.js.map