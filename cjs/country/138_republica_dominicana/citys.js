"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./387_santo_domingo/citys");
const citys_2 = require("./2073_duarte/citys");
const citys_3 = require("./2074_puerto_plata/citys");
const citys_4 = require("./2075_valverde/citys");
const citys_5 = require("./2076_maria_trinidad_sanchez/citys");
const citys_6 = require("./2077_azua/citys");
const citys_7 = require("./2078_santiago/citys");
const citys_8 = require("./2079_san_cristobal/citys");
const citys_9 = require("./2080_peravia/citys");
const citys_10 = require("./2081_elias_pina/citys");
const citys_11 = require("./2082_barahona/citys");
const citys_12 = require("./2083_monte_plata/citys");
const citys_13 = require("./2084_salcedo/citys");
const citys_14 = require("./2085_la_altagracia/citys");
const citys_15 = require("./2086_san_juan/citys");
const citys_16 = require("./2087_monsenor_nouel/citys");
const citys_17 = require("./2088_monte_cristi/citys");
const citys_18 = require("./2089_espaillat/citys");
const citys_19 = require("./2090_sanchez_ramirez/citys");
const citys_20 = require("./2091_la_vega/citys");
const citys_21 = require("./2092_san_pedro_de_macoris/citys");
const citys_22 = require("./2093_independencia/citys");
const citys_23 = require("./2094_dajabon/citys");
const citys_24 = require("./2095_baoruco/citys");
const citys_25 = require("./2096_el_seibo/citys");
const citys_26 = require("./2097_hato_mayor/citys");
const citys_27 = require("./2098_la_romana/citys");
const citys_28 = require("./2099_pedernales/citys");
const citys_29 = require("./2100_samana/citys");
const citys_30 = require("./2101_santiago_rodriguez/citys");
const citys_31 = require("./2102_san_jose_de_ocoa/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
];
//# sourceMappingURL=citys.js.map