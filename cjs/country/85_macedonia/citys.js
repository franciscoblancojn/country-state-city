"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1167_aracinovo/citys");
const citys_2 = require("./1168_bac/citys");
const citys_3 = require("./1169_belcista/citys");
const citys_4 = require("./1170_berovo/citys");
const citys_5 = require("./1171_bistrica/citys");
const citys_6 = require("./1172_bitola/citys");
const citys_7 = require("./1173_blatec/citys");
const citys_8 = require("./1174_bogdanci/citys");
const citys_9 = require("./1175_bogomila/citys");
const citys_10 = require("./1176_bogovinje/citys");
const citys_11 = require("./1177_bosilovo/citys");
const citys_12 = require("./1179_cair/citys");
const citys_13 = require("./1180_capari/citys");
const citys_14 = require("./1181_caska/citys");
const citys_15 = require("./1182_cegrane/citys");
const citys_16 = require("./1184_centar_zupa/citys");
const citys_17 = require("./1187_debar/citys");
const citys_18 = require("./1188_delcevo/citys");
const citys_19 = require("./1190_demir_hisar/citys");
const citys_20 = require("./1191_demir_kapija/citys");
const citys_21 = require("./1195_dorce_petrov/citys");
const citys_22 = require("./1198_gazi_baba/citys");
const citys_23 = require("./1199_gevgelija/citys");
const citys_24 = require("./1200_gostivar/citys");
const citys_25 = require("./1201_gradsko/citys");
const citys_26 = require("./1204_jegunovce/citys");
const citys_27 = require("./1205_kamenjane/citys");
const citys_28 = require("./1207_karpos/citys");
const citys_29 = require("./1208_kavadarci/citys");
const citys_30 = require("./1209_kicevo/citys");
const citys_31 = require("./1210_kisela_voda/citys");
const citys_32 = require("./1211_klecevce/citys");
const citys_33 = require("./1212_kocani/citys");
const citys_34 = require("./1214_kondovo/citys");
const citys_35 = require("./1217_kratovo/citys");
const citys_36 = require("./1219_krivogastani/citys");
const citys_37 = require("./1220_krusevo/citys");
const citys_38 = require("./1223_kumanovo/citys");
const citys_39 = require("./1224_labunista/citys");
const citys_40 = require("./1225_lipkovo/citys");
const citys_41 = require("./1228_makedonska_kamenica/citys");
const citys_42 = require("./1229_makedonski_brod/citys");
const citys_43 = require("./1234_murtino/citys");
const citys_44 = require("./1235_negotino/citys");
const citys_45 = require("./1238_novo_selo/citys");
const citys_46 = require("./1240_ohrid/citys");
const citys_47 = require("./1242_orizari/citys");
const citys_48 = require("./1245_petrovec/citys");
const citys_49 = require("./1248_prilep/citys");
const citys_50 = require("./1249_probistip/citys");
const citys_51 = require("./1250_radovis/citys");
const citys_52 = require("./1252_resen/citys");
const citys_53 = require("./1253_rosoman/citys");
const citys_54 = require("./1256_saraj/citys");
const citys_55 = require("./1260_srbinovo/citys");
const citys_56 = require("./1262_star_dojran/citys");
const citys_57 = require("./1264_stip/citys");
const citys_58 = require("./1265_struga/citys");
const citys_59 = require("./1266_strumica/citys");
const citys_60 = require("./1267_studenicani/citys");
const citys_61 = require("./1268_suto_orizari/citys");
const citys_62 = require("./1269_sveti_nikole/citys");
const citys_63 = require("./1270_tearce/citys");
const citys_64 = require("./1271_tetovo/citys");
const citys_65 = require("./1273_valandovo/citys");
const citys_66 = require("./1275_veles/citys");
const citys_67 = require("./1277_vevcani/citys");
const citys_68 = require("./1278_vinica/citys");
const citys_69 = require("./1281_vrapciste/citys");
const citys_70 = require("./1286_zelino/citys");
const citys_71 = require("./1289_zrnovci/citys");
const citys_72 = require("./2106_skopje/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
    ...citys_38.citys,
    ...citys_39.citys,
    ...citys_40.citys,
    ...citys_41.citys,
    ...citys_42.citys,
    ...citys_43.citys,
    ...citys_44.citys,
    ...citys_45.citys,
    ...citys_46.citys,
    ...citys_47.citys,
    ...citys_48.citys,
    ...citys_49.citys,
    ...citys_50.citys,
    ...citys_51.citys,
    ...citys_52.citys,
    ...citys_53.citys,
    ...citys_54.citys,
    ...citys_55.citys,
    ...citys_56.citys,
    ...citys_57.citys,
    ...citys_58.citys,
    ...citys_59.citys,
    ...citys_60.citys,
    ...citys_61.citys,
    ...citys_62.citys,
    ...citys_63.citys,
    ...citys_64.citys,
    ...citys_65.citys,
    ...citys_66.citys,
    ...citys_67.citys,
    ...citys_68.citys,
    ...citys_69.citys,
    ...citys_70.citys,
    ...citys_71.citys,
    ...citys_72.citys,
];
//# sourceMappingURL=citys.js.map