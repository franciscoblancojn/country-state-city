"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./104_issik_kulskaya_region/citys");
const citys_2 = require("./105_kyrgyzstan/citys");
const citys_3 = require("./106_narinskaya_region/citys");
const citys_4 = require("./107_oshskaya_region/citys");
const citys_5 = require("./108_tallaskaya_region/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
];
//# sourceMappingURL=citys.js.map