"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./940_arhus/citys");
const citys_2 = require("./941_bornholm/citys");
const citys_3 = require("./942_frederiksborg/citys");
const citys_4 = require("./943_fyn/citys");
const citys_5 = require("./944_kobenhavn/citys");
const citys_6 = require("./945_staden_kobenhavn/citys");
const citys_7 = require("./946_nordjylland/citys");
const citys_8 = require("./947_ribe/citys");
const citys_9 = require("./948_ringkobing/citys");
const citys_10 = require("./949_roskilde/citys");
const citys_11 = require("./950_sonderjylland/citys");
const citys_12 = require("./951_storstrom/citys");
const citys_13 = require("./952_vejle/citys");
const citys_14 = require("./953_vestsjalland/citys");
const citys_15 = require("./954_viborg/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
];
//# sourceMappingURL=citys.js.map