"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1886_pinar_del_rio/citys");
const citys_2 = require("./1887_ciudad_de_la_habana/citys");
const citys_3 = require("./1888_matanzas/citys");
const citys_4 = require("./1889_isla_de_la_juventud/citys");
const citys_5 = require("./1890_camaguey/citys");
const citys_6 = require("./1891_ciego_de_avila/citys");
const citys_7 = require("./1892_cienfuegos/citys");
const citys_8 = require("./1893_granma/citys");
const citys_9 = require("./1894_guantanamo/citys");
const citys_10 = require("./1895_la_habana/citys");
const citys_11 = require("./1896_holguin/citys");
const citys_12 = require("./1897_las_tunas/citys");
const citys_13 = require("./1898_sancti_spiritus/citys");
const citys_14 = require("./1899_santiago_de_cuba/citys");
const citys_15 = require("./1900_villa_clara/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
];
//# sourceMappingURL=citys.js.map