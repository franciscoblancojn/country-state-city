"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1662_valparaiso/citys");
const citys_2 = require("./1663_aisen_del_general_carlos_ibanez_del_campo/citys");
const citys_3 = require("./1664_antofagasta/citys");
const citys_4 = require("./1665_araucania/citys");
const citys_5 = require("./1666_atacama/citys");
const citys_6 = require("./1667_bio-bio/citys");
const citys_7 = require("./1668_coquimbo/citys");
const citys_8 = require("./1669_libertador_general_bernardo_ohiggins/citys");
const citys_9 = require("./1670_los_lagos/citys");
const citys_10 = require("./1671_magallanes_y_de_la_antartica_chilena/citys");
const citys_11 = require("./1672_maule/citys");
const citys_12 = require("./1673_region_metropolitana/citys");
const citys_13 = require("./1674_tarapaca/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
];
//# sourceMappingURL=citys.js.map