"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./206_ahuachapan/citys");
const citys_2 = require("./207_cuscatlan/citys");
const citys_3 = require("./208_la_libertad/citys");
const citys_4 = require("./209_la_paz/citys");
const citys_5 = require("./210_la_union/citys");
const citys_6 = require("./211_san_miguel/citys");
const citys_7 = require("./212_san_salvador/citys");
const citys_8 = require("./213_santa_ana/citys");
const citys_9 = require("./214_sonsonate/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
];
//# sourceMappingURL=citys.js.map