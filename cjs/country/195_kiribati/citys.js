"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./2006_gilbert_islands/citys");
const citys_2 = require("./2007_line_islands/citys");
const citys_3 = require("./2008_phoenix_islands/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys, ...citys_3.citys];
//# sourceMappingURL=citys.js.map