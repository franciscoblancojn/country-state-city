"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./227_bartin/citys");
const citys_2 = require("./228_bayburt/citys");
const citys_3 = require("./229_karabuk/citys");
const citys_4 = require("./230_adana/citys");
const citys_5 = require("./231_aydin/citys");
const citys_6 = require("./232_amasya/citys");
const citys_7 = require("./233_ankara/citys");
const citys_8 = require("./234_antalya/citys");
const citys_9 = require("./235_artvin/citys");
const citys_10 = require("./236_afion/citys");
const citys_11 = require("./237_balikesir/citys");
const citys_12 = require("./238_bilecik/citys");
const citys_13 = require("./239_bursa/citys");
const citys_14 = require("./240_gaziantep/citys");
const citys_15 = require("./241_denizli/citys");
const citys_16 = require("./242_izmir/citys");
const citys_17 = require("./243_isparta/citys");
const citys_18 = require("./244_icel/citys");
const citys_19 = require("./245_kayseri/citys");
const citys_20 = require("./246_kars/citys");
const citys_21 = require("./247_kodjaeli/citys");
const citys_22 = require("./248_konya/citys");
const citys_23 = require("./249_kirklareli/citys");
const citys_24 = require("./250_kutahya/citys");
const citys_25 = require("./251_malatya/citys");
const citys_26 = require("./252_manisa/citys");
const citys_27 = require("./253_sakarya/citys");
const citys_28 = require("./254_samsun/citys");
const citys_29 = require("./255_sivas/citys");
const citys_30 = require("./256_istanbul/citys");
const citys_31 = require("./257_trabzon/citys");
const citys_32 = require("./258_corum/citys");
const citys_33 = require("./259_edirne/citys");
const citys_34 = require("./260_elazig/citys");
const citys_35 = require("./261_erzincan/citys");
const citys_36 = require("./262_erzurum/citys");
const citys_37 = require("./263_eskisehir/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
    ...citys_32.citys,
    ...citys_33.citys,
    ...citys_34.citys,
    ...citys_35.citys,
    ...citys_36.citys,
    ...citys_37.citys,
];
//# sourceMappingURL=citys.js.map