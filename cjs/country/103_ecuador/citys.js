"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1795_galapagos/citys");
const citys_2 = require("./1796_azuay/citys");
const citys_3 = require("./1797_bolivar/citys");
const citys_4 = require("./1798_canar/citys");
const citys_5 = require("./1799_carchi/citys");
const citys_6 = require("./1800_chimborazo/citys");
const citys_7 = require("./1801_cotopaxi/citys");
const citys_8 = require("./1802_el_oro/citys");
const citys_9 = require("./1803_esmeraldas/citys");
const citys_10 = require("./1804_guayas/citys");
const citys_11 = require("./1805_imbabura/citys");
const citys_12 = require("./1806_loja/citys");
const citys_13 = require("./1807_los_rios/citys");
const citys_14 = require("./1808_manabi/citys");
const citys_15 = require("./1809_morona-santiago/citys");
const citys_16 = require("./1810_pastaza/citys");
const citys_17 = require("./1811_pichincha/citys");
const citys_18 = require("./1812_tungurahua/citys");
const citys_19 = require("./1813_zamora-chinchipe/citys");
const citys_20 = require("./1814_sucumbios/citys");
const citys_21 = require("./1815_napo/citys");
const citys_22 = require("./1816_orellana/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
];
//# sourceMappingURL=citys.js.map