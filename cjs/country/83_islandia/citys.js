"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1930_akureyri/citys");
const citys_2 = require("./1931_arnessysla/citys");
const citys_3 = require("./1932_austur-bardastrandarsysla/citys");
const citys_4 = require("./1933_austur-hunavatnssysla/citys");
const citys_5 = require("./1934_austur-skaftafellssysla/citys");
const citys_6 = require("./1935_borgarfjardarsysla/citys");
const citys_7 = require("./1936_dalasysla/citys");
const citys_8 = require("./1937_eyjafjardarsysla/citys");
const citys_9 = require("./1938_gullbringusysla/citys");
const citys_10 = require("./1939_hafnarfjordur/citys");
const citys_11 = require("./1943_kjosarsysla/citys");
const citys_12 = require("./1944_kopavogur/citys");
const citys_13 = require("./1945_myrasysla/citys");
const citys_14 = require("./1946_neskaupstadur/citys");
const citys_15 = require("./1947_nordur-isafjardarsysla/citys");
const citys_16 = require("./1948_nordur-mulasysla/citys");
const citys_17 = require("./1949_nordur-tingeyjarsysla/citys");
const citys_18 = require("./1950_olafsfjordur/citys");
const citys_19 = require("./1951_rangarvallasysla/citys");
const citys_20 = require("./1952_reykjavik/citys");
const citys_21 = require("./1953_saudarkrokur/citys");
const citys_22 = require("./1954_seydisfjordur/citys");
const citys_23 = require("./1956_skagafjardarsysla/citys");
const citys_24 = require("./1957_snafellsnes-_og_hnappadalssysla/citys");
const citys_25 = require("./1958_strandasysla/citys");
const citys_26 = require("./1959_sudur-mulasysla/citys");
const citys_27 = require("./1960_sudur-tingeyjarsysla/citys");
const citys_28 = require("./1961_vestmannaeyjar/citys");
const citys_29 = require("./1962_vestur-bardastrandarsysla/citys");
const citys_30 = require("./1964_vestur-isafjardarsysla/citys");
const citys_31 = require("./1965_vestur-skaftafellssysla/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
    ...citys_19.citys,
    ...citys_20.citys,
    ...citys_21.citys,
    ...citys_22.citys,
    ...citys_23.citys,
    ...citys_24.citys,
    ...citys_25.citys,
    ...citys_26.citys,
    ...citys_27.citys,
    ...citys_28.citys,
    ...citys_29.citys,
    ...citys_30.citys,
    ...citys_31.citys,
];
//# sourceMappingURL=citys.js.map