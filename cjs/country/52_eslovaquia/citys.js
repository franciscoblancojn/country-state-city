"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1413_banska_bystrica/citys");
const citys_2 = require("./1414_bratislava/citys");
const citys_3 = require("./1415_kosice/citys");
const citys_4 = require("./1416_nitra/citys");
const citys_5 = require("./1417_presov/citys");
const citys_6 = require("./1418_trencin/citys");
const citys_7 = require("./1419_trnava/citys");
const citys_8 = require("./1420_zilina/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
];
//# sourceMappingURL=citys.js.map