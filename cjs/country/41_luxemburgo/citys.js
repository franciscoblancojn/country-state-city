"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1163_diekirch/citys");
const citys_2 = require("./1164_grevenmacher/citys");
const citys_3 = require("./1165_luxembourg/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys, ...citys_3.citys];
//# sourceMappingURL=citys.js.map