"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1766_bocas_del_toro/citys");
const citys_2 = require("./1767_chiriqui/citys");
const citys_3 = require("./1768_cocle/citys");
const citys_4 = require("./1769_colon/citys");
const citys_5 = require("./1770_darien/citys");
const citys_6 = require("./1771_herrera/citys");
const citys_7 = require("./1772_los_santos/citys");
const citys_8 = require("./1773_panama/citys");
const citys_9 = require("./1774_san_blas/citys");
const citys_10 = require("./1775_veraguas/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
];
//# sourceMappingURL=citys.js.map