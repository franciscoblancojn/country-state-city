"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./102_government_controlled_area/citys");
const citys_2 = require("./103_turkish_controlled_area/citys");
exports.citys = [...citys_1.citys, ...citys_2.citys];
//# sourceMappingURL=citys.js.map