"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./1622_alto_parana/citys");
const citys_2 = require("./1623_amambay/citys");
const citys_3 = require("./1624_boqueron/citys");
const citys_4 = require("./1625_caaguazu/citys");
const citys_5 = require("./1626_caazapa/citys");
const citys_6 = require("./1627_central/citys");
const citys_7 = require("./1628_concepcion/citys");
const citys_8 = require("./1629_cordillera/citys");
const citys_9 = require("./1630_guaira/citys");
const citys_10 = require("./1631_itapua/citys");
const citys_11 = require("./1632_misiones/citys");
const citys_12 = require("./1633_neembucu/citys");
const citys_13 = require("./1634_paraguari/citys");
const citys_14 = require("./1635_presidente_hayes/citys");
const citys_15 = require("./1636_san_pedro/citys");
const citys_16 = require("./1637_alto_paraguay/citys");
const citys_17 = require("./1638_canindeyu/citys");
const citys_18 = require("./1639_chaco/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
    ...citys_17.citys,
    ...citys_18.citys,
];
//# sourceMappingURL=citys.js.map