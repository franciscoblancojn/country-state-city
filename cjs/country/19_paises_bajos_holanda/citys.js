"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./2060_drenthe/citys");
const citys_2 = require("./2061_friesland/citys");
const citys_3 = require("./2062_gelderland/citys");
const citys_4 = require("./2063_groningen/citys");
const citys_5 = require("./2064_limburg/citys");
const citys_6 = require("./2065_noord-brabant/citys");
const citys_7 = require("./2066_noord-holland/citys");
const citys_8 = require("./2067_utrecht/citys");
const citys_9 = require("./2068_zeeland/citys");
const citys_10 = require("./2069_zuid-holland/citys");
const citys_11 = require("./2071_overijssel/citys");
const citys_12 = require("./2072_flevoland/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
];
//# sourceMappingURL=citys.js.map