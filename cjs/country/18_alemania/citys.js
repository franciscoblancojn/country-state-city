"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.citys = void 0;
const citys_1 = require("./832_brandenburg/citys");
const citys_2 = require("./833_baden-wurttemberg/citys");
const citys_3 = require("./834_bayern/citys");
const citys_4 = require("./835_hessen/citys");
const citys_5 = require("./836_hamburg/citys");
const citys_6 = require("./837_mecklenburg-vorpommern/citys");
const citys_7 = require("./838_niedersachsen/citys");
const citys_8 = require("./839_nordrhein-westfalen/citys");
const citys_9 = require("./840_rheinland-pfalz/citys");
const citys_10 = require("./841_schleswig-holstein/citys");
const citys_11 = require("./842_sachsen/citys");
const citys_12 = require("./843_sachsen-anhalt/citys");
const citys_13 = require("./844_thuringen/citys");
const citys_14 = require("./845_berlin/citys");
const citys_15 = require("./846_bremen/citys");
const citys_16 = require("./847_saarland/citys");
exports.citys = [
    ...citys_1.citys,
    ...citys_2.citys,
    ...citys_3.citys,
    ...citys_4.citys,
    ...citys_5.citys,
    ...citys_6.citys,
    ...citys_7.citys,
    ...citys_8.citys,
    ...citys_9.citys,
    ...citys_10.citys,
    ...citys_11.citys,
    ...citys_12.citys,
    ...citys_13.citys,
    ...citys_14.citys,
    ...citys_15.citys,
    ...citys_16.citys,
];
//# sourceMappingURL=citys.js.map