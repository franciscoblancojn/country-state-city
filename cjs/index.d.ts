export interface countryProps {
    id: number;
    text: string;
    code: string;
    img?: string;
}
export declare const loadCountrys: () => Promise<countryProps[]>;
export declare const loadCountrysWidthImg: () => Promise<countryProps[]>;
export interface stateProps {
    id: number;
    text: string;
    id_country: number;
}
export declare const loadStates: () => Promise<stateProps[]>;
export interface cityProps {
    id: number;
    text: string;
    id_state: number;
    id_country: number;
    nameAve?: string | undefined;
}
export declare const loadCitys: () => Promise<cityProps[]>;
export declare const parseNameFolder: (e: {
    text: string;
    id: number;
}) => string;
export declare const getRuteDir: () => string;
export declare const getRuteCountrys: () => string;
export declare const getRuteCountrysWidthImg: () => string;
export declare const getRuteStates: () => string;
export declare const getRuteStatesByCountry: (country: {
    text: string;
    id: number;
}) => string;
export declare const getRuteCitys: () => string;
export declare const getRuteCitysByStateAndCountry: (country: {
    text: string;
    id: number;
}, state: {
    text: string;
    id: number;
}) => string;
