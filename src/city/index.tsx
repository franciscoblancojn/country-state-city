import { citys as citys_144 } from "../country/144_afganistan/citys";
import { citys as citys_114 } from "../country/114_albania/citys";
import { citys as citys_18 } from "../country/18_alemania/citys";
import { citys as citys_98 } from "../country/98_argelia/citys";
import { citys as citys_145 } from "../country/145_andorra/citys";
import { citys as citys_119 } from "../country/119_angola/citys";
import { citys as citys_4 } from "../country/4_anguila/citys";
import { citys as citys_147 } from "../country/147_antigua_y_barbuda/citys";
import { citys as citys_207 } from "../country/207_antillas_holandesas/citys";
import { citys as citys_91 } from "../country/91_arabia_saudita/citys";
import { citys as citys_5 } from "../country/5_argentina/citys";
import { citys as citys_6 } from "../country/6_armenia/citys";
import { citys as citys_142 } from "../country/142_aruba/citys";
import { citys as citys_1 } from "../country/1_australia/citys";
import { citys as citys_2 } from "../country/2_austria/citys";
import { citys as citys_3 } from "../country/3_azerbaiyan/citys";
import { citys as citys_80 } from "../country/80_bahamas/citys";
import { citys as citys_127 } from "../country/127_barein/citys";
import { citys as citys_149 } from "../country/149_bangladesh/citys";
import { citys as citys_128 } from "../country/128_barbados/citys";
import { citys as citys_9 } from "../country/9_belgica/citys";
import { citys as citys_8 } from "../country/8_belice/citys";
import { citys as citys_151 } from "../country/151_benin/citys";
import { citys as citys_10 } from "../country/10_bermudas/citys";
import { citys as citys_7 } from "../country/7_bielorrusia/citys";
import { citys as citys_123 } from "../country/123_bolivia/citys";
import { citys as citys_79 } from "../country/79_bosnia_y_herzegovina/citys";
import { citys as citys_100 } from "../country/100_botsuana/citys";
import { citys as citys_12 } from "../country/12_brasil/citys";
import { citys as citys_155 } from "../country/155_brunei/citys";
import { citys as citys_11 } from "../country/11_bulgaria/citys";
import { citys as citys_156 } from "../country/156_burkina_faso/citys";
import { citys as citys_157 } from "../country/157_burundi/citys";
import { citys as citys_152 } from "../country/152_butan/citys";
import { citys as citys_159 } from "../country/159_cabo_verde/citys";
import { citys as citys_158 } from "../country/158_camboya/citys";
import { citys as citys_31 } from "../country/31_camerun/citys";
import { citys as citys_32 } from "../country/32_canada/citys";
import { citys as citys_130 } from "../country/130_chad/citys";
import { citys as citys_81 } from "../country/81_chile/citys";
import { citys as citys_35 } from "../country/35_china/citys";
import { citys as citys_33 } from "../country/33_chipre/citys";
import { citys as citys_82 } from "../country/82_colombia/citys";
import { citys as citys_164 } from "../country/164_comores/citys";
import { citys as citys_112 } from "../country/112_congo_(brazzaville)/citys";
import { citys as citys_165 } from "../country/165_congo_(kinshasa)/citys";
import { citys as citys_166 } from "../country/166_cook_islas/citys";
import { citys as citys_84 } from "../country/84_corea_del_norte/citys";
import { citys as citys_69 } from "../country/69_corea_del_sur/citys";
import { citys as citys_168 } from "../country/168_costa_de_marfil/citys";
import { citys as citys_36 } from "../country/36_costa_rica/citys";
import { citys as citys_71 } from "../country/71_croacia/citys";
import { citys as citys_113 } from "../country/113_cuba/citys";
import { citys as citys_22 } from "../country/22_dinamarca/citys";
import { citys as citys_169 } from "../country/169_djibouti_yibuti/citys";
import { citys as citys_103 } from "../country/103_ecuador/citys";
import { citys as citys_23 } from "../country/23_egipto/citys";
import { citys as citys_51 } from "../country/51_el_salvador/citys";
import { citys as citys_93 } from "../country/93_emiratos_arabes_unidos/citys";
import { citys as citys_173 } from "../country/173_eritrea/citys";
import { citys as citys_52 } from "../country/52_eslovaquia/citys";
import { citys as citys_53 } from "../country/53_eslovenia/citys";
import { citys as citys_28 } from "../country/28_espana/citys";
import { citys as citys_55 } from "../country/55_estados_unidos/citys";
import { citys as citys_68 } from "../country/68_estonia/citys";
import { citys as citys_121 } from "../country/121_etiopia/citys";
import { citys as citys_175 } from "../country/175_feroe_islas/citys";
import { citys as citys_90 } from "../country/90_filipinas/citys";
import { citys as citys_63 } from "../country/63_finlandia/citys";
import { citys as citys_176 } from "../country/176_fiyi/citys";
import { citys as citys_64 } from "../country/64_francia/citys";
import { citys as citys_180 } from "../country/180_gabon/citys";
import { citys as citys_181 } from "../country/181_gambia/citys";
import { citys as citys_21 } from "../country/21_georgia/citys";
import { citys as citys_105 } from "../country/105_ghana/citys";
import { citys as citys_143 } from "../country/143_gibraltar/citys";
import { citys as citys_184 } from "../country/184_granada/citys";
import { citys as citys_20 } from "../country/20_grecia/citys";
import { citys as citys_94 } from "../country/94_groenlandia/citys";
import { citys as citys_17 } from "../country/17_guadalupe/citys";
import { citys as citys_185 } from "../country/185_guatemala/citys";
import { citys as citys_186 } from "../country/186_guernsey/citys";
import { citys as citys_187 } from "../country/187_guinea/citys";
import { citys as citys_172 } from "../country/172_guinea_ecuatorial/citys";
import { citys as citys_188 } from "../country/188_guinea-bissau/citys";
import { citys as citys_189 } from "../country/189_guyana/citys";
import { citys as citys_16 } from "../country/16_haiti/citys";
import { citys as citys_137 } from "../country/137_honduras/citys";
import { citys as citys_73 } from "../country/73_hong_kong/citys";
import { citys as citys_14 } from "../country/14_hungria/citys";
import { citys as citys_25 } from "../country/25_india/citys";
import { citys as citys_74 } from "../country/74_indonesia/citys";
import { citys as citys_140 } from "../country/140_irak/citys";
import { citys as citys_26 } from "../country/26_iran/citys";
import { citys as citys_27 } from "../country/27_irlanda/citys";
import { citys as citys_215 } from "../country/215_isla_pitcairn/citys";
import { citys as citys_83 } from "../country/83_islandia/citys";
import { citys as citys_228 } from "../country/228_islas_salomon/citys";
import { citys as citys_58 } from "../country/58_islas_turcas_y_caicos/citys";
import { citys as citys_154 } from "../country/154_islas_virgenes_britanicas/citys";
import { citys as citys_24 } from "../country/24_israel/citys";
import { citys as citys_29 } from "../country/29_italia/citys";
import { citys as citys_132 } from "../country/132_jamaica/citys";
import { citys as citys_70 } from "../country/70_japon/citys";
import { citys as citys_193 } from "../country/193_jersey/citys";
import { citys as citys_75 } from "../country/75_jordania/citys";
import { citys as citys_30 } from "../country/30_kazajstan/citys";
import { citys as citys_97 } from "../country/97_kenia/citys";
import { citys as citys_34 } from "../country/34_kirguistan/citys";
import { citys as citys_195 } from "../country/195_kiribati/citys";
import { citys as citys_37 } from "../country/37_kuwait/citys";
import { citys as citys_196 } from "../country/196_laos/citys";
import { citys as citys_197 } from "../country/197_lesotho/citys";
import { citys as citys_38 } from "../country/38_letonia/citys";
import { citys as citys_99 } from "../country/99_libano/citys";
import { citys as citys_198 } from "../country/198_liberia/citys";
import { citys as citys_39 } from "../country/39_libia/citys";
import { citys as citys_126 } from "../country/126_liechtenstein/citys";
import { citys as citys_40 } from "../country/40_lituania/citys";
import { citys as citys_41 } from "../country/41_luxemburgo/citys";
import { citys as citys_85 } from "../country/85_macedonia/citys";
import { citys as citys_134 } from "../country/134_madagascar/citys";
import { citys as citys_76 } from "../country/76_malasia/citys";
import { citys as citys_125 } from "../country/125_malaui/citys";
import { citys as citys_200 } from "../country/200_maldivas/citys";
import { citys as citys_133 } from "../country/133_mali/citys";
import { citys as citys_86 } from "../country/86_malta/citys";
import { citys as citys_131 } from "../country/131_isla_de_man/citys";
import { citys as citys_104 } from "../country/104_marruecos/citys";
import { citys as citys_201 } from "../country/201_martinica/citys";
import { citys as citys_202 } from "../country/202_mauricio/citys";
import { citys as citys_108 } from "../country/108_mauritania/citys";
import { citys as citys_42 } from "../country/42_mexico/citys";
import { citys as citys_43 } from "../country/43_moldavia/citys";
import { citys as citys_44 } from "../country/44_monaco/citys";
import { citys as citys_139 } from "../country/139_mongolia/citys";
import { citys as citys_117 } from "../country/117_mozambique/citys";
import { citys as citys_205 } from "../country/205_myanmar/citys";
import { citys as citys_102 } from "../country/102_namibia/citys";
import { citys as citys_206 } from "../country/206_nauru/citys";
import { citys as citys_107 } from "../country/107_nepal/citys";
import { citys as citys_209 } from "../country/209_nicaragua/citys";
import { citys as citys_210 } from "../country/210_niger/citys";
import { citys as citys_115 } from "../country/115_nigeria/citys";
import { citys as citys_212 } from "../country/212_norfolk_island/citys";
import { citys as citys_46 } from "../country/46_noruega/citys";
import { citys as citys_208 } from "../country/208_nueva_caledonia/citys";
import { citys as citys_45 } from "../country/45_nueva_zelanda/citys";
import { citys as citys_213 } from "../country/213_oman/citys";
import { citys as citys_19 } from "../country/19_paises_bajos_holanda/citys";
import { citys as citys_87 } from "../country/87_pakistan/citys";
import { citys as citys_124 } from "../country/124_panama/citys";
import { citys as citys_88 } from "../country/88_papua-nueva_guinea/citys";
import { citys as citys_110 } from "../country/110_paraguay/citys";
import { citys as citys_89 } from "../country/89_peru/citys";
import { citys as citys_178 } from "../country/178_polinesia_francesa/citys";
import { citys as citys_47 } from "../country/47_polonia/citys";
import { citys as citys_48 } from "../country/48_portugal/citys";
import { citys as citys_246 } from "../country/246_puerto_rico/citys";
import { citys as citys_216 } from "../country/216_qatar/citys";
import { citys as citys_13 } from "../country/13_reino_unido/citys";
import { citys as citys_65 } from "../country/65_republica_checa/citys";
import { citys as citys_138 } from "../country/138_republica_dominicana/citys";
import { citys as citys_49 } from "../country/49_reunion/citys";
import { citys as citys_217 } from "../country/217_ruanda/citys";
import { citys as citys_72 } from "../country/72_rumania/citys";
import { citys as citys_50 } from "../country/50_rusia/citys";
import { citys as citys_242 } from "../country/242_sahara_occidental/citys";
import { citys as citys_223 } from "../country/223_samoa/citys";
import { citys as citys_219 } from "../country/219_san_cristobal_y_nieves/citys";
import { citys as citys_224 } from "../country/224_san_marino/citys";
import { citys as citys_221 } from "../country/221_san_pedro_y_miquelon/citys";
import { citys as citys_225 } from "../country/225_san_tome_y_principe/citys";
import { citys as citys_222 } from "../country/222_san_vicente_y_las_granadinas/citys";
import { citys as citys_218 } from "../country/218_santa_elena/citys";
import { citys as citys_220 } from "../country/220_santa_lucia/citys";
import { citys as citys_135 } from "../country/135_senegal/citys";
import { citys as citys_226 } from "../country/226_serbia_y_montenegro/citys";
import { citys as citys_109 } from "../country/109_seychelles/citys";
import { citys as citys_227 } from "../country/227_sierra_leona/citys";
import { citys as citys_77 } from "../country/77_singapur/citys";
import { citys as citys_106 } from "../country/106_siria/citys";
import { citys as citys_229 } from "../country/229_somalia/citys";
import { citys as citys_120 } from "../country/120_sri_lanka/citys";
import { citys as citys_141 } from "../country/141_sudafrica/citys";
import { citys as citys_232 } from "../country/232_sudan/citys";
import { citys as citys_67 } from "../country/67_suecia/citys";
import { citys as citys_66 } from "../country/66_suiza/citys";
import { citys as citys_54 } from "../country/54_surinam/citys";
import { citys as citys_234 } from "../country/234_suazilandia/citys";
import { citys as citys_56 } from "../country/56_tayikistan/citys";
import { citys as citys_92 } from "../country/92_tailandia/citys";
import { citys as citys_78 } from "../country/78_taiwan/citys";
import { citys as citys_101 } from "../country/101_tanzania/citys";
import { citys as citys_171 } from "../country/171_timor_oriental/citys";
import { citys as citys_136 } from "../country/136_togo/citys";
import { citys as citys_235 } from "../country/235_tokelau/citys";
import { citys as citys_236 } from "../country/236_tonga/citys";
import { citys as citys_237 } from "../country/237_trinidad_y_tobago/citys";
import { citys as citys_122 } from "../country/122_tunez/citys";
import { citys as citys_57 } from "../country/57_turkmenistan/citys";
import { citys as citys_59 } from "../country/59_turquia/citys";
import { citys as citys_239 } from "../country/239_tuvalu/citys";
import { citys as citys_62 } from "../country/62_ucrania/citys";
import { citys as citys_60 } from "../country/60_uganda/citys";
import { citys as citys_111 } from "../country/111_uruguay/citys";
import { citys as citys_61 } from "../country/61_uzbekistan/citys";
import { citys as citys_240 } from "../country/240_vanuatu/citys";
import { citys as citys_95 } from "../country/95_venezuela/citys";
import { citys as citys_15 } from "../country/15_vietnam/citys";
import { citys as citys_241 } from "../country/241_wallis_y_futuna/citys";
import { citys as citys_243 } from "../country/243_yemen/citys";
import { citys as citys_116 } from "../country/116_zambia/citys";
import { citys as citys_96 } from "../country/96_zimbabue/citys";
export const citys = [
    ...citys_144,
    ...citys_114,
    ...citys_18,
    ...citys_98,
    ...citys_145,
    ...citys_119,
    ...citys_4,
    ...citys_147,
    ...citys_207,
    ...citys_91,
    ...citys_5,
    ...citys_6,
    ...citys_142,
    ...citys_1,
    ...citys_2,
    ...citys_3,
    ...citys_80,
    ...citys_127,
    ...citys_149,
    ...citys_128,
    ...citys_9,
    ...citys_8,
    ...citys_151,
    ...citys_10,
    ...citys_7,
    ...citys_123,
    ...citys_79,
    ...citys_100,
    ...citys_12,
    ...citys_155,
    ...citys_11,
    ...citys_156,
    ...citys_157,
    ...citys_152,
    ...citys_159,
    ...citys_158,
    ...citys_31,
    ...citys_32,
    ...citys_130,
    ...citys_81,
    ...citys_35,
    ...citys_33,
    ...citys_82,
    ...citys_164,
    ...citys_112,
    ...citys_165,
    ...citys_166,
    ...citys_84,
    ...citys_69,
    ...citys_168,
    ...citys_36,
    ...citys_71,
    ...citys_113,
    ...citys_22,
    ...citys_169,
    ...citys_103,
    ...citys_23,
    ...citys_51,
    ...citys_93,
    ...citys_173,
    ...citys_52,
    ...citys_53,
    ...citys_28,
    ...citys_55,
    ...citys_68,
    ...citys_121,
    ...citys_175,
    ...citys_90,
    ...citys_63,
    ...citys_176,
    ...citys_64,
    ...citys_180,
    ...citys_181,
    ...citys_21,
    ...citys_105,
    ...citys_143,
    ...citys_184,
    ...citys_20,
    ...citys_94,
    ...citys_17,
    ...citys_185,
    ...citys_186,
    ...citys_187,
    ...citys_172,
    ...citys_188,
    ...citys_189,
    ...citys_16,
    ...citys_137,
    ...citys_73,
    ...citys_14,
    ...citys_25,
    ...citys_74,
    ...citys_140,
    ...citys_26,
    ...citys_27,
    ...citys_215,
    ...citys_83,
    ...citys_228,
    ...citys_58,
    ...citys_154,
    ...citys_24,
    ...citys_29,
    ...citys_132,
    ...citys_70,
    ...citys_193,
    ...citys_75,
    ...citys_30,
    ...citys_97,
    ...citys_34,
    ...citys_195,
    ...citys_37,
    ...citys_196,
    ...citys_197,
    ...citys_38,
    ...citys_99,
    ...citys_198,
    ...citys_39,
    ...citys_126,
    ...citys_40,
    ...citys_41,
    ...citys_85,
    ...citys_134,
    ...citys_76,
    ...citys_125,
    ...citys_200,
    ...citys_133,
    ...citys_86,
    ...citys_131,
    ...citys_104,
    ...citys_201,
    ...citys_202,
    ...citys_108,
    ...citys_42,
    ...citys_43,
    ...citys_44,
    ...citys_139,
    ...citys_117,
    ...citys_205,
    ...citys_102,
    ...citys_206,
    ...citys_107,
    ...citys_209,
    ...citys_210,
    ...citys_115,
    ...citys_212,
    ...citys_46,
    ...citys_208,
    ...citys_45,
    ...citys_213,
    ...citys_19,
    ...citys_87,
    ...citys_124,
    ...citys_88,
    ...citys_110,
    ...citys_89,
    ...citys_178,
    ...citys_47,
    ...citys_48,
    ...citys_246,
    ...citys_216,
    ...citys_13,
    ...citys_65,
    ...citys_138,
    ...citys_49,
    ...citys_217,
    ...citys_72,
    ...citys_50,
    ...citys_242,
    ...citys_223,
    ...citys_219,
    ...citys_224,
    ...citys_221,
    ...citys_225,
    ...citys_222,
    ...citys_218,
    ...citys_220,
    ...citys_135,
    ...citys_226,
    ...citys_109,
    ...citys_227,
    ...citys_77,
    ...citys_106,
    ...citys_229,
    ...citys_120,
    ...citys_141,
    ...citys_232,
    ...citys_67,
    ...citys_66,
    ...citys_54,
    ...citys_234,
    ...citys_56,
    ...citys_92,
    ...citys_78,
    ...citys_101,
    ...citys_171,
    ...citys_136,
    ...citys_235,
    ...citys_236,
    ...citys_237,
    ...citys_122,
    ...citys_57,
    ...citys_59,
    ...citys_239,
    ...citys_62,
    ...citys_60,
    ...citys_111,
    ...citys_61,
    ...citys_240,
    ...citys_95,
    ...citys_15,
    ...citys_241,
    ...citys_243,
    ...citys_116,
    ...citys_96,
];
