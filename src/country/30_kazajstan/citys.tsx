import { citys as citys_30_81 } from "./81_aktyubinskaya_obl./citys";
import { citys as citys_30_82 } from "./82_alma-atinskaya_obl./citys";
import { citys as citys_30_83 } from "./83_vostochno-kazahstanskaya_obl./citys";
import { citys as citys_30_84 } from "./84_gurevskaya_obl./citys";
import { citys as citys_30_85 } from "./85_zhambylskaya_obl._(dzhambulskaya_obl.)/citys";
import { citys as citys_30_86 } from "./86_dzhezkazganskaya_obl./citys";
import { citys as citys_30_87 } from "./87_karagandinskaya_obl./citys";
import { citys as citys_30_88 } from "./88_kzyl-ordinskaya_obl./citys";
import { citys as citys_30_89 } from "./89_kokchetavskaya_obl./citys";
import { citys as citys_30_90 } from "./90_kustanaiskaya_obl./citys";
import { citys as citys_30_91 } from "./91_mangystauskaya_(mangyshlakskaya_obl.)/citys";
import { citys as citys_30_92 } from "./92_pavlodarskaya_obl./citys";
import { citys as citys_30_93 } from "./93_severo-kazahstanskaya_obl./citys";
import { citys as citys_30_94 } from "./94_taldy-kurganskaya_obl./citys";
import { citys as citys_30_95 } from "./95_turgaiskaya_obl./citys";
import { citys as citys_30_96 } from "./96_akmolinskaya_obl._(tselinogradskaya_obl.)/citys";
import { citys as citys_30_97 } from "./97_chimkentskaya_obl./citys";
import { citys as citys_30_374 } from "./374_kazahstan/citys";
import { citys as citys_30_380 } from "./380_zapadno-kazahstanskaya_obl./citys";
export const citys = [
    ...citys_30_81,
    ...citys_30_82,
    ...citys_30_83,
    ...citys_30_84,
    ...citys_30_85,
    ...citys_30_86,
    ...citys_30_87,
    ...citys_30_88,
    ...citys_30_89,
    ...citys_30_90,
    ...citys_30_91,
    ...citys_30_92,
    ...citys_30_93,
    ...citys_30_94,
    ...citys_30_95,
    ...citys_30_96,
    ...citys_30_97,
    ...citys_30_374,
    ...citys_30_380,
];
