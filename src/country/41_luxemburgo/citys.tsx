import { citys as citys_41_1163 } from "./1163_diekirch/citys";
import { citys as citys_41_1164 } from "./1164_grevenmacher/citys";
import { citys as citys_41_1165 } from "./1165_luxembourg/citys";
export const citys = [...citys_41_1163, ...citys_41_1164, ...citys_41_1165];
