import { citys as citys_45_116 } from "./116_auckland/citys";
import { citys as citys_45_117 } from "./117_bay_of_plenty/citys";
import { citys as citys_45_118 } from "./118_canterbury/citys";
import { citys as citys_45_119 } from "./119_gisborne/citys";
import { citys as citys_45_120 } from "./120_hawkes_bay/citys";
import { citys as citys_45_121 } from "./121_manawatu-wanganui/citys";
import { citys as citys_45_122 } from "./122_marlborough/citys";
import { citys as citys_45_123 } from "./123_nelson/citys";
import { citys as citys_45_124 } from "./124_northland/citys";
import { citys as citys_45_125 } from "./125_otago/citys";
import { citys as citys_45_126 } from "./126_southland/citys";
import { citys as citys_45_127 } from "./127_taranaki/citys";
import { citys as citys_45_128 } from "./128_tasman/citys";
import { citys as citys_45_129 } from "./129_waikato/citys";
import { citys as citys_45_130 } from "./130_wellington/citys";
import { citys as citys_45_131 } from "./131_west_coast/citys";
export const citys = [
    ...citys_45_116,
    ...citys_45_117,
    ...citys_45_118,
    ...citys_45_119,
    ...citys_45_120,
    ...citys_45_121,
    ...citys_45_122,
    ...citys_45_123,
    ...citys_45_124,
    ...citys_45_125,
    ...citys_45_126,
    ...citys_45_127,
    ...citys_45_128,
    ...citys_45_129,
    ...citys_45_130,
    ...citys_45_131,
];
