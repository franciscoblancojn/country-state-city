import { citys as citys_5_1818 } from "./1818_buenos_aires/citys";
import { citys as citys_5_1819 } from "./1819_catamarca/citys";
import { citys as citys_5_1820 } from "./1820_chaco/citys";
import { citys as citys_5_1821 } from "./1821_chubut/citys";
import { citys as citys_5_1822 } from "./1822_cordoba/citys";
import { citys as citys_5_1823 } from "./1823_corrientes/citys";
import { citys as citys_5_1824 } from "./1824_distrito_federal/citys";
import { citys as citys_5_1825 } from "./1825_entre_rios/citys";
import { citys as citys_5_1826 } from "./1826_formosa/citys";
import { citys as citys_5_1827 } from "./1827_jujuy/citys";
import { citys as citys_5_1828 } from "./1828_la_pampa/citys";
import { citys as citys_5_1829 } from "./1829_la_rioja/citys";
import { citys as citys_5_1830 } from "./1830_mendoza/citys";
import { citys as citys_5_1831 } from "./1831_misiones/citys";
import { citys as citys_5_1832 } from "./1832_neuquen/citys";
import { citys as citys_5_1833 } from "./1833_rio_negro/citys";
import { citys as citys_5_1834 } from "./1834_salta/citys";
import { citys as citys_5_1835 } from "./1835_san_juan/citys";
import { citys as citys_5_1836 } from "./1836_san_luis/citys";
import { citys as citys_5_1837 } from "./1837_santa_cruz/citys";
import { citys as citys_5_1838 } from "./1838_santa_fe/citys";
import { citys as citys_5_1839 } from "./1839_santiago_del_estero/citys";
import { citys as citys_5_1840 } from "./1840_tierra_del_fuego/citys";
import { citys as citys_5_1841 } from "./1841_tucuman/citys";
export const citys = [
    ...citys_5_1818,
    ...citys_5_1819,
    ...citys_5_1820,
    ...citys_5_1821,
    ...citys_5_1822,
    ...citys_5_1823,
    ...citys_5_1824,
    ...citys_5_1825,
    ...citys_5_1826,
    ...citys_5_1827,
    ...citys_5_1828,
    ...citys_5_1829,
    ...citys_5_1830,
    ...citys_5_1831,
    ...citys_5_1832,
    ...citys_5_1833,
    ...citys_5_1834,
    ...citys_5_1835,
    ...citys_5_1836,
    ...citys_5_1837,
    ...citys_5_1838,
    ...citys_5_1839,
    ...citys_5_1840,
    ...citys_5_1841,
];
