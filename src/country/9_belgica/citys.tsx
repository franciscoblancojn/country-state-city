import { citys as citys_9_874 } from "./874_bruxelles/citys";
import { citys as citys_9_875 } from "./875_west-vlaanderen/citys";
import { citys as citys_9_876 } from "./876_oost-vlaanderen/citys";
import { citys as citys_9_877 } from "./877_limburg/citys";
import { citys as citys_9_878 } from "./878_vlaams_brabant/citys";
import { citys as citys_9_879 } from "./879_antwerpen/citys";
import { citys as citys_9_880 } from "./880_lia„a�ge/citys";
import { citys as citys_9_881 } from "./881_namur/citys";
import { citys as citys_9_882 } from "./882_hainaut/citys";
import { citys as citys_9_883 } from "./883_luxembourg/citys";
import { citys as citys_9_884 } from "./884_brabant_wallon/citys";
export const citys = [
    ...citys_9_874,
    ...citys_9_875,
    ...citys_9_876,
    ...citys_9_877,
    ...citys_9_878,
    ...citys_9_879,
    ...citys_9_880,
    ...citys_9_881,
    ...citys_9_882,
    ...citys_9_883,
    ...citys_9_884,
];
