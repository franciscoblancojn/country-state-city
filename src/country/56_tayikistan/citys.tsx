import { citys as citys_56_216 } from "./216_gorno-badakhshan_region/citys";
import { citys as citys_56_217 } from "./217_kuljabsk_region/citys";
import { citys as citys_56_218 } from "./218_kurgan-tjube_region/citys";
import { citys as citys_56_219 } from "./219_sughd_region/citys";
import { citys as citys_56_220 } from "./220_tajikistan/citys";
export const citys = [
    ...citys_56_216,
    ...citys_56_217,
    ...citys_56_218,
    ...citys_56_219,
    ...citys_56_220,
];
