import { citys as citys_18_832 } from "./832_brandenburg/citys";
import { citys as citys_18_833 } from "./833_baden-wurttemberg/citys";
import { citys as citys_18_834 } from "./834_bayern/citys";
import { citys as citys_18_835 } from "./835_hessen/citys";
import { citys as citys_18_836 } from "./836_hamburg/citys";
import { citys as citys_18_837 } from "./837_mecklenburg-vorpommern/citys";
import { citys as citys_18_838 } from "./838_niedersachsen/citys";
import { citys as citys_18_839 } from "./839_nordrhein-westfalen/citys";
import { citys as citys_18_840 } from "./840_rheinland-pfalz/citys";
import { citys as citys_18_841 } from "./841_schleswig-holstein/citys";
import { citys as citys_18_842 } from "./842_sachsen/citys";
import { citys as citys_18_843 } from "./843_sachsen-anhalt/citys";
import { citys as citys_18_844 } from "./844_thuringen/citys";
import { citys as citys_18_845 } from "./845_berlin/citys";
import { citys as citys_18_846 } from "./846_bremen/citys";
import { citys as citys_18_847 } from "./847_saarland/citys";
export const citys = [
    ...citys_18_832,
    ...citys_18_833,
    ...citys_18_834,
    ...citys_18_835,
    ...citys_18_836,
    ...citys_18_837,
    ...citys_18_838,
    ...citys_18_839,
    ...citys_18_840,
    ...citys_18_841,
    ...citys_18_842,
    ...citys_18_843,
    ...citys_18_844,
    ...citys_18_845,
    ...citys_18_846,
    ...citys_18_847,
];
