import { citys as citys_27_2018 } from "./2018_dublin/citys";
import { citys as citys_27_2019 } from "./2019_galway/citys";
import { citys as citys_27_2020 } from "./2020_kildare/citys";
import { citys as citys_27_2021 } from "./2021_leitrim/citys";
import { citys as citys_27_2022 } from "./2022_limerick/citys";
import { citys as citys_27_2023 } from "./2023_mayo/citys";
import { citys as citys_27_2024 } from "./2024_meath/citys";
import { citys as citys_27_2025 } from "./2025_carlow/citys";
import { citys as citys_27_2026 } from "./2026_kilkenny/citys";
import { citys as citys_27_2027 } from "./2027_laois/citys";
import { citys as citys_27_2028 } from "./2028_longford/citys";
import { citys as citys_27_2029 } from "./2029_louth/citys";
import { citys as citys_27_2030 } from "./2030_offaly/citys";
import { citys as citys_27_2031 } from "./2031_westmeath/citys";
import { citys as citys_27_2032 } from "./2032_wexford/citys";
import { citys as citys_27_2033 } from "./2033_wicklow/citys";
import { citys as citys_27_2034 } from "./2034_roscommon/citys";
import { citys as citys_27_2035 } from "./2035_sligo/citys";
import { citys as citys_27_2036 } from "./2036_clare/citys";
import { citys as citys_27_2037 } from "./2037_cork/citys";
import { citys as citys_27_2038 } from "./2038_kerry/citys";
import { citys as citys_27_2039 } from "./2039_tipperary/citys";
import { citys as citys_27_2040 } from "./2040_waterford/citys";
import { citys as citys_27_2041 } from "./2041_cavan/citys";
import { citys as citys_27_2042 } from "./2042_donegal/citys";
import { citys as citys_27_2043 } from "./2043_monaghan/citys";
export const citys = [
    ...citys_27_2018,
    ...citys_27_2019,
    ...citys_27_2020,
    ...citys_27_2021,
    ...citys_27_2022,
    ...citys_27_2023,
    ...citys_27_2024,
    ...citys_27_2025,
    ...citys_27_2026,
    ...citys_27_2027,
    ...citys_27_2028,
    ...citys_27_2029,
    ...citys_27_2030,
    ...citys_27_2031,
    ...citys_27_2032,
    ...citys_27_2033,
    ...citys_27_2034,
    ...citys_27_2035,
    ...citys_27_2036,
    ...citys_27_2037,
    ...citys_27_2038,
    ...citys_27_2039,
    ...citys_27_2040,
    ...citys_27_2041,
    ...citys_27_2042,
    ...citys_27_2043,
];
