import { citys as citys_195_2006 } from "./2006_gilbert_islands/citys";
import { citys as citys_195_2007 } from "./2007_line_islands/citys";
import { citys as citys_195_2008 } from "./2008_phoenix_islands/citys";
export const citys = [...citys_195_2006, ...citys_195_2007, ...citys_195_2008];
