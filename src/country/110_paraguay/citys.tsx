import { citys as citys_110_1622 } from "./1622_alto_parana/citys";
import { citys as citys_110_1623 } from "./1623_amambay/citys";
import { citys as citys_110_1624 } from "./1624_boqueron/citys";
import { citys as citys_110_1625 } from "./1625_caaguazu/citys";
import { citys as citys_110_1626 } from "./1626_caazapa/citys";
import { citys as citys_110_1627 } from "./1627_central/citys";
import { citys as citys_110_1628 } from "./1628_concepcion/citys";
import { citys as citys_110_1629 } from "./1629_cordillera/citys";
import { citys as citys_110_1630 } from "./1630_guaira/citys";
import { citys as citys_110_1631 } from "./1631_itapua/citys";
import { citys as citys_110_1632 } from "./1632_misiones/citys";
import { citys as citys_110_1633 } from "./1633_neembucu/citys";
import { citys as citys_110_1634 } from "./1634_paraguari/citys";
import { citys as citys_110_1635 } from "./1635_presidente_hayes/citys";
import { citys as citys_110_1636 } from "./1636_san_pedro/citys";
import { citys as citys_110_1637 } from "./1637_alto_paraguay/citys";
import { citys as citys_110_1638 } from "./1638_canindeyu/citys";
import { citys as citys_110_1639 } from "./1639_chaco/citys";
export const citys = [
    ...citys_110_1622,
    ...citys_110_1623,
    ...citys_110_1624,
    ...citys_110_1625,
    ...citys_110_1626,
    ...citys_110_1627,
    ...citys_110_1628,
    ...citys_110_1629,
    ...citys_110_1630,
    ...citys_110_1631,
    ...citys_110_1632,
    ...citys_110_1633,
    ...citys_110_1634,
    ...citys_110_1635,
    ...citys_110_1636,
    ...citys_110_1637,
    ...citys_110_1638,
    ...citys_110_1639,
];
