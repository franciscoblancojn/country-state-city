import { citys as citys_25_44 } from "./44_bangala/citys";
import { citys as citys_25_45 } from "./45_chhattisgarh/citys";
import { citys as citys_25_46 } from "./46_karnataka/citys";
import { citys as citys_25_47 } from "./47_uttaranchal/citys";
import { citys as citys_25_48 } from "./48_andhara_pradesh/citys";
import { citys as citys_25_49 } from "./49_assam/citys";
import { citys as citys_25_50 } from "./50_bihar/citys";
import { citys as citys_25_51 } from "./51_gujarat/citys";
import { citys as citys_25_52 } from "./52_jammu_and_kashmir/citys";
import { citys as citys_25_53 } from "./53_kerala/citys";
import { citys as citys_25_54 } from "./54_madhya_pradesh/citys";
import { citys as citys_25_55 } from "./55_manipur/citys";
import { citys as citys_25_56 } from "./56_maharashtra/citys";
import { citys as citys_25_57 } from "./57_megahalaya/citys";
import { citys as citys_25_58 } from "./58_orissa/citys";
import { citys as citys_25_59 } from "./59_punjab/citys";
import { citys as citys_25_60 } from "./60_pondisheri/citys";
import { citys as citys_25_61 } from "./61_rajasthan/citys";
import { citys as citys_25_62 } from "./62_tamil_nadu/citys";
import { citys as citys_25_63 } from "./63_tripura/citys";
import { citys as citys_25_64 } from "./64_uttar_pradesh/citys";
import { citys as citys_25_65 } from "./65_haryana/citys";
import { citys as citys_25_66 } from "./66_chandigarh/citys";
import { citys as citys_25_376 } from "./376_india/citys";
import { citys as citys_25_2201 } from "./2201_delhi/citys";
export const citys = [
    ...citys_25_44,
    ...citys_25_45,
    ...citys_25_46,
    ...citys_25_47,
    ...citys_25_48,
    ...citys_25_49,
    ...citys_25_50,
    ...citys_25_51,
    ...citys_25_52,
    ...citys_25_53,
    ...citys_25_54,
    ...citys_25_55,
    ...citys_25_56,
    ...citys_25_57,
    ...citys_25_58,
    ...citys_25_59,
    ...citys_25_60,
    ...citys_25_61,
    ...citys_25_62,
    ...citys_25_63,
    ...citys_25_64,
    ...citys_25_65,
    ...citys_25_66,
    ...citys_25_376,
    ...citys_25_2201,
];
