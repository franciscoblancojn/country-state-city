import { citys as citys_42_1733 } from "./1733_aguascalientes/citys";
import { citys as citys_42_1734 } from "./1734_baja_california/citys";
import { citys as citys_42_1735 } from "./1735_baja_california_sur/citys";
import { citys as citys_42_1736 } from "./1736_campeche/citys";
import { citys as citys_42_1737 } from "./1737_chiapas/citys";
import { citys as citys_42_1738 } from "./1738_chihuahua/citys";
import { citys as citys_42_1739 } from "./1739_coahuila_de_zaragoza/citys";
import { citys as citys_42_1740 } from "./1740_colima/citys";
import { citys as citys_42_1741 } from "./1741_distrito_federal/citys";
import { citys as citys_42_1742 } from "./1742_durango/citys";
import { citys as citys_42_1743 } from "./1743_guanajuato/citys";
import { citys as citys_42_1744 } from "./1744_guerrero/citys";
import { citys as citys_42_1745 } from "./1745_hidalgo/citys";
import { citys as citys_42_1746 } from "./1746_jalisco/citys";
import { citys as citys_42_1747 } from "./1747_mexico/citys";
import { citys as citys_42_1748 } from "./1748_michoacan_de_ocampo/citys";
import { citys as citys_42_1749 } from "./1749_morelos/citys";
import { citys as citys_42_1750 } from "./1750_nayarit/citys";
import { citys as citys_42_1751 } from "./1751_nuevo_leon/citys";
import { citys as citys_42_1752 } from "./1752_oaxaca/citys";
import { citys as citys_42_1753 } from "./1753_puebla/citys";
import { citys as citys_42_1754 } from "./1754_queretaro_de_arteaga/citys";
import { citys as citys_42_1755 } from "./1755_quintana_roo/citys";
import { citys as citys_42_1756 } from "./1756_san_luis_potosi/citys";
import { citys as citys_42_1757 } from "./1757_sinaloa/citys";
import { citys as citys_42_1758 } from "./1758_sonora/citys";
import { citys as citys_42_1759 } from "./1759_tabasco/citys";
import { citys as citys_42_1760 } from "./1760_tamaulipas/citys";
import { citys as citys_42_1761 } from "./1761_tlaxcala/citys";
import { citys as citys_42_1762 } from "./1762_veracruz-llave/citys";
import { citys as citys_42_1763 } from "./1763_yucatan/citys";
import { citys as citys_42_1764 } from "./1764_zacatecas/citys";
export const citys = [
    ...citys_42_1733,
    ...citys_42_1734,
    ...citys_42_1735,
    ...citys_42_1736,
    ...citys_42_1737,
    ...citys_42_1738,
    ...citys_42_1739,
    ...citys_42_1740,
    ...citys_42_1741,
    ...citys_42_1742,
    ...citys_42_1743,
    ...citys_42_1744,
    ...citys_42_1745,
    ...citys_42_1746,
    ...citys_42_1747,
    ...citys_42_1748,
    ...citys_42_1749,
    ...citys_42_1750,
    ...citys_42_1751,
    ...citys_42_1752,
    ...citys_42_1753,
    ...citys_42_1754,
    ...citys_42_1755,
    ...citys_42_1756,
    ...citys_42_1757,
    ...citys_42_1758,
    ...citys_42_1759,
    ...citys_42_1760,
    ...citys_42_1761,
    ...citys_42_1762,
    ...citys_42_1763,
    ...citys_42_1764,
];
