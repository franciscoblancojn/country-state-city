import { citys as citys_36_1787 } from "./1787_alajuela/citys";
import { citys as citys_36_1788 } from "./1788_cartago/citys";
import { citys as citys_36_1789 } from "./1789_guanacaste/citys";
import { citys as citys_36_1790 } from "./1790_heredia/citys";
import { citys as citys_36_1791 } from "./1791_limon/citys";
import { citys as citys_36_1792 } from "./1792_puntarenas/citys";
import { citys as citys_36_1793 } from "./1793_san_jose/citys";
export const citys = [
    ...citys_36_1787,
    ...citys_36_1788,
    ...citys_36_1789,
    ...citys_36_1790,
    ...citys_36_1791,
    ...citys_36_1792,
    ...citys_36_1793,
];
