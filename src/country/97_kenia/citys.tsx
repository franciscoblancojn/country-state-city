import { citys as citys_97_1997 } from "./1997_central/citys";
import { citys as citys_97_1998 } from "./1998_coast/citys";
import { citys as citys_97_1999 } from "./1999_eastern/citys";
import { citys as citys_97_2000 } from "./2000_nairobi_area/citys";
import { citys as citys_97_2001 } from "./2001_north-eastern/citys";
import { citys as citys_97_2002 } from "./2002_nyanza/citys";
import { citys as citys_97_2003 } from "./2003_rift_valley/citys";
import { citys as citys_97_2004 } from "./2004_western/citys";
export const citys = [
    ...citys_97_1997,
    ...citys_97_1998,
    ...citys_97_1999,
    ...citys_97_2000,
    ...citys_97_2001,
    ...citys_97_2002,
    ...citys_97_2003,
    ...citys_97_2004,
];
