import { citys as citys_6_992 } from "./992_aragatsotn/citys";
import { citys as citys_6_993 } from "./993_ararat/citys";
import { citys as citys_6_994 } from "./994_armavir/citys";
import { citys as citys_6_995 } from "./995_gegharkunik/citys";
import { citys as citys_6_996 } from "./996_kotayk/citys";
import { citys as citys_6_997 } from "./997_lorri/citys";
import { citys as citys_6_998 } from "./998_shirak/citys";
import { citys as citys_6_999 } from "./999_syunik/citys";
import { citys as citys_6_1000 } from "./1000_tavush/citys";
import { citys as citys_6_1001 } from "./1001_vayots_dzor/citys";
import { citys as citys_6_1002 } from "./1002_yerevan/citys";
export const citys = [
    ...citys_6_992,
    ...citys_6_993,
    ...citys_6_994,
    ...citys_6_995,
    ...citys_6_996,
    ...citys_6_997,
    ...citys_6_998,
    ...citys_6_999,
    ...citys_6_1000,
    ...citys_6_1001,
    ...citys_6_1002,
];
