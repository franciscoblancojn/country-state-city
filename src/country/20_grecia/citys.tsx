import { citys as citys_20_1060 } from "./1060_evros/citys";
import { citys as citys_20_1061 } from "./1061_rodhopi/citys";
import { citys as citys_20_1062 } from "./1062_xanthi/citys";
import { citys as citys_20_1063 } from "./1063_drama/citys";
import { citys as citys_20_1064 } from "./1064_serrai/citys";
import { citys as citys_20_1065 } from "./1065_kilkis/citys";
import { citys as citys_20_1066 } from "./1066_pella/citys";
import { citys as citys_20_1067 } from "./1067_florina/citys";
import { citys as citys_20_1068 } from "./1068_kastoria/citys";
import { citys as citys_20_1069 } from "./1069_grevena/citys";
import { citys as citys_20_1070 } from "./1070_kozani/citys";
import { citys as citys_20_1071 } from "./1071_imathia/citys";
import { citys as citys_20_1072 } from "./1072_thessaloniki/citys";
import { citys as citys_20_1073 } from "./1073_kavala/citys";
import { citys as citys_20_1074 } from "./1074_khalkidhiki/citys";
import { citys as citys_20_1075 } from "./1075_pieria/citys";
import { citys as citys_20_1076 } from "./1076_ioannina/citys";
import { citys as citys_20_1077 } from "./1077_thesprotia/citys";
import { citys as citys_20_1078 } from "./1078_preveza/citys";
import { citys as citys_20_1079 } from "./1079_arta/citys";
import { citys as citys_20_1080 } from "./1080_larisa/citys";
import { citys as citys_20_1081 } from "./1081_trikala/citys";
import { citys as citys_20_1082 } from "./1082_kardhitsa/citys";
import { citys as citys_20_1083 } from "./1083_magnisia/citys";
import { citys as citys_20_1084 } from "./1084_kerkira/citys";
import { citys as citys_20_1085 } from "./1085_levkas/citys";
import { citys as citys_20_1086 } from "./1086_kefallinia/citys";
import { citys as citys_20_1087 } from "./1087_zakinthos/citys";
import { citys as citys_20_1088 } from "./1088_fthiotis/citys";
import { citys as citys_20_1089 } from "./1089_evritania/citys";
import { citys as citys_20_1090 } from "./1090_aitolia_kai_akarnania/citys";
import { citys as citys_20_1091 } from "./1091_fokis/citys";
import { citys as citys_20_1092 } from "./1092_voiotia/citys";
import { citys as citys_20_1093 } from "./1093_evvoia/citys";
import { citys as citys_20_1094 } from "./1094_attiki/citys";
import { citys as citys_20_1095 } from "./1095_argolis/citys";
import { citys as citys_20_1096 } from "./1096_korinthia/citys";
import { citys as citys_20_1097 } from "./1097_akhaia/citys";
import { citys as citys_20_1098 } from "./1098_ilia/citys";
import { citys as citys_20_1099 } from "./1099_messinia/citys";
import { citys as citys_20_1100 } from "./1100_arkadhia/citys";
import { citys as citys_20_1101 } from "./1101_lakonia/citys";
import { citys as citys_20_1102 } from "./1102_khania/citys";
import { citys as citys_20_1103 } from "./1103_rethimni/citys";
import { citys as citys_20_1104 } from "./1104_iraklion/citys";
import { citys as citys_20_1105 } from "./1105_lasithi/citys";
import { citys as citys_20_1106 } from "./1106_dhodhekanisos/citys";
import { citys as citys_20_1107 } from "./1107_samos/citys";
import { citys as citys_20_1108 } from "./1108_kikladhes/citys";
import { citys as citys_20_1109 } from "./1109_khios/citys";
import { citys as citys_20_1110 } from "./1110_lesvos/citys";
export const citys = [
    ...citys_20_1060,
    ...citys_20_1061,
    ...citys_20_1062,
    ...citys_20_1063,
    ...citys_20_1064,
    ...citys_20_1065,
    ...citys_20_1066,
    ...citys_20_1067,
    ...citys_20_1068,
    ...citys_20_1069,
    ...citys_20_1070,
    ...citys_20_1071,
    ...citys_20_1072,
    ...citys_20_1073,
    ...citys_20_1074,
    ...citys_20_1075,
    ...citys_20_1076,
    ...citys_20_1077,
    ...citys_20_1078,
    ...citys_20_1079,
    ...citys_20_1080,
    ...citys_20_1081,
    ...citys_20_1082,
    ...citys_20_1083,
    ...citys_20_1084,
    ...citys_20_1085,
    ...citys_20_1086,
    ...citys_20_1087,
    ...citys_20_1088,
    ...citys_20_1089,
    ...citys_20_1090,
    ...citys_20_1091,
    ...citys_20_1092,
    ...citys_20_1093,
    ...citys_20_1094,
    ...citys_20_1095,
    ...citys_20_1096,
    ...citys_20_1097,
    ...citys_20_1098,
    ...citys_20_1099,
    ...citys_20_1100,
    ...citys_20_1101,
    ...citys_20_1102,
    ...citys_20_1103,
    ...citys_20_1104,
    ...citys_20_1105,
    ...citys_20_1106,
    ...citys_20_1107,
    ...citys_20_1108,
    ...citys_20_1109,
    ...citys_20_1110,
];
