import { citys as citys_50_133 } from "./133_altaiskii_krai/citys";
import { citys as citys_50_134 } from "./134_amurskaya_obl./citys";
import { citys as citys_50_135 } from "./135_arhangelskaya_obl./citys";
import { citys as citys_50_136 } from "./136_astrahanskaya_obl./citys";
import { citys as citys_50_137 } from "./137_bashkiriya_obl./citys";
import { citys as citys_50_138 } from "./138_belgorodskaya_obl./citys";
import { citys as citys_50_139 } from "./139_bryanskaya_obl./citys";
import { citys as citys_50_140 } from "./140_buryatiya/citys";
import { citys as citys_50_141 } from "./141_vladimirskaya_obl./citys";
import { citys as citys_50_142 } from "./142_volgogradskaya_obl./citys";
import { citys as citys_50_143 } from "./143_vologodskaya_obl./citys";
import { citys as citys_50_144 } from "./144_voronezhskaya_obl./citys";
import { citys as citys_50_145 } from "./145_nizhegorodskaya_obl./citys";
import { citys as citys_50_146 } from "./146_dagestan/citys";
import { citys as citys_50_147 } from "./147_evreiskaya_obl./citys";
import { citys as citys_50_148 } from "./148_ivanovskaya_obl./citys";
import { citys as citys_50_149 } from "./149_irkutskaya_obl./citys";
import { citys as citys_50_150 } from "./150_kabardino-balkariya/citys";
import { citys as citys_50_151 } from "./151_kaliningradskaya_obl./citys";
import { citys as citys_50_152 } from "./152_tverskaya_obl./citys";
import { citys as citys_50_153 } from "./153_kalmykiya/citys";
import { citys as citys_50_154 } from "./154_kaluzhskaya_obl./citys";
import { citys as citys_50_155 } from "./155_kamchatskaya_obl./citys";
import { citys as citys_50_156 } from "./156_kareliya/citys";
import { citys as citys_50_157 } from "./157_kemerovskaya_obl./citys";
import { citys as citys_50_158 } from "./158_kirovskaya_obl./citys";
import { citys as citys_50_159 } from "./159_komi/citys";
import { citys as citys_50_160 } from "./160_kostromskaya_obl./citys";
import { citys as citys_50_161 } from "./161_krasnodarskii_krai/citys";
import { citys as citys_50_162 } from "./162_krasnoyarskii_krai/citys";
import { citys as citys_50_163 } from "./163_kurganskaya_obl./citys";
import { citys as citys_50_164 } from "./164_kurskaya_obl./citys";
import { citys as citys_50_165 } from "./165_lipetskaya_obl./citys";
import { citys as citys_50_166 } from "./166_magadanskaya_obl./citys";
import { citys as citys_50_167 } from "./167_marii_el/citys";
import { citys as citys_50_168 } from "./168_mordoviya/citys";
import { citys as citys_50_169 } from "./169_moscow_y_moscow_region/citys";
import { citys as citys_50_170 } from "./170_murmanskaya_obl./citys";
import { citys as citys_50_171 } from "./171_novgorodskaya_obl./citys";
import { citys as citys_50_172 } from "./172_novosibirskaya_obl./citys";
import { citys as citys_50_173 } from "./173_omskaya_obl./citys";
import { citys as citys_50_174 } from "./174_orenburgskaya_obl./citys";
import { citys as citys_50_175 } from "./175_orlovskaya_obl./citys";
import { citys as citys_50_176 } from "./176_penzenskaya_obl./citys";
import { citys as citys_50_177 } from "./177_permskiy_krai/citys";
import { citys as citys_50_178 } from "./178_primorskii_krai/citys";
import { citys as citys_50_179 } from "./179_pskovskaya_obl./citys";
import { citys as citys_50_180 } from "./180_rostovskaya_obl./citys";
import { citys as citys_50_181 } from "./181_ryazanskaya_obl./citys";
import { citys as citys_50_182 } from "./182_samarskaya_obl./citys";
import { citys as citys_50_183 } from "./183_saint-petersburg_and_region/citys";
import { citys as citys_50_184 } from "./184_saratovskaya_obl./citys";
import { citys as citys_50_185 } from "./185_saha_(yakutiya)/citys";
import { citys as citys_50_186 } from "./186_sahalin/citys";
import { citys as citys_50_187 } from "./187_sverdlovskaya_obl./citys";
import { citys as citys_50_188 } from "./188_severnaya_osetiya/citys";
import { citys as citys_50_189 } from "./189_smolenskaya_obl./citys";
import { citys as citys_50_190 } from "./190_stavropolskii_krai/citys";
import { citys as citys_50_191 } from "./191_tambovskaya_obl./citys";
import { citys as citys_50_192 } from "./192_tatarstan/citys";
import { citys as citys_50_193 } from "./193_tomskaya_obl./citys";
import { citys as citys_50_195 } from "./195_tulskaya_obl./citys";
import { citys as citys_50_196 } from "./196_tyumenskaya_obl._i_hanty-mansiiskii_ao/citys";
import { citys as citys_50_197 } from "./197_udmurtiya/citys";
import { citys as citys_50_198 } from "./198_ulyanovskaya_obl./citys";
import { citys as citys_50_199 } from "./199_uralskaya_obl./citys";
import { citys as citys_50_200 } from "./200_habarovskii_krai/citys";
import { citys as citys_50_201 } from "./201_chelyabinskaya_obl./citys";
import { citys as citys_50_202 } from "./202_checheno-ingushetiya/citys";
import { citys as citys_50_203 } from "./203_chitinskaya_obl./citys";
import { citys as citys_50_204 } from "./204_chuvashiya/citys";
import { citys as citys_50_205 } from "./205_yaroslavskaya_obl./citys";
import { citys as citys_50_381 } from "./381_adygeya/citys";
import { citys as citys_50_382 } from "./382_hakasiya/citys";
import { citys as citys_50_384 } from "./384_chukotskii_ao/citys";
import { citys as citys_50_396 } from "./396_yamalo-nenetskii_ao/citys";
import { citys as citys_50_2044 } from "./2044_karachaeva-cherkesskaya_respublica/citys";
import { citys as citys_50_2045 } from "./2045_raimirskii_(dolgano-nenetskii)_ao/citys";
import { citys as citys_50_2046 } from "./2046_respublica_tiva/citys";
export const citys = [
    ...citys_50_133,
    ...citys_50_134,
    ...citys_50_135,
    ...citys_50_136,
    ...citys_50_137,
    ...citys_50_138,
    ...citys_50_139,
    ...citys_50_140,
    ...citys_50_141,
    ...citys_50_142,
    ...citys_50_143,
    ...citys_50_144,
    ...citys_50_145,
    ...citys_50_146,
    ...citys_50_147,
    ...citys_50_148,
    ...citys_50_149,
    ...citys_50_150,
    ...citys_50_151,
    ...citys_50_152,
    ...citys_50_153,
    ...citys_50_154,
    ...citys_50_155,
    ...citys_50_156,
    ...citys_50_157,
    ...citys_50_158,
    ...citys_50_159,
    ...citys_50_160,
    ...citys_50_161,
    ...citys_50_162,
    ...citys_50_163,
    ...citys_50_164,
    ...citys_50_165,
    ...citys_50_166,
    ...citys_50_167,
    ...citys_50_168,
    ...citys_50_169,
    ...citys_50_170,
    ...citys_50_171,
    ...citys_50_172,
    ...citys_50_173,
    ...citys_50_174,
    ...citys_50_175,
    ...citys_50_176,
    ...citys_50_177,
    ...citys_50_178,
    ...citys_50_179,
    ...citys_50_180,
    ...citys_50_181,
    ...citys_50_182,
    ...citys_50_183,
    ...citys_50_184,
    ...citys_50_185,
    ...citys_50_186,
    ...citys_50_187,
    ...citys_50_188,
    ...citys_50_189,
    ...citys_50_190,
    ...citys_50_191,
    ...citys_50_192,
    ...citys_50_193,
    ...citys_50_195,
    ...citys_50_196,
    ...citys_50_197,
    ...citys_50_198,
    ...citys_50_199,
    ...citys_50_200,
    ...citys_50_201,
    ...citys_50_202,
    ...citys_50_203,
    ...citys_50_204,
    ...citys_50_205,
    ...citys_50_381,
    ...citys_50_382,
    ...citys_50_384,
    ...citys_50_396,
    ...citys_50_2044,
    ...citys_50_2045,
    ...citys_50_2046,
];
