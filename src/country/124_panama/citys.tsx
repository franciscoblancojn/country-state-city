import { citys as citys_124_1766 } from "./1766_bocas_del_toro/citys";
import { citys as citys_124_1767 } from "./1767_chiriqui/citys";
import { citys as citys_124_1768 } from "./1768_cocle/citys";
import { citys as citys_124_1769 } from "./1769_colon/citys";
import { citys as citys_124_1770 } from "./1770_darien/citys";
import { citys as citys_124_1771 } from "./1771_herrera/citys";
import { citys as citys_124_1772 } from "./1772_los_santos/citys";
import { citys as citys_124_1773 } from "./1773_panama/citys";
import { citys as citys_124_1774 } from "./1774_san_blas/citys";
import { citys as citys_124_1775 } from "./1775_veraguas/citys";
export const citys = [
    ...citys_124_1766,
    ...citys_124_1767,
    ...citys_124_1768,
    ...citys_124_1769,
    ...citys_124_1770,
    ...citys_124_1771,
    ...citys_124_1772,
    ...citys_124_1773,
    ...citys_124_1774,
    ...citys_124_1775,
];
