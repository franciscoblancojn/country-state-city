import { citys as citys_47_1295 } from "./1295_biala_podlaska/citys";
import { citys as citys_47_1296 } from "./1296_bialystok/citys";
import { citys as citys_47_1297 } from "./1297_bielsko/citys";
import { citys as citys_47_1298 } from "./1298_bydgoszcz/citys";
import { citys as citys_47_1299 } from "./1299_chelm/citys";
import { citys as citys_47_1300 } from "./1300_ciechanow/citys";
import { citys as citys_47_1301 } from "./1301_czestochowa/citys";
import { citys as citys_47_1302 } from "./1302_elblag/citys";
import { citys as citys_47_1303 } from "./1303_gdansk/citys";
import { citys as citys_47_1304 } from "./1304_gorzow/citys";
import { citys as citys_47_1305 } from "./1305_jelenia_gora/citys";
import { citys as citys_47_1306 } from "./1306_kalisz/citys";
import { citys as citys_47_1307 } from "./1307_katowice/citys";
import { citys as citys_47_1308 } from "./1308_kielce/citys";
import { citys as citys_47_1309 } from "./1309_konin/citys";
import { citys as citys_47_1310 } from "./1310_koszalin/citys";
import { citys as citys_47_1311 } from "./1311_krakow/citys";
import { citys as citys_47_1312 } from "./1312_krosno/citys";
import { citys as citys_47_1313 } from "./1313_legnica/citys";
import { citys as citys_47_1314 } from "./1314_leszno/citys";
import { citys as citys_47_1315 } from "./1315_lodz/citys";
import { citys as citys_47_1316 } from "./1316_lomza/citys";
import { citys as citys_47_1317 } from "./1317_lublin/citys";
import { citys as citys_47_1318 } from "./1318_nowy_sacz/citys";
import { citys as citys_47_1319 } from "./1319_olsztyn/citys";
import { citys as citys_47_1320 } from "./1320_opole/citys";
import { citys as citys_47_1321 } from "./1321_ostroleka/citys";
import { citys as citys_47_1322 } from "./1322_pila/citys";
import { citys as citys_47_1323 } from "./1323_piotrkow/citys";
import { citys as citys_47_1324 } from "./1324_plock/citys";
import { citys as citys_47_1325 } from "./1325_poznan/citys";
import { citys as citys_47_1326 } from "./1326_przemysl/citys";
import { citys as citys_47_1327 } from "./1327_radom/citys";
import { citys as citys_47_1328 } from "./1328_rzeszow/citys";
import { citys as citys_47_1329 } from "./1329_siedlce/citys";
import { citys as citys_47_1330 } from "./1330_sieradz/citys";
import { citys as citys_47_1331 } from "./1331_skierniewice/citys";
import { citys as citys_47_1332 } from "./1332_slupsk/citys";
import { citys as citys_47_1333 } from "./1333_suwalki/citys";
import { citys as citys_47_1335 } from "./1335_tarnobrzeg/citys";
import { citys as citys_47_1336 } from "./1336_tarnow/citys";
import { citys as citys_47_1337 } from "./1337_torun/citys";
import { citys as citys_47_1338 } from "./1338_walbrzych/citys";
import { citys as citys_47_1339 } from "./1339_warszawa/citys";
import { citys as citys_47_1340 } from "./1340_wloclawek/citys";
import { citys as citys_47_1341 } from "./1341_wroclaw/citys";
import { citys as citys_47_1342 } from "./1342_zamosc/citys";
import { citys as citys_47_1343 } from "./1343_zielona_gora/citys";
import { citys as citys_47_1344 } from "./1344_dolnoslaskie/citys";
import { citys as citys_47_1345 } from "./1345_kujawsko-pomorskie/citys";
import { citys as citys_47_1346 } from "./1346_lodzkie/citys";
import { citys as citys_47_1347 } from "./1347_lubelskie/citys";
import { citys as citys_47_1348 } from "./1348_lubuskie/citys";
import { citys as citys_47_1349 } from "./1349_malopolskie/citys";
import { citys as citys_47_1350 } from "./1350_mazowieckie/citys";
import { citys as citys_47_1351 } from "./1351_opolskie/citys";
import { citys as citys_47_1352 } from "./1352_podkarpackie/citys";
import { citys as citys_47_1353 } from "./1353_podlaskie/citys";
import { citys as citys_47_1354 } from "./1354_pomorskie/citys";
import { citys as citys_47_1355 } from "./1355_slaskie/citys";
import { citys as citys_47_1356 } from "./1356_swietokrzyskie/citys";
import { citys as citys_47_1357 } from "./1357_warminsko-mazurskie/citys";
import { citys as citys_47_1358 } from "./1358_wielkopolskie/citys";
import { citys as citys_47_1359 } from "./1359_zachodniopomorskie/citys";
export const citys = [
    ...citys_47_1295,
    ...citys_47_1296,
    ...citys_47_1297,
    ...citys_47_1298,
    ...citys_47_1299,
    ...citys_47_1300,
    ...citys_47_1301,
    ...citys_47_1302,
    ...citys_47_1303,
    ...citys_47_1304,
    ...citys_47_1305,
    ...citys_47_1306,
    ...citys_47_1307,
    ...citys_47_1308,
    ...citys_47_1309,
    ...citys_47_1310,
    ...citys_47_1311,
    ...citys_47_1312,
    ...citys_47_1313,
    ...citys_47_1314,
    ...citys_47_1315,
    ...citys_47_1316,
    ...citys_47_1317,
    ...citys_47_1318,
    ...citys_47_1319,
    ...citys_47_1320,
    ...citys_47_1321,
    ...citys_47_1322,
    ...citys_47_1323,
    ...citys_47_1324,
    ...citys_47_1325,
    ...citys_47_1326,
    ...citys_47_1327,
    ...citys_47_1328,
    ...citys_47_1329,
    ...citys_47_1330,
    ...citys_47_1331,
    ...citys_47_1332,
    ...citys_47_1333,
    ...citys_47_1335,
    ...citys_47_1336,
    ...citys_47_1337,
    ...citys_47_1338,
    ...citys_47_1339,
    ...citys_47_1340,
    ...citys_47_1341,
    ...citys_47_1342,
    ...citys_47_1343,
    ...citys_47_1344,
    ...citys_47_1345,
    ...citys_47_1346,
    ...citys_47_1347,
    ...citys_47_1348,
    ...citys_47_1349,
    ...citys_47_1350,
    ...citys_47_1351,
    ...citys_47_1352,
    ...citys_47_1353,
    ...citys_47_1354,
    ...citys_47_1355,
    ...citys_47_1356,
    ...citys_47_1357,
    ...citys_47_1358,
    ...citys_47_1359,
];
