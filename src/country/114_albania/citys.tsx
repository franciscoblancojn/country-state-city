import { citys as citys_114_971 } from "./971_berat/citys";
import { citys as citys_114_972 } from "./972_diber/citys";
import { citys as citys_114_973 } from "./973_durres/citys";
import { citys as citys_114_974 } from "./974_elbasan/citys";
import { citys as citys_114_975 } from "./975_fier/citys";
import { citys as citys_114_976 } from "./976_gjirokaster/citys";
import { citys as citys_114_977 } from "./977_korce/citys";
import { citys as citys_114_978 } from "./978_kukes/citys";
import { citys as citys_114_979 } from "./979_lezhe/citys";
import { citys as citys_114_980 } from "./980_shkoder/citys";
import { citys as citys_114_981 } from "./981_tirane/citys";
import { citys as citys_114_982 } from "./982_vlore/citys";
export const citys = [
    ...citys_114_971,
    ...citys_114_972,
    ...citys_114_973,
    ...citys_114_974,
    ...citys_114_975,
    ...citys_114_976,
    ...citys_114_977,
    ...citys_114_978,
    ...citys_114_979,
    ...citys_114_980,
    ...citys_114_981,
    ...citys_114_982,
];
