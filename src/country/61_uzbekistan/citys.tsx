import { citys as citys_61_266 } from "./266_andijon_region/citys";
import { citys as citys_61_267 } from "./267_buxoro_region/citys";
import { citys as citys_61_268 } from "./268_jizzac_region/citys";
import { citys as citys_61_269 } from "./269_qaraqalpaqstan/citys";
import { citys as citys_61_270 } from "./270_qashqadaryo_region/citys";
import { citys as citys_61_271 } from "./271_navoiy_region/citys";
import { citys as citys_61_272 } from "./272_namangan_region/citys";
import { citys as citys_61_273 } from "./273_samarqand_region/citys";
import { citys as citys_61_274 } from "./274_surxondaryo_region/citys";
import { citys as citys_61_275 } from "./275_sirdaryo_region/citys";
import { citys as citys_61_276 } from "./276_tashkent_region/citys";
import { citys as citys_61_277 } from "./277_fergana_region/citys";
import { citys as citys_61_278 } from "./278_xorazm_region/citys";
export const citys = [
    ...citys_61_266,
    ...citys_61_267,
    ...citys_61_268,
    ...citys_61_269,
    ...citys_61_270,
    ...citys_61_271,
    ...citys_61_272,
    ...citys_61_273,
    ...citys_61_274,
    ...citys_61_275,
    ...citys_61_276,
    ...citys_61_277,
    ...citys_61_278,
];
