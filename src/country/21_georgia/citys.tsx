import { citys as citys_21_28 } from "./28_abkhazia/citys";
import { citys as citys_21_29 } from "./29_ajaria/citys";
import { citys as citys_21_30 } from "./30_georgia/citys";
import { citys as citys_21_31 } from "./31_south_ossetia/citys";
export const citys = [
    ...citys_21_28,
    ...citys_21_29,
    ...citys_21_30,
    ...citys_21_31,
];
