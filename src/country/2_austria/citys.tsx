import { citys as citys_2_865 } from "./865_burgenland/citys";
import { citys as citys_2_866 } from "./866_karnten/citys";
import { citys as citys_2_867 } from "./867_niederosterreich/citys";
import { citys as citys_2_868 } from "./868_oberosterreich/citys";
import { citys as citys_2_869 } from "./869_salzburg/citys";
import { citys as citys_2_870 } from "./870_steiermark/citys";
import { citys as citys_2_871 } from "./871_tirol/citys";
import { citys as citys_2_872 } from "./872_vorarlberg/citys";
import { citys as citys_2_873 } from "./873_wien/citys";
export const citys = [
    ...citys_2_865,
    ...citys_2_866,
    ...citys_2_867,
    ...citys_2_868,
    ...citys_2_869,
    ...citys_2_870,
    ...citys_2_871,
    ...citys_2_872,
    ...citys_2_873,
];
