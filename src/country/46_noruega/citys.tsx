import { citys as citys_46_913 } from "./913_akershus/citys";
import { citys as citys_46_914 } from "./914_aust-agder/citys";
import { citys as citys_46_915 } from "./915_buskerud/citys";
import { citys as citys_46_916 } from "./916_finnmark/citys";
import { citys as citys_46_917 } from "./917_hedmark/citys";
import { citys as citys_46_918 } from "./918_hordaland/citys";
import { citys as citys_46_919 } from "./919_more_og_romsdal/citys";
import { citys as citys_46_920 } from "./920_nordland/citys";
import { citys as citys_46_921 } from "./921_nord-trondelag/citys";
import { citys as citys_46_922 } from "./922_oppland/citys";
import { citys as citys_46_923 } from "./923_oslo/citys";
import { citys as citys_46_924 } from "./924_ostfold/citys";
import { citys as citys_46_925 } from "./925_rogaland/citys";
import { citys as citys_46_926 } from "./926_sogn_og_fjordane/citys";
import { citys as citys_46_927 } from "./927_sor-trondelag/citys";
import { citys as citys_46_928 } from "./928_telemark/citys";
import { citys as citys_46_929 } from "./929_troms/citys";
import { citys as citys_46_930 } from "./930_vest-agder/citys";
import { citys as citys_46_931 } from "./931_vestfold/citys";
export const citys = [
    ...citys_46_913,
    ...citys_46_914,
    ...citys_46_915,
    ...citys_46_916,
    ...citys_46_917,
    ...citys_46_918,
    ...citys_46_919,
    ...citys_46_920,
    ...citys_46_921,
    ...citys_46_922,
    ...citys_46_923,
    ...citys_46_924,
    ...citys_46_925,
    ...citys_46_926,
    ...citys_46_927,
    ...citys_46_928,
    ...citys_46_929,
    ...citys_46_930,
    ...citys_46_931,
];
