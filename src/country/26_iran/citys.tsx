import { citys as citys_26_67 } from "./67_azarbayjan-e_khavari/citys";
import { citys as citys_26_68 } from "./68_esfahan/citys";
import { citys as citys_26_69 } from "./69_hamadan/citys";
import { citys as citys_26_70 } from "./70_kordestan/citys";
import { citys as citys_26_71 } from "./71_markazi/citys";
import { citys as citys_26_72 } from "./72_sistan-e_baluches/citys";
import { citys as citys_26_73 } from "./73_yazd/citys";
import { citys as citys_26_74 } from "./74_kerman/citys";
import { citys as citys_26_75 } from "./75_kermanshakhan/citys";
import { citys as citys_26_76 } from "./76_mazenderan/citys";
import { citys as citys_26_77 } from "./77_tehran/citys";
import { citys as citys_26_78 } from "./78_fars/citys";
import { citys as citys_26_79 } from "./79_horasan/citys";
import { citys as citys_26_80 } from "./80_husistan/citys";
export const citys = [
    ...citys_26_67,
    ...citys_26_68,
    ...citys_26_69,
    ...citys_26_70,
    ...citys_26_71,
    ...citys_26_72,
    ...citys_26_73,
    ...citys_26_74,
    ...citys_26_75,
    ...citys_26_76,
    ...citys_26_77,
    ...citys_26_78,
    ...citys_26_79,
    ...citys_26_80,
];
