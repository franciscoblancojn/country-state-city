import { citys as citys_33_102 } from "./102_government_controlled_area/citys";
import { citys as citys_33_103 } from "./103_turkish_controlled_area/citys";
export const citys = [...citys_33_102, ...citys_33_103];
