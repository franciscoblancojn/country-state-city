import { citys as citys_48_749 } from "./749_aveiro/citys";
import { citys as citys_48_750 } from "./750_beja/citys";
import { citys as citys_48_751 } from "./751_braga/citys";
import { citys as citys_48_752 } from "./752_braganca/citys";
import { citys as citys_48_753 } from "./753_castelo_branco/citys";
import { citys as citys_48_754 } from "./754_coimbra/citys";
import { citys as citys_48_755 } from "./755_evora/citys";
import { citys as citys_48_756 } from "./756_faro/citys";
import { citys as citys_48_757 } from "./757_madeira/citys";
import { citys as citys_48_758 } from "./758_guarda/citys";
import { citys as citys_48_759 } from "./759_leiria/citys";
import { citys as citys_48_760 } from "./760_lisboa/citys";
import { citys as citys_48_761 } from "./761_portalegre/citys";
import { citys as citys_48_762 } from "./762_porto/citys";
import { citys as citys_48_763 } from "./763_santarem/citys";
import { citys as citys_48_764 } from "./764_setubal/citys";
import { citys as citys_48_765 } from "./765_viana_do_castelo/citys";
import { citys as citys_48_766 } from "./766_vila_real/citys";
import { citys as citys_48_767 } from "./767_viseu/citys";
import { citys as citys_48_768 } from "./768_azores/citys";
export const citys = [
    ...citys_48_749,
    ...citys_48_750,
    ...citys_48_751,
    ...citys_48_752,
    ...citys_48_753,
    ...citys_48_754,
    ...citys_48_755,
    ...citys_48_756,
    ...citys_48_757,
    ...citys_48_758,
    ...citys_48_759,
    ...citys_48_760,
    ...citys_48_761,
    ...citys_48_762,
    ...citys_48_763,
    ...citys_48_764,
    ...citys_48_765,
    ...citys_48_766,
    ...citys_48_767,
    ...citys_48_768,
];
