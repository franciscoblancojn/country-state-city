import { citys as citys_13_848 } from "./848_scotland_north/citys";
import { citys as citys_13_849 } from "./849_england_-_east/citys";
import { citys as citys_13_850 } from "./850_england_-_west_midlands/citys";
import { citys as citys_13_851 } from "./851_england_-_south_west/citys";
import { citys as citys_13_852 } from "./852_england_-_north_west/citys";
import { citys as citys_13_853 } from "./853_england_-_yorks_y_humber/citys";
import { citys as citys_13_854 } from "./854_england_-_south_east/citys";
import { citys as citys_13_855 } from "./855_england_-_london/citys";
import { citys as citys_13_856 } from "./856_northern_ireland/citys";
import { citys as citys_13_857 } from "./857_england_-_north_east/citys";
import { citys as citys_13_858 } from "./858_wales_south/citys";
import { citys as citys_13_859 } from "./859_wales_north/citys";
import { citys as citys_13_860 } from "./860_england_-_east_midlands/citys";
import { citys as citys_13_861 } from "./861_scotland_central/citys";
import { citys as citys_13_862 } from "./862_scotland_south/citys";
import { citys as citys_13_863 } from "./863_channel_islands/citys";
import { citys as citys_13_864 } from "./864_isle_of_man/citys";
export const citys = [
    ...citys_13_848,
    ...citys_13_849,
    ...citys_13_850,
    ...citys_13_851,
    ...citys_13_852,
    ...citys_13_853,
    ...citys_13_854,
    ...citys_13_855,
    ...citys_13_856,
    ...citys_13_857,
    ...citys_13_858,
    ...citys_13_859,
    ...citys_13_860,
    ...citys_13_861,
    ...citys_13_862,
    ...citys_13_863,
    ...citys_13_864,
];
