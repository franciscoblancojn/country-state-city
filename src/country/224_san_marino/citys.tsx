import { citys as citys_224_1404 } from "./1404_acquaviva/citys";
import { citys as citys_224_1405 } from "./1405_chiesanuova/citys";
import { citys as citys_224_1406 } from "./1406_domagnano/citys";
import { citys as citys_224_1407 } from "./1407_faetano/citys";
import { citys as citys_224_1408 } from "./1408_fiorentino/citys";
import { citys as citys_224_1409 } from "./1409_borgo_maggiore/citys";
import { citys as citys_224_1410 } from "./1410_san_marino/citys";
import { citys as citys_224_1411 } from "./1411_monte_giardino/citys";
import { citys as citys_224_1412 } from "./1412_serravalle/citys";
export const citys = [
    ...citys_224_1404,
    ...citys_224_1405,
    ...citys_224_1406,
    ...citys_224_1407,
    ...citys_224_1408,
    ...citys_224_1409,
    ...citys_224_1410,
    ...citys_224_1411,
    ...citys_224_1412,
];
