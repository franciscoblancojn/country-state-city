import { citys as citys_31_98 } from "./98_littoral/citys";
import { citys as citys_31_99 } from "./99_southwest_region/citys";
import { citys as citys_31_100 } from "./100_north/citys";
import { citys as citys_31_101 } from "./101_central/citys";
export const citys = [
    ...citys_31_98,
    ...citys_31_99,
    ...citys_31_100,
    ...citys_31_101,
];
