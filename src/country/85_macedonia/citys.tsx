import { citys as citys_85_1167 } from "./1167_aracinovo/citys";
import { citys as citys_85_1168 } from "./1168_bac/citys";
import { citys as citys_85_1169 } from "./1169_belcista/citys";
import { citys as citys_85_1170 } from "./1170_berovo/citys";
import { citys as citys_85_1171 } from "./1171_bistrica/citys";
import { citys as citys_85_1172 } from "./1172_bitola/citys";
import { citys as citys_85_1173 } from "./1173_blatec/citys";
import { citys as citys_85_1174 } from "./1174_bogdanci/citys";
import { citys as citys_85_1175 } from "./1175_bogomila/citys";
import { citys as citys_85_1176 } from "./1176_bogovinje/citys";
import { citys as citys_85_1177 } from "./1177_bosilovo/citys";
import { citys as citys_85_1179 } from "./1179_cair/citys";
import { citys as citys_85_1180 } from "./1180_capari/citys";
import { citys as citys_85_1181 } from "./1181_caska/citys";
import { citys as citys_85_1182 } from "./1182_cegrane/citys";
import { citys as citys_85_1184 } from "./1184_centar_zupa/citys";
import { citys as citys_85_1187 } from "./1187_debar/citys";
import { citys as citys_85_1188 } from "./1188_delcevo/citys";
import { citys as citys_85_1190 } from "./1190_demir_hisar/citys";
import { citys as citys_85_1191 } from "./1191_demir_kapija/citys";
import { citys as citys_85_1195 } from "./1195_dorce_petrov/citys";
import { citys as citys_85_1198 } from "./1198_gazi_baba/citys";
import { citys as citys_85_1199 } from "./1199_gevgelija/citys";
import { citys as citys_85_1200 } from "./1200_gostivar/citys";
import { citys as citys_85_1201 } from "./1201_gradsko/citys";
import { citys as citys_85_1204 } from "./1204_jegunovce/citys";
import { citys as citys_85_1205 } from "./1205_kamenjane/citys";
import { citys as citys_85_1207 } from "./1207_karpos/citys";
import { citys as citys_85_1208 } from "./1208_kavadarci/citys";
import { citys as citys_85_1209 } from "./1209_kicevo/citys";
import { citys as citys_85_1210 } from "./1210_kisela_voda/citys";
import { citys as citys_85_1211 } from "./1211_klecevce/citys";
import { citys as citys_85_1212 } from "./1212_kocani/citys";
import { citys as citys_85_1214 } from "./1214_kondovo/citys";
import { citys as citys_85_1217 } from "./1217_kratovo/citys";
import { citys as citys_85_1219 } from "./1219_krivogastani/citys";
import { citys as citys_85_1220 } from "./1220_krusevo/citys";
import { citys as citys_85_1223 } from "./1223_kumanovo/citys";
import { citys as citys_85_1224 } from "./1224_labunista/citys";
import { citys as citys_85_1225 } from "./1225_lipkovo/citys";
import { citys as citys_85_1228 } from "./1228_makedonska_kamenica/citys";
import { citys as citys_85_1229 } from "./1229_makedonski_brod/citys";
import { citys as citys_85_1234 } from "./1234_murtino/citys";
import { citys as citys_85_1235 } from "./1235_negotino/citys";
import { citys as citys_85_1238 } from "./1238_novo_selo/citys";
import { citys as citys_85_1240 } from "./1240_ohrid/citys";
import { citys as citys_85_1242 } from "./1242_orizari/citys";
import { citys as citys_85_1245 } from "./1245_petrovec/citys";
import { citys as citys_85_1248 } from "./1248_prilep/citys";
import { citys as citys_85_1249 } from "./1249_probistip/citys";
import { citys as citys_85_1250 } from "./1250_radovis/citys";
import { citys as citys_85_1252 } from "./1252_resen/citys";
import { citys as citys_85_1253 } from "./1253_rosoman/citys";
import { citys as citys_85_1256 } from "./1256_saraj/citys";
import { citys as citys_85_1260 } from "./1260_srbinovo/citys";
import { citys as citys_85_1262 } from "./1262_star_dojran/citys";
import { citys as citys_85_1264 } from "./1264_stip/citys";
import { citys as citys_85_1265 } from "./1265_struga/citys";
import { citys as citys_85_1266 } from "./1266_strumica/citys";
import { citys as citys_85_1267 } from "./1267_studenicani/citys";
import { citys as citys_85_1268 } from "./1268_suto_orizari/citys";
import { citys as citys_85_1269 } from "./1269_sveti_nikole/citys";
import { citys as citys_85_1270 } from "./1270_tearce/citys";
import { citys as citys_85_1271 } from "./1271_tetovo/citys";
import { citys as citys_85_1273 } from "./1273_valandovo/citys";
import { citys as citys_85_1275 } from "./1275_veles/citys";
import { citys as citys_85_1277 } from "./1277_vevcani/citys";
import { citys as citys_85_1278 } from "./1278_vinica/citys";
import { citys as citys_85_1281 } from "./1281_vrapciste/citys";
import { citys as citys_85_1286 } from "./1286_zelino/citys";
import { citys as citys_85_1289 } from "./1289_zrnovci/citys";
import { citys as citys_85_2106 } from "./2106_skopje/citys";
export const citys = [
    ...citys_85_1167,
    ...citys_85_1168,
    ...citys_85_1169,
    ...citys_85_1170,
    ...citys_85_1171,
    ...citys_85_1172,
    ...citys_85_1173,
    ...citys_85_1174,
    ...citys_85_1175,
    ...citys_85_1176,
    ...citys_85_1177,
    ...citys_85_1179,
    ...citys_85_1180,
    ...citys_85_1181,
    ...citys_85_1182,
    ...citys_85_1184,
    ...citys_85_1187,
    ...citys_85_1188,
    ...citys_85_1190,
    ...citys_85_1191,
    ...citys_85_1195,
    ...citys_85_1198,
    ...citys_85_1199,
    ...citys_85_1200,
    ...citys_85_1201,
    ...citys_85_1204,
    ...citys_85_1205,
    ...citys_85_1207,
    ...citys_85_1208,
    ...citys_85_1209,
    ...citys_85_1210,
    ...citys_85_1211,
    ...citys_85_1212,
    ...citys_85_1214,
    ...citys_85_1217,
    ...citys_85_1219,
    ...citys_85_1220,
    ...citys_85_1223,
    ...citys_85_1224,
    ...citys_85_1225,
    ...citys_85_1228,
    ...citys_85_1229,
    ...citys_85_1234,
    ...citys_85_1235,
    ...citys_85_1238,
    ...citys_85_1240,
    ...citys_85_1242,
    ...citys_85_1245,
    ...citys_85_1248,
    ...citys_85_1249,
    ...citys_85_1250,
    ...citys_85_1252,
    ...citys_85_1253,
    ...citys_85_1256,
    ...citys_85_1260,
    ...citys_85_1262,
    ...citys_85_1264,
    ...citys_85_1265,
    ...citys_85_1266,
    ...citys_85_1267,
    ...citys_85_1268,
    ...citys_85_1269,
    ...citys_85_1270,
    ...citys_85_1271,
    ...citys_85_1273,
    ...citys_85_1275,
    ...citys_85_1277,
    ...citys_85_1278,
    ...citys_85_1281,
    ...citys_85_1286,
    ...citys_85_1289,
    ...citys_85_2106,
];
