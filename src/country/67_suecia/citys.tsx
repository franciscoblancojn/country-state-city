import { citys as citys_67_887 } from "./887_blekinge_lan/citys";
import { citys as citys_67_888 } from "./888_gavleborgs_lan/citys";
import { citys as citys_67_890 } from "./890_gotlands_lan/citys";
import { citys as citys_67_891 } from "./891_hallands_lan/citys";
import { citys as citys_67_892 } from "./892_jamtlands_lan/citys";
import { citys as citys_67_893 } from "./893_jonkopings_lan/citys";
import { citys as citys_67_894 } from "./894_kalmar_lan/citys";
import { citys as citys_67_895 } from "./895_dalarnas_lan/citys";
import { citys as citys_67_897 } from "./897_kronobergs_lan/citys";
import { citys as citys_67_899 } from "./899_norrbottens_lan/citys";
import { citys as citys_67_900 } from "./900_orebro_lan/citys";
import { citys as citys_67_901 } from "./901_ostergotlands_lan/citys";
import { citys as citys_67_903 } from "./903_sodermanlands_lan/citys";
import { citys as citys_67_904 } from "./904_uppsala_lan/citys";
import { citys as citys_67_905 } from "./905_varmlands_lan/citys";
import { citys as citys_67_906 } from "./906_vasterbottens_lan/citys";
import { citys as citys_67_907 } from "./907_vasternorrlands_lan/citys";
import { citys as citys_67_908 } from "./908_vastmanlands_lan/citys";
import { citys as citys_67_909 } from "./909_stockholms_lan/citys";
import { citys as citys_67_910 } from "./910_skane_lan/citys";
import { citys as citys_67_911 } from "./911_vastra_gotaland/citys";
export const citys = [
    ...citys_67_887,
    ...citys_67_888,
    ...citys_67_890,
    ...citys_67_891,
    ...citys_67_892,
    ...citys_67_893,
    ...citys_67_894,
    ...citys_67_895,
    ...citys_67_897,
    ...citys_67_899,
    ...citys_67_900,
    ...citys_67_901,
    ...citys_67_903,
    ...citys_67_904,
    ...citys_67_905,
    ...citys_67_906,
    ...citys_67_907,
    ...citys_67_908,
    ...citys_67_909,
    ...citys_67_910,
    ...citys_67_911,
];
