import { citys as citys_83_1930 } from "./1930_akureyri/citys";
import { citys as citys_83_1931 } from "./1931_arnessysla/citys";
import { citys as citys_83_1932 } from "./1932_austur-bardastrandarsysla/citys";
import { citys as citys_83_1933 } from "./1933_austur-hunavatnssysla/citys";
import { citys as citys_83_1934 } from "./1934_austur-skaftafellssysla/citys";
import { citys as citys_83_1935 } from "./1935_borgarfjardarsysla/citys";
import { citys as citys_83_1936 } from "./1936_dalasysla/citys";
import { citys as citys_83_1937 } from "./1937_eyjafjardarsysla/citys";
import { citys as citys_83_1938 } from "./1938_gullbringusysla/citys";
import { citys as citys_83_1939 } from "./1939_hafnarfjordur/citys";
import { citys as citys_83_1943 } from "./1943_kjosarsysla/citys";
import { citys as citys_83_1944 } from "./1944_kopavogur/citys";
import { citys as citys_83_1945 } from "./1945_myrasysla/citys";
import { citys as citys_83_1946 } from "./1946_neskaupstadur/citys";
import { citys as citys_83_1947 } from "./1947_nordur-isafjardarsysla/citys";
import { citys as citys_83_1948 } from "./1948_nordur-mulasysla/citys";
import { citys as citys_83_1949 } from "./1949_nordur-tingeyjarsysla/citys";
import { citys as citys_83_1950 } from "./1950_olafsfjordur/citys";
import { citys as citys_83_1951 } from "./1951_rangarvallasysla/citys";
import { citys as citys_83_1952 } from "./1952_reykjavik/citys";
import { citys as citys_83_1953 } from "./1953_saudarkrokur/citys";
import { citys as citys_83_1954 } from "./1954_seydisfjordur/citys";
import { citys as citys_83_1956 } from "./1956_skagafjardarsysla/citys";
import { citys as citys_83_1957 } from "./1957_snafellsnes-_og_hnappadalssysla/citys";
import { citys as citys_83_1958 } from "./1958_strandasysla/citys";
import { citys as citys_83_1959 } from "./1959_sudur-mulasysla/citys";
import { citys as citys_83_1960 } from "./1960_sudur-tingeyjarsysla/citys";
import { citys as citys_83_1961 } from "./1961_vestmannaeyjar/citys";
import { citys as citys_83_1962 } from "./1962_vestur-bardastrandarsysla/citys";
import { citys as citys_83_1964 } from "./1964_vestur-isafjardarsysla/citys";
import { citys as citys_83_1965 } from "./1965_vestur-skaftafellssysla/citys";
export const citys = [
    ...citys_83_1930,
    ...citys_83_1931,
    ...citys_83_1932,
    ...citys_83_1933,
    ...citys_83_1934,
    ...citys_83_1935,
    ...citys_83_1936,
    ...citys_83_1937,
    ...citys_83_1938,
    ...citys_83_1939,
    ...citys_83_1943,
    ...citys_83_1944,
    ...citys_83_1945,
    ...citys_83_1946,
    ...citys_83_1947,
    ...citys_83_1948,
    ...citys_83_1949,
    ...citys_83_1950,
    ...citys_83_1951,
    ...citys_83_1952,
    ...citys_83_1953,
    ...citys_83_1954,
    ...citys_83_1956,
    ...citys_83_1957,
    ...citys_83_1958,
    ...citys_83_1959,
    ...citys_83_1960,
    ...citys_83_1961,
    ...citys_83_1962,
    ...citys_83_1964,
    ...citys_83_1965,
];
