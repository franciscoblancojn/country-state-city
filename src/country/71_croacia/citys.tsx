import { citys as citys_71_1037 } from "./1037_bjelovarsko-bilogorska/citys";
import { citys as citys_71_1038 } from "./1038_brodsko-posavska/citys";
import { citys as citys_71_1039 } from "./1039_dubrovacko-neretvanska/citys";
import { citys as citys_71_1040 } from "./1040_istarska/citys";
import { citys as citys_71_1041 } from "./1041_karlovacka/citys";
import { citys as citys_71_1042 } from "./1042_koprivnicko-krizevacka/citys";
import { citys as citys_71_1043 } from "./1043_krapinsko-zagorska/citys";
import { citys as citys_71_1044 } from "./1044_licko-senjska/citys";
import { citys as citys_71_1045 } from "./1045_medimurska/citys";
import { citys as citys_71_1046 } from "./1046_osjecko-baranjska/citys";
import { citys as citys_71_1047 } from "./1047_pozesko-slavonska/citys";
import { citys as citys_71_1048 } from "./1048_primorsko-goranska/citys";
import { citys as citys_71_1049 } from "./1049_sibensko-kninska/citys";
import { citys as citys_71_1050 } from "./1050_sisacko-moslavacka/citys";
import { citys as citys_71_1051 } from "./1051_splitsko-dalmatinska/citys";
import { citys as citys_71_1052 } from "./1052_varazdinska/citys";
import { citys as citys_71_1053 } from "./1053_viroviticko-podravska/citys";
import { citys as citys_71_1054 } from "./1054_vukovarsko-srijemska/citys";
import { citys as citys_71_1055 } from "./1055_zadarska/citys";
import { citys as citys_71_1056 } from "./1056_zagrebacka/citys";
import { citys as citys_71_1057 } from "./1057_grad_zagreb/citys";
export const citys = [
    ...citys_71_1037,
    ...citys_71_1038,
    ...citys_71_1039,
    ...citys_71_1040,
    ...citys_71_1041,
    ...citys_71_1042,
    ...citys_71_1043,
    ...citys_71_1044,
    ...citys_71_1045,
    ...citys_71_1046,
    ...citys_71_1047,
    ...citys_71_1048,
    ...citys_71_1049,
    ...citys_71_1050,
    ...citys_71_1051,
    ...citys_71_1052,
    ...citys_71_1053,
    ...citys_71_1054,
    ...citys_71_1055,
    ...citys_71_1056,
    ...citys_71_1057,
];
