import { citys as citys_95_1843 } from "./1843_amazonas/citys";
import { citys as citys_95_1844 } from "./1844_anzoategui/citys";
import { citys as citys_95_1845 } from "./1845_apure/citys";
import { citys as citys_95_1846 } from "./1846_aragua/citys";
import { citys as citys_95_1847 } from "./1847_barinas/citys";
import { citys as citys_95_1848 } from "./1848_bolivar/citys";
import { citys as citys_95_1849 } from "./1849_carabobo/citys";
import { citys as citys_95_1850 } from "./1850_cojedes/citys";
import { citys as citys_95_1851 } from "./1851_delta_amacuro/citys";
import { citys as citys_95_1852 } from "./1852_falcon/citys";
import { citys as citys_95_1853 } from "./1853_guarico/citys";
import { citys as citys_95_1854 } from "./1854_lara/citys";
import { citys as citys_95_1855 } from "./1855_merida/citys";
import { citys as citys_95_1856 } from "./1856_miranda/citys";
import { citys as citys_95_1857 } from "./1857_monagas/citys";
import { citys as citys_95_1858 } from "./1858_nueva_esparta/citys";
import { citys as citys_95_1859 } from "./1859_portuguesa/citys";
import { citys as citys_95_1860 } from "./1860_sucre/citys";
import { citys as citys_95_1861 } from "./1861_tachira/citys";
import { citys as citys_95_1862 } from "./1862_trujillo/citys";
import { citys as citys_95_1863 } from "./1863_yaracuy/citys";
import { citys as citys_95_1864 } from "./1864_zulia/citys";
import { citys as citys_95_1865 } from "./1865_dependencias_federales/citys";
import { citys as citys_95_1866 } from "./1866_distrito_federal/citys";
import { citys as citys_95_1867 } from "./1867_vargas/citys";
export const citys = [
    ...citys_95_1843,
    ...citys_95_1844,
    ...citys_95_1845,
    ...citys_95_1846,
    ...citys_95_1847,
    ...citys_95_1848,
    ...citys_95_1849,
    ...citys_95_1850,
    ...citys_95_1851,
    ...citys_95_1852,
    ...citys_95_1853,
    ...citys_95_1854,
    ...citys_95_1855,
    ...citys_95_1856,
    ...citys_95_1857,
    ...citys_95_1858,
    ...citys_95_1859,
    ...citys_95_1860,
    ...citys_95_1861,
    ...citys_95_1862,
    ...citys_95_1863,
    ...citys_95_1864,
    ...citys_95_1865,
    ...citys_95_1866,
    ...citys_95_1867,
];
