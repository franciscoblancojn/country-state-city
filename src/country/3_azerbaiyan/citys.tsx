import { citys as citys_3_1 } from "./1_azerbaijan/citys";
import { citys as citys_3_2 } from "./2_nargorni_karabakh/citys";
import { citys as citys_3_3 } from "./3_nakhichevanskaya_region/citys";
export const citys = [...citys_3_1, ...citys_3_2, ...citys_3_3];
