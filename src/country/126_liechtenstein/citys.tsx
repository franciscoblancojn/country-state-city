import { citys as citys_126_1152 } from "./1152_balzers/citys";
import { citys as citys_126_1153 } from "./1153_eschen/citys";
import { citys as citys_126_1154 } from "./1154_gamprin/citys";
import { citys as citys_126_1155 } from "./1155_mauren/citys";
import { citys as citys_126_1156 } from "./1156_planken/citys";
import { citys as citys_126_1157 } from "./1157_ruggell/citys";
import { citys as citys_126_1158 } from "./1158_schaan/citys";
import { citys as citys_126_1159 } from "./1159_schellenberg/citys";
import { citys as citys_126_1160 } from "./1160_triesen/citys";
import { citys as citys_126_1161 } from "./1161_triesenberg/citys";
import { citys as citys_126_1162 } from "./1162_vaduz/citys";
export const citys = [
    ...citys_126_1152,
    ...citys_126_1153,
    ...citys_126_1154,
    ...citys_126_1155,
    ...citys_126_1156,
    ...citys_126_1157,
    ...citys_126_1158,
    ...citys_126_1159,
    ...citys_126_1160,
    ...citys_126_1161,
    ...citys_126_1162,
];
