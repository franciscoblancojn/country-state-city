import { citys as citys_65_956 } from "./956_hlavni_mesto_praha/citys";
import { citys as citys_65_957 } from "./957_jihomoravsky_kraj/citys";
import { citys as citys_65_958 } from "./958_jihocesky_kraj/citys";
import { citys as citys_65_959 } from "./959_vysocina/citys";
import { citys as citys_65_960 } from "./960_karlovarsky_kraj/citys";
import { citys as citys_65_961 } from "./961_kralovehradecky_kraj/citys";
import { citys as citys_65_962 } from "./962_liberecky_kraj/citys";
import { citys as citys_65_963 } from "./963_olomoucky_kraj/citys";
import { citys as citys_65_964 } from "./964_moravskoslezsky_kraj/citys";
import { citys as citys_65_965 } from "./965_pardubicky_kraj/citys";
import { citys as citys_65_966 } from "./966_plzensky_kraj/citys";
import { citys as citys_65_967 } from "./967_stredocesky_kraj/citys";
import { citys as citys_65_968 } from "./968_ustecky_kraj/citys";
import { citys as citys_65_969 } from "./969_zlinsky_kraj/citys";
export const citys = [
    ...citys_65_956,
    ...citys_65_957,
    ...citys_65_958,
    ...citys_65_959,
    ...citys_65_960,
    ...citys_65_961,
    ...citys_65_962,
    ...citys_65_963,
    ...citys_65_964,
    ...citys_65_965,
    ...citys_65_966,
    ...citys_65_967,
    ...citys_65_968,
    ...citys_65_969,
];
