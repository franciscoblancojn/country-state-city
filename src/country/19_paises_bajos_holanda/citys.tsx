import { citys as citys_19_2060 } from "./2060_drenthe/citys";
import { citys as citys_19_2061 } from "./2061_friesland/citys";
import { citys as citys_19_2062 } from "./2062_gelderland/citys";
import { citys as citys_19_2063 } from "./2063_groningen/citys";
import { citys as citys_19_2064 } from "./2064_limburg/citys";
import { citys as citys_19_2065 } from "./2065_noord-brabant/citys";
import { citys as citys_19_2066 } from "./2066_noord-holland/citys";
import { citys as citys_19_2067 } from "./2067_utrecht/citys";
import { citys as citys_19_2068 } from "./2068_zeeland/citys";
import { citys as citys_19_2069 } from "./2069_zuid-holland/citys";
import { citys as citys_19_2071 } from "./2071_overijssel/citys";
import { citys as citys_19_2072 } from "./2072_flevoland/citys";
export const citys = [
    ...citys_19_2060,
    ...citys_19_2061,
    ...citys_19_2062,
    ...citys_19_2063,
    ...citys_19_2064,
    ...citys_19_2065,
    ...citys_19_2066,
    ...citys_19_2067,
    ...citys_19_2068,
    ...citys_19_2069,
    ...citys_19_2071,
    ...citys_19_2072,
];
