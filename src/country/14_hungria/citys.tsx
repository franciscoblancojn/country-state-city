import { citys as citys_14_1112 } from "./1112_bacs-kiskun/citys";
import { citys as citys_14_1113 } from "./1113_baranya/citys";
import { citys as citys_14_1114 } from "./1114_bekes/citys";
import { citys as citys_14_1115 } from "./1115_borsod-abauj-zemplen/citys";
import { citys as citys_14_1116 } from "./1116_budapest/citys";
import { citys as citys_14_1117 } from "./1117_csongrad/citys";
import { citys as citys_14_1118 } from "./1118_debrecen/citys";
import { citys as citys_14_1119 } from "./1119_fejer/citys";
import { citys as citys_14_1120 } from "./1120_gyor-moson-sopron/citys";
import { citys as citys_14_1121 } from "./1121_hajdu-bihar/citys";
import { citys as citys_14_1122 } from "./1122_heves/citys";
import { citys as citys_14_1123 } from "./1123_komarom-esztergom/citys";
import { citys as citys_14_1124 } from "./1124_miskolc/citys";
import { citys as citys_14_1125 } from "./1125_nograd/citys";
import { citys as citys_14_1126 } from "./1126_pecs/citys";
import { citys as citys_14_1127 } from "./1127_pest/citys";
import { citys as citys_14_1128 } from "./1128_somogy/citys";
import { citys as citys_14_1129 } from "./1129_szabolcs-szatmar-bereg/citys";
import { citys as citys_14_1130 } from "./1130_szeged/citys";
import { citys as citys_14_1131 } from "./1131_jasz-nagykun-szolnok/citys";
import { citys as citys_14_1132 } from "./1132_tolna/citys";
import { citys as citys_14_1133 } from "./1133_vas/citys";
import { citys as citys_14_1134 } from "./1134_veszprem/citys";
import { citys as citys_14_1135 } from "./1135_zala/citys";
import { citys as citys_14_1136 } from "./1136_gyor/citys";
import { citys as citys_14_1150 } from "./1150_veszprem/citys";
export const citys = [
    ...citys_14_1112,
    ...citys_14_1113,
    ...citys_14_1114,
    ...citys_14_1115,
    ...citys_14_1116,
    ...citys_14_1117,
    ...citys_14_1118,
    ...citys_14_1119,
    ...citys_14_1120,
    ...citys_14_1121,
    ...citys_14_1122,
    ...citys_14_1123,
    ...citys_14_1124,
    ...citys_14_1125,
    ...citys_14_1126,
    ...citys_14_1127,
    ...citys_14_1128,
    ...citys_14_1129,
    ...citys_14_1130,
    ...citys_14_1131,
    ...citys_14_1132,
    ...citys_14_1133,
    ...citys_14_1134,
    ...citys_14_1135,
    ...citys_14_1136,
    ...citys_14_1150,
];
