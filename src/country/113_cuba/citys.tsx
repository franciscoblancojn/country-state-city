import { citys as citys_113_1886 } from "./1886_pinar_del_rio/citys";
import { citys as citys_113_1887 } from "./1887_ciudad_de_la_habana/citys";
import { citys as citys_113_1888 } from "./1888_matanzas/citys";
import { citys as citys_113_1889 } from "./1889_isla_de_la_juventud/citys";
import { citys as citys_113_1890 } from "./1890_camaguey/citys";
import { citys as citys_113_1891 } from "./1891_ciego_de_avila/citys";
import { citys as citys_113_1892 } from "./1892_cienfuegos/citys";
import { citys as citys_113_1893 } from "./1893_granma/citys";
import { citys as citys_113_1894 } from "./1894_guantanamo/citys";
import { citys as citys_113_1895 } from "./1895_la_habana/citys";
import { citys as citys_113_1896 } from "./1896_holguin/citys";
import { citys as citys_113_1897 } from "./1897_las_tunas/citys";
import { citys as citys_113_1898 } from "./1898_sancti_spiritus/citys";
import { citys as citys_113_1899 } from "./1899_santiago_de_cuba/citys";
import { citys as citys_113_1900 } from "./1900_villa_clara/citys";
export const citys = [
    ...citys_113_1886,
    ...citys_113_1887,
    ...citys_113_1888,
    ...citys_113_1889,
    ...citys_113_1890,
    ...citys_113_1891,
    ...citys_113_1892,
    ...citys_113_1893,
    ...citys_113_1894,
    ...citys_113_1895,
    ...citys_113_1896,
    ...citys_113_1897,
    ...citys_113_1898,
    ...citys_113_1899,
    ...citys_113_1900,
];
