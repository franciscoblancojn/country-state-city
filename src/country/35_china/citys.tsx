import { citys as citys_35_1966 } from "./1966_anhui/citys";
import { citys as citys_35_1967 } from "./1967_zhejiang/citys";
import { citys as citys_35_1968 } from "./1968_jiangxi/citys";
import { citys as citys_35_1969 } from "./1969_jiangsu/citys";
import { citys as citys_35_1970 } from "./1970_jilin/citys";
import { citys as citys_35_1971 } from "./1971_qinghai/citys";
import { citys as citys_35_1972 } from "./1972_fujian/citys";
import { citys as citys_35_1973 } from "./1973_heilongjiang/citys";
import { citys as citys_35_1974 } from "./1974_henan/citys";
import { citys as citys_35_1975 } from "./1975_hebei/citys";
import { citys as citys_35_1976 } from "./1976_hunan/citys";
import { citys as citys_35_1977 } from "./1977_hubei/citys";
import { citys as citys_35_1978 } from "./1978_xinjiang/citys";
import { citys as citys_35_1979 } from "./1979_xizang/citys";
import { citys as citys_35_1980 } from "./1980_gansu/citys";
import { citys as citys_35_1981 } from "./1981_guangxi/citys";
import { citys as citys_35_1982 } from "./1982_guizhou/citys";
import { citys as citys_35_1983 } from "./1983_liaoning/citys";
import { citys as citys_35_1984 } from "./1984_nei_mongol/citys";
import { citys as citys_35_1985 } from "./1985_ningxia/citys";
import { citys as citys_35_1986 } from "./1986_beijing/citys";
import { citys as citys_35_1987 } from "./1987_shanghai/citys";
import { citys as citys_35_1988 } from "./1988_shanxi/citys";
import { citys as citys_35_1989 } from "./1989_shandong/citys";
import { citys as citys_35_1990 } from "./1990_shaanxi/citys";
import { citys as citys_35_1991 } from "./1991_sichuan/citys";
import { citys as citys_35_1992 } from "./1992_tianjin/citys";
import { citys as citys_35_1993 } from "./1993_yunnan/citys";
import { citys as citys_35_1994 } from "./1994_guangdong/citys";
import { citys as citys_35_1995 } from "./1995_hainan/citys";
import { citys as citys_35_1996 } from "./1996_chongqing/citys";
import { citys as citys_35_2108 } from "./2108_schanghai/citys";
import { citys as citys_35_2109 } from "./2109_hongkong/citys";
import { citys as citys_35_2110 } from "./2110_neimenggu/citys";
import { citys as citys_35_2111 } from "./2111_aomen/citys";
export const citys = [
    ...citys_35_1966,
    ...citys_35_1967,
    ...citys_35_1968,
    ...citys_35_1969,
    ...citys_35_1970,
    ...citys_35_1971,
    ...citys_35_1972,
    ...citys_35_1973,
    ...citys_35_1974,
    ...citys_35_1975,
    ...citys_35_1976,
    ...citys_35_1977,
    ...citys_35_1978,
    ...citys_35_1979,
    ...citys_35_1980,
    ...citys_35_1981,
    ...citys_35_1982,
    ...citys_35_1983,
    ...citys_35_1984,
    ...citys_35_1985,
    ...citys_35_1986,
    ...citys_35_1987,
    ...citys_35_1988,
    ...citys_35_1989,
    ...citys_35_1990,
    ...citys_35_1991,
    ...citys_35_1992,
    ...citys_35_1993,
    ...citys_35_1994,
    ...citys_35_1995,
    ...citys_35_1996,
    ...citys_35_2108,
    ...citys_35_2109,
    ...citys_35_2110,
    ...citys_35_2111,
];
