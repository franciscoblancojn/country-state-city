import { citys as citys_81_1662 } from "./1662_valparaiso/citys";
import { citys as citys_81_1663 } from "./1663_aisen_del_general_carlos_ibanez_del_campo/citys";
import { citys as citys_81_1664 } from "./1664_antofagasta/citys";
import { citys as citys_81_1665 } from "./1665_araucania/citys";
import { citys as citys_81_1666 } from "./1666_atacama/citys";
import { citys as citys_81_1667 } from "./1667_bio-bio/citys";
import { citys as citys_81_1668 } from "./1668_coquimbo/citys";
import { citys as citys_81_1669 } from "./1669_libertador_general_bernardo_ohiggins/citys";
import { citys as citys_81_1670 } from "./1670_los_lagos/citys";
import { citys as citys_81_1671 } from "./1671_magallanes_y_de_la_antartica_chilena/citys";
import { citys as citys_81_1672 } from "./1672_maule/citys";
import { citys as citys_81_1673 } from "./1673_region_metropolitana/citys";
import { citys as citys_81_1674 } from "./1674_tarapaca/citys";
export const citys = [
    ...citys_81_1662,
    ...citys_81_1663,
    ...citys_81_1664,
    ...citys_81_1665,
    ...citys_81_1666,
    ...citys_81_1667,
    ...citys_81_1668,
    ...citys_81_1669,
    ...citys_81_1670,
    ...citys_81_1671,
    ...citys_81_1672,
    ...citys_81_1673,
    ...citys_81_1674,
];
