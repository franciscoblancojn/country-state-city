import { citys as citys_11_1007 } from "./1007_mikhaylovgrad/citys";
import { citys as citys_11_1008 } from "./1008_blagoevgrad/citys";
import { citys as citys_11_1009 } from "./1009_burgas/citys";
import { citys as citys_11_1010 } from "./1010_dobrich/citys";
import { citys as citys_11_1011 } from "./1011_gabrovo/citys";
import { citys as citys_11_1012 } from "./1012_grad_sofiya/citys";
import { citys as citys_11_1013 } from "./1013_khaskovo/citys";
import { citys as citys_11_1014 } from "./1014_kurdzhali/citys";
import { citys as citys_11_1015 } from "./1015_kyustendil/citys";
import { citys as citys_11_1016 } from "./1016_lovech/citys";
import { citys as citys_11_1017 } from "./1017_montana/citys";
import { citys as citys_11_1018 } from "./1018_pazardzhik/citys";
import { citys as citys_11_1019 } from "./1019_pernik/citys";
import { citys as citys_11_1020 } from "./1020_pleven/citys";
import { citys as citys_11_1021 } from "./1021_plovdiv/citys";
import { citys as citys_11_1022 } from "./1022_razgrad/citys";
import { citys as citys_11_1023 } from "./1023_ruse/citys";
import { citys as citys_11_1024 } from "./1024_shumen/citys";
import { citys as citys_11_1025 } from "./1025_silistra/citys";
import { citys as citys_11_1026 } from "./1026_sliven/citys";
import { citys as citys_11_1027 } from "./1027_smolyan/citys";
import { citys as citys_11_1028 } from "./1028_sofiya/citys";
import { citys as citys_11_1029 } from "./1029_stara_zagora/citys";
import { citys as citys_11_1030 } from "./1030_turgovishte/citys";
import { citys as citys_11_1031 } from "./1031_varna/citys";
import { citys as citys_11_1032 } from "./1032_veliko_turnovo/citys";
import { citys as citys_11_1033 } from "./1033_vidin/citys";
import { citys as citys_11_1034 } from "./1034_vratsa/citys";
import { citys as citys_11_1035 } from "./1035_yambol/citys";
export const citys = [
    ...citys_11_1007,
    ...citys_11_1008,
    ...citys_11_1009,
    ...citys_11_1010,
    ...citys_11_1011,
    ...citys_11_1012,
    ...citys_11_1013,
    ...citys_11_1014,
    ...citys_11_1015,
    ...citys_11_1016,
    ...citys_11_1017,
    ...citys_11_1018,
    ...citys_11_1019,
    ...citys_11_1020,
    ...citys_11_1021,
    ...citys_11_1022,
    ...citys_11_1023,
    ...citys_11_1024,
    ...citys_11_1025,
    ...citys_11_1026,
    ...citys_11_1027,
    ...citys_11_1028,
    ...citys_11_1029,
    ...citys_11_1030,
    ...citys_11_1031,
    ...citys_11_1032,
    ...citys_11_1033,
    ...citys_11_1034,
    ...citys_11_1035,
];
