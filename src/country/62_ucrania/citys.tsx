import { citys as citys_62_279 } from "./279_vinnitskaya_obl./citys";
import { citys as citys_62_280 } from "./280_volynskaya_obl./citys";
import { citys as citys_62_281 } from "./281_dnepropetrovskaya_obl./citys";
import { citys as citys_62_282 } from "./282_donetskaya_obl./citys";
import { citys as citys_62_283 } from "./283_zhitomirskaya_obl./citys";
import { citys as citys_62_284 } from "./284_zakarpatskaya_obl./citys";
import { citys as citys_62_285 } from "./285_zaporozhskaya_obl./citys";
import { citys as citys_62_286 } from "./286_ivano-frankovskaya_obl./citys";
import { citys as citys_62_287 } from "./287_kievskaya_obl./citys";
import { citys as citys_62_288 } from "./288_kirovogradskaya_obl./citys";
import { citys as citys_62_289 } from "./289_krymskaya_obl./citys";
import { citys as citys_62_290 } from "./290_luganskaya_obl./citys";
import { citys as citys_62_291 } from "./291_lvovskaya_obl./citys";
import { citys as citys_62_292 } from "./292_nikolaevskaya_obl./citys";
import { citys as citys_62_293 } from "./293_odesskaya_obl./citys";
import { citys as citys_62_294 } from "./294_poltavskaya_obl./citys";
import { citys as citys_62_295 } from "./295_rovenskaya_obl./citys";
import { citys as citys_62_296 } from "./296_sumskaya_obl./citys";
import { citys as citys_62_297 } from "./297_ternopolskaya_obl./citys";
import { citys as citys_62_298 } from "./298_harkovskaya_obl./citys";
import { citys as citys_62_299 } from "./299_hersonskaya_obl./citys";
import { citys as citys_62_300 } from "./300_hmelnitskaya_obl./citys";
import { citys as citys_62_301 } from "./301_cherkasskaya_obl./citys";
import { citys as citys_62_302 } from "./302_chernigovskaya_obl./citys";
import { citys as citys_62_303 } from "./303_chernovitskaya_obl./citys";
import { citys as citys_62_375 } from "./375_ukraina/citys";
export const citys = [
    ...citys_62_279,
    ...citys_62_280,
    ...citys_62_281,
    ...citys_62_282,
    ...citys_62_283,
    ...citys_62_284,
    ...citys_62_285,
    ...citys_62_286,
    ...citys_62_287,
    ...citys_62_288,
    ...citys_62_289,
    ...citys_62_290,
    ...citys_62_291,
    ...citys_62_292,
    ...citys_62_293,
    ...citys_62_294,
    ...citys_62_295,
    ...citys_62_296,
    ...citys_62_297,
    ...citys_62_298,
    ...citys_62_299,
    ...citys_62_300,
    ...citys_62_301,
    ...citys_62_302,
    ...citys_62_303,
    ...citys_62_375,
];
