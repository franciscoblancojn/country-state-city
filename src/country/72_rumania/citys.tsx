import { citys as citys_72_1361 } from "./1361_alba/citys";
import { citys as citys_72_1362 } from "./1362_arad/citys";
import { citys as citys_72_1363 } from "./1363_arges/citys";
import { citys as citys_72_1364 } from "./1364_bacau/citys";
import { citys as citys_72_1365 } from "./1365_bihor/citys";
import { citys as citys_72_1366 } from "./1366_bistrita-nasaud/citys";
import { citys as citys_72_1367 } from "./1367_botosani/citys";
import { citys as citys_72_1368 } from "./1368_braila/citys";
import { citys as citys_72_1369 } from "./1369_brasov/citys";
import { citys as citys_72_1370 } from "./1370_bucuresti/citys";
import { citys as citys_72_1371 } from "./1371_buzau/citys";
import { citys as citys_72_1372 } from "./1372_caras-severin/citys";
import { citys as citys_72_1373 } from "./1373_cluj/citys";
import { citys as citys_72_1374 } from "./1374_constanta/citys";
import { citys as citys_72_1375 } from "./1375_covasna/citys";
import { citys as citys_72_1376 } from "./1376_dambovita/citys";
import { citys as citys_72_1377 } from "./1377_dolj/citys";
import { citys as citys_72_1378 } from "./1378_galati/citys";
import { citys as citys_72_1379 } from "./1379_gorj/citys";
import { citys as citys_72_1380 } from "./1380_harghita/citys";
import { citys as citys_72_1381 } from "./1381_hunedoara/citys";
import { citys as citys_72_1382 } from "./1382_ialomita/citys";
import { citys as citys_72_1383 } from "./1383_iasi/citys";
import { citys as citys_72_1384 } from "./1384_maramures/citys";
import { citys as citys_72_1385 } from "./1385_mehedinti/citys";
import { citys as citys_72_1386 } from "./1386_mures/citys";
import { citys as citys_72_1387 } from "./1387_neamt/citys";
import { citys as citys_72_1388 } from "./1388_olt/citys";
import { citys as citys_72_1389 } from "./1389_prahova/citys";
import { citys as citys_72_1390 } from "./1390_salaj/citys";
import { citys as citys_72_1391 } from "./1391_satu_mare/citys";
import { citys as citys_72_1392 } from "./1392_sibiu/citys";
import { citys as citys_72_1393 } from "./1393_suceava/citys";
import { citys as citys_72_1394 } from "./1394_teleorman/citys";
import { citys as citys_72_1395 } from "./1395_timis/citys";
import { citys as citys_72_1396 } from "./1396_tulcea/citys";
import { citys as citys_72_1397 } from "./1397_vaslui/citys";
import { citys as citys_72_1398 } from "./1398_valcea/citys";
import { citys as citys_72_1399 } from "./1399_vrancea/citys";
import { citys as citys_72_1400 } from "./1400_calarasi/citys";
import { citys as citys_72_1401 } from "./1401_giurgiu/citys";
export const citys = [
    ...citys_72_1361,
    ...citys_72_1362,
    ...citys_72_1363,
    ...citys_72_1364,
    ...citys_72_1365,
    ...citys_72_1366,
    ...citys_72_1367,
    ...citys_72_1368,
    ...citys_72_1369,
    ...citys_72_1370,
    ...citys_72_1371,
    ...citys_72_1372,
    ...citys_72_1373,
    ...citys_72_1374,
    ...citys_72_1375,
    ...citys_72_1376,
    ...citys_72_1377,
    ...citys_72_1378,
    ...citys_72_1379,
    ...citys_72_1380,
    ...citys_72_1381,
    ...citys_72_1382,
    ...citys_72_1383,
    ...citys_72_1384,
    ...citys_72_1385,
    ...citys_72_1386,
    ...citys_72_1387,
    ...citys_72_1388,
    ...citys_72_1389,
    ...citys_72_1390,
    ...citys_72_1391,
    ...citys_72_1392,
    ...citys_72_1393,
    ...citys_72_1394,
    ...citys_72_1395,
    ...citys_72_1396,
    ...citys_72_1397,
    ...citys_72_1398,
    ...citys_72_1399,
    ...citys_72_1400,
    ...citys_72_1401,
];
