import { citys as citys_53_1423 } from "./1423_beltinci/citys";
import { citys as citys_53_1425 } from "./1425_bohinj/citys";
import { citys as citys_53_1426 } from "./1426_borovnica/citys";
import { citys as citys_53_1427 } from "./1427_bovec/citys";
import { citys as citys_53_1428 } from "./1428_brda/citys";
import { citys as citys_53_1429 } from "./1429_brezice/citys";
import { citys as citys_53_1430 } from "./1430_brezovica/citys";
import { citys as citys_53_1432 } from "./1432_cerklje_na_gorenjskem/citys";
import { citys as citys_53_1434 } from "./1434_cerkno/citys";
import { citys as citys_53_1436 } from "./1436_crna_na_koroskem/citys";
import { citys as citys_53_1437 } from "./1437_crnomelj/citys";
import { citys as citys_53_1438 } from "./1438_divaca/citys";
import { citys as citys_53_1439 } from "./1439_dobrepolje/citys";
import { citys as citys_53_1440 } from "./1440_dol_pri_ljubljani/citys";
import { citys as citys_53_1443 } from "./1443_duplek/citys";
import { citys as citys_53_1447 } from "./1447_gornji_grad/citys";
import { citys as citys_53_1450 } from "./1450_hrastnik/citys";
import { citys as citys_53_1451 } from "./1451_hrpelje-kozina/citys";
import { citys as citys_53_1452 } from "./1452_idrija/citys";
import { citys as citys_53_1453 } from "./1453_ig/citys";
import { citys as citys_53_1454 } from "./1454_ilirska_bistrica/citys";
import { citys as citys_53_1455 } from "./1455_ivancna_gorica/citys";
import { citys as citys_53_1462 } from "./1462_komen/citys";
import { citys as citys_53_1463 } from "./1463_koper-capodistria/citys";
import { citys as citys_53_1464 } from "./1464_kozje/citys";
import { citys as citys_53_1465 } from "./1465_kranj/citys";
import { citys as citys_53_1466 } from "./1466_kranjska_gora/citys";
import { citys as citys_53_1467 } from "./1467_krsko/citys";
import { citys as citys_53_1469 } from "./1469_lasko/citys";
import { citys as citys_53_1470 } from "./1470_ljubljana/citys";
import { citys as citys_53_1471 } from "./1471_ljubno/citys";
import { citys as citys_53_1472 } from "./1472_logatec/citys";
import { citys as citys_53_1475 } from "./1475_medvode/citys";
import { citys as citys_53_1476 } from "./1476_menges/citys";
import { citys as citys_53_1478 } from "./1478_mezica/citys";
import { citys as citys_53_1480 } from "./1480_moravce/citys";
import { citys as citys_53_1482 } from "./1482_mozirje/citys";
import { citys as citys_53_1483 } from "./1483_murska_sobota/citys";
import { citys as citys_53_1487 } from "./1487_nova_gorica/citys";
import { citys as citys_53_1489 } from "./1489_ormoz/citys";
import { citys as citys_53_1491 } from "./1491_pesnica/citys";
import { citys as citys_53_1494 } from "./1494_postojna/citys";
import { citys as citys_53_1497 } from "./1497_radece/citys";
import { citys as citys_53_1498 } from "./1498_radenci/citys";
import { citys as citys_53_1500 } from "./1500_radovljica/citys";
import { citys as citys_53_1502 } from "./1502_rogaska_slatina/citys";
import { citys as citys_53_1505 } from "./1505_sencur/citys";
import { citys as citys_53_1506 } from "./1506_sentilj/citys";
import { citys as citys_53_1508 } from "./1508_sevnica/citys";
import { citys as citys_53_1509 } from "./1509_sezana/citys";
import { citys as citys_53_1511 } from "./1511_skofja_loka/citys";
import { citys as citys_53_1513 } from "./1513_slovenj_gradec/citys";
import { citys as citys_53_1514 } from "./1514_slovenske_konjice/citys";
import { citys as citys_53_1515 } from "./1515_smarje_pri_jelsah/citys";
import { citys as citys_53_1521 } from "./1521_tolmin/citys";
import { citys as citys_53_1522 } from "./1522_trbovlje/citys";
import { citys as citys_53_1524 } from "./1524_trzic/citys";
import { citys as citys_53_1526 } from "./1526_velenje/citys";
import { citys as citys_53_1528 } from "./1528_vipava/citys";
import { citys as citys_53_1531 } from "./1531_vrhnika/citys";
import { citys as citys_53_1532 } from "./1532_vuzenica/citys";
import { citys as citys_53_1533 } from "./1533_zagorje_ob_savi/citys";
import { citys as citys_53_1535 } from "./1535_zelezniki/citys";
import { citys as citys_53_1536 } from "./1536_ziri/citys";
import { citys as citys_53_1537 } from "./1537_zrece/citys";
import { citys as citys_53_1539 } from "./1539_domzale/citys";
import { citys as citys_53_1540 } from "./1540_jesenice/citys";
import { citys as citys_53_1541 } from "./1541_kamnik/citys";
import { citys as citys_53_1542 } from "./1542_kocevje/citys";
import { citys as citys_53_1544 } from "./1544_lenart/citys";
import { citys as citys_53_1545 } from "./1545_litija/citys";
import { citys as citys_53_1546 } from "./1546_ljutomer/citys";
import { citys as citys_53_1550 } from "./1550_maribor/citys";
import { citys as citys_53_1552 } from "./1552_novo_mesto/citys";
import { citys as citys_53_1553 } from "./1553_piran/citys";
import { citys as citys_53_1554 } from "./1554_preddvor/citys";
import { citys as citys_53_1555 } from "./1555_ptuj/citys";
import { citys as citys_53_1556 } from "./1556_ribnica/citys";
import { citys as citys_53_1558 } from "./1558_sentjur_pri_celju/citys";
import { citys as citys_53_1559 } from "./1559_slovenska_bistrica/citys";
import { citys as citys_53_1560 } from "./1560_videm/citys";
import { citys as citys_53_1562 } from "./1562_zalec/citys";
export const citys = [
    ...citys_53_1423,
    ...citys_53_1425,
    ...citys_53_1426,
    ...citys_53_1427,
    ...citys_53_1428,
    ...citys_53_1429,
    ...citys_53_1430,
    ...citys_53_1432,
    ...citys_53_1434,
    ...citys_53_1436,
    ...citys_53_1437,
    ...citys_53_1438,
    ...citys_53_1439,
    ...citys_53_1440,
    ...citys_53_1443,
    ...citys_53_1447,
    ...citys_53_1450,
    ...citys_53_1451,
    ...citys_53_1452,
    ...citys_53_1453,
    ...citys_53_1454,
    ...citys_53_1455,
    ...citys_53_1462,
    ...citys_53_1463,
    ...citys_53_1464,
    ...citys_53_1465,
    ...citys_53_1466,
    ...citys_53_1467,
    ...citys_53_1469,
    ...citys_53_1470,
    ...citys_53_1471,
    ...citys_53_1472,
    ...citys_53_1475,
    ...citys_53_1476,
    ...citys_53_1478,
    ...citys_53_1480,
    ...citys_53_1482,
    ...citys_53_1483,
    ...citys_53_1487,
    ...citys_53_1489,
    ...citys_53_1491,
    ...citys_53_1494,
    ...citys_53_1497,
    ...citys_53_1498,
    ...citys_53_1500,
    ...citys_53_1502,
    ...citys_53_1505,
    ...citys_53_1506,
    ...citys_53_1508,
    ...citys_53_1509,
    ...citys_53_1511,
    ...citys_53_1513,
    ...citys_53_1514,
    ...citys_53_1515,
    ...citys_53_1521,
    ...citys_53_1522,
    ...citys_53_1524,
    ...citys_53_1526,
    ...citys_53_1528,
    ...citys_53_1531,
    ...citys_53_1532,
    ...citys_53_1533,
    ...citys_53_1535,
    ...citys_53_1536,
    ...citys_53_1537,
    ...citys_53_1539,
    ...citys_53_1540,
    ...citys_53_1541,
    ...citys_53_1542,
    ...citys_53_1544,
    ...citys_53_1545,
    ...citys_53_1546,
    ...citys_53_1550,
    ...citys_53_1552,
    ...citys_53_1553,
    ...citys_53_1554,
    ...citys_53_1555,
    ...citys_53_1556,
    ...citys_53_1558,
    ...citys_53_1559,
    ...citys_53_1560,
    ...citys_53_1562,
];
