import { citys as citys_66_722 } from "./722_aargau/citys";
import { citys as citys_66_723 } from "./723_appenzell_innerrhoden/citys";
import { citys as citys_66_724 } from "./724_appenzell_ausserrhoden/citys";
import { citys as citys_66_725 } from "./725_bern/citys";
import { citys as citys_66_726 } from "./726_basel-landschaft/citys";
import { citys as citys_66_727 } from "./727_basel-stadt/citys";
import { citys as citys_66_728 } from "./728_fribourg/citys";
import { citys as citys_66_729 } from "./729_geneve/citys";
import { citys as citys_66_730 } from "./730_glarus/citys";
import { citys as citys_66_731 } from "./731_graubunden/citys";
import { citys as citys_66_732 } from "./732_jura/citys";
import { citys as citys_66_733 } from "./733_luzern/citys";
import { citys as citys_66_734 } from "./734_neuchatel/citys";
import { citys as citys_66_735 } from "./735_nidwalden/citys";
import { citys as citys_66_736 } from "./736_obwalden/citys";
import { citys as citys_66_737 } from "./737_sankt_gallen/citys";
import { citys as citys_66_738 } from "./738_schaffhausen/citys";
import { citys as citys_66_739 } from "./739_solothurn/citys";
import { citys as citys_66_740 } from "./740_schwyz/citys";
import { citys as citys_66_741 } from "./741_thurgau/citys";
import { citys as citys_66_742 } from "./742_ticino/citys";
import { citys as citys_66_743 } from "./743_uri/citys";
import { citys as citys_66_744 } from "./744_vaud/citys";
import { citys as citys_66_745 } from "./745_valais/citys";
import { citys as citys_66_746 } from "./746_zug/citys";
import { citys as citys_66_747 } from "./747_zurich/citys";
export const citys = [
    ...citys_66_722,
    ...citys_66_723,
    ...citys_66_724,
    ...citys_66_725,
    ...citys_66_726,
    ...citys_66_727,
    ...citys_66_728,
    ...citys_66_729,
    ...citys_66_730,
    ...citys_66_731,
    ...citys_66_732,
    ...citys_66_733,
    ...citys_66_734,
    ...citys_66_735,
    ...citys_66_736,
    ...citys_66_737,
    ...citys_66_738,
    ...citys_66_739,
    ...citys_66_740,
    ...citys_66_741,
    ...citys_66_742,
    ...citys_66_743,
    ...citys_66_744,
    ...citys_66_745,
    ...citys_66_746,
    ...citys_66_747,
];
