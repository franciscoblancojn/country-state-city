import { citys as citys_89_1596 } from "./1596_amazonas/citys";
import { citys as citys_89_1597 } from "./1597_ancash/citys";
import { citys as citys_89_1598 } from "./1598_apurimac/citys";
import { citys as citys_89_1599 } from "./1599_arequipa/citys";
import { citys as citys_89_1600 } from "./1600_ayacucho/citys";
import { citys as citys_89_1601 } from "./1601_cajamarca/citys";
import { citys as citys_89_1602 } from "./1602_callao/citys";
import { citys as citys_89_1603 } from "./1603_cusco/citys";
import { citys as citys_89_1604 } from "./1604_huancavelica/citys";
import { citys as citys_89_1605 } from "./1605_huanuco/citys";
import { citys as citys_89_1606 } from "./1606_ica/citys";
import { citys as citys_89_1607 } from "./1607_junin/citys";
import { citys as citys_89_1608 } from "./1608_la_libertad/citys";
import { citys as citys_89_1609 } from "./1609_lambayeque/citys";
import { citys as citys_89_1610 } from "./1610_lima/citys";
import { citys as citys_89_1611 } from "./1611_loreto/citys";
import { citys as citys_89_1612 } from "./1612_madre_de_dios/citys";
import { citys as citys_89_1613 } from "./1613_moquegua/citys";
import { citys as citys_89_1614 } from "./1614_pasco/citys";
import { citys as citys_89_1615 } from "./1615_piura/citys";
import { citys as citys_89_1616 } from "./1616_puno/citys";
import { citys as citys_89_1617 } from "./1617_san_martin/citys";
import { citys as citys_89_1618 } from "./1618_tacna/citys";
import { citys as citys_89_1619 } from "./1619_tumbes/citys";
import { citys as citys_89_1620 } from "./1620_ucayali/citys";
export const citys = [
    ...citys_89_1596,
    ...citys_89_1597,
    ...citys_89_1598,
    ...citys_89_1599,
    ...citys_89_1600,
    ...citys_89_1601,
    ...citys_89_1602,
    ...citys_89_1603,
    ...citys_89_1604,
    ...citys_89_1605,
    ...citys_89_1606,
    ...citys_89_1607,
    ...citys_89_1608,
    ...citys_89_1609,
    ...citys_89_1610,
    ...citys_89_1611,
    ...citys_89_1612,
    ...citys_89_1613,
    ...citys_89_1614,
    ...citys_89_1615,
    ...citys_89_1616,
    ...citys_89_1617,
    ...citys_89_1618,
    ...citys_89_1619,
    ...citys_89_1620,
];
