import { citys as citys_28_609 } from "./609_las_palmas/citys";
import { citys as citys_28_610 } from "./610_soria/citys";
import { citys as citys_28_611 } from "./611_palencia/citys";
import { citys as citys_28_612 } from "./612_zamora/citys";
import { citys as citys_28_613 } from "./613_cadiz/citys";
import { citys as citys_28_614 } from "./614_navarra/citys";
import { citys as citys_28_615 } from "./615_ourense/citys";
import { citys as citys_28_616 } from "./616_segovia/citys";
import { citys as citys_28_617 } from "./617_guipuzcoa/citys";
import { citys as citys_28_618 } from "./618_ciudad_real/citys";
import { citys as citys_28_619 } from "./619_vizcaya/citys";
import { citys as citys_28_620 } from "./620_alava/citys";
import { citys as citys_28_621 } from "./621_a_coruna/citys";
import { citys as citys_28_622 } from "./622_cantabria/citys";
import { citys as citys_28_623 } from "./623_almeria/citys";
import { citys as citys_28_624 } from "./624_zaragoza/citys";
import { citys as citys_28_625 } from "./625_santa_cruz_de_tenerife/citys";
import { citys as citys_28_626 } from "./626_caceres/citys";
import { citys as citys_28_627 } from "./627_guadalajara/citys";
import { citys as citys_28_628 } from "./628_avila/citys";
import { citys as citys_28_629 } from "./629_toledo/citys";
import { citys as citys_28_630 } from "./630_castellon/citys";
import { citys as citys_28_631 } from "./631_tarragona/citys";
import { citys as citys_28_632 } from "./632_lugo/citys";
import { citys as citys_28_633 } from "./633_la_rioja/citys";
import { citys as citys_28_634 } from "./634_ceuta/citys";
import { citys as citys_28_635 } from "./635_murcia/citys";
import { citys as citys_28_636 } from "./636_salamanca/citys";
import { citys as citys_28_637 } from "./637_valladolid/citys";
import { citys as citys_28_638 } from "./638_jaen/citys";
import { citys as citys_28_639 } from "./639_girona/citys";
import { citys as citys_28_640 } from "./640_granada/citys";
import { citys as citys_28_641 } from "./641_alacant/citys";
import { citys as citys_28_642 } from "./642_cordoba/citys";
import { citys as citys_28_643 } from "./643_albacete/citys";
import { citys as citys_28_644 } from "./644_cuenca/citys";
import { citys as citys_28_645 } from "./645_pontevedra/citys";
import { citys as citys_28_646 } from "./646_teruel/citys";
import { citys as citys_28_647 } from "./647_melilla/citys";
import { citys as citys_28_648 } from "./648_barcelona/citys";
import { citys as citys_28_649 } from "./649_badajoz/citys";
import { citys as citys_28_650 } from "./650_madrid/citys";
import { citys as citys_28_651 } from "./651_sevilla/citys";
import { citys as citys_28_652 } from "./652_valencia/citys";
import { citys as citys_28_653 } from "./653_huelva/citys";
import { citys as citys_28_654 } from "./654_lleida/citys";
import { citys as citys_28_655 } from "./655_leon/citys";
import { citys as citys_28_656 } from "./656_illes_balears/citys";
import { citys as citys_28_657 } from "./657_burgos/citys";
import { citys as citys_28_658 } from "./658_huesca/citys";
import { citys as citys_28_659 } from "./659_asturias/citys";
import { citys as citys_28_660 } from "./660_malaga/citys";
export const citys = [
    ...citys_28_609,
    ...citys_28_610,
    ...citys_28_611,
    ...citys_28_612,
    ...citys_28_613,
    ...citys_28_614,
    ...citys_28_615,
    ...citys_28_616,
    ...citys_28_617,
    ...citys_28_618,
    ...citys_28_619,
    ...citys_28_620,
    ...citys_28_621,
    ...citys_28_622,
    ...citys_28_623,
    ...citys_28_624,
    ...citys_28_625,
    ...citys_28_626,
    ...citys_28_627,
    ...citys_28_628,
    ...citys_28_629,
    ...citys_28_630,
    ...citys_28_631,
    ...citys_28_632,
    ...citys_28_633,
    ...citys_28_634,
    ...citys_28_635,
    ...citys_28_636,
    ...citys_28_637,
    ...citys_28_638,
    ...citys_28_639,
    ...citys_28_640,
    ...citys_28_641,
    ...citys_28_642,
    ...citys_28_643,
    ...citys_28_644,
    ...citys_28_645,
    ...citys_28_646,
    ...citys_28_647,
    ...citys_28_648,
    ...citys_28_649,
    ...citys_28_650,
    ...citys_28_651,
    ...citys_28_652,
    ...citys_28_653,
    ...citys_28_654,
    ...citys_28_655,
    ...citys_28_656,
    ...citys_28_657,
    ...citys_28_658,
    ...citys_28_659,
    ...citys_28_660,
];
