import { citys as citys_24_38 } from "./38_southern_district/citys";
import { citys as citys_24_39 } from "./39_central_district/citys";
import { citys as citys_24_40 } from "./40_northern_district/citys";
import { citys as citys_24_41 } from "./41_haifa/citys";
import { citys as citys_24_42 } from "./42_tel_aviv/citys";
import { citys as citys_24_43 } from "./43_jerusalem/citys";
import { citys as citys_24_409 } from "./409_ramat_hagolan/citys";
export const citys = [
    ...citys_24_38,
    ...citys_24_39,
    ...citys_24_40,
    ...citys_24_41,
    ...citys_24_42,
    ...citys_24_43,
    ...citys_24_409,
];
