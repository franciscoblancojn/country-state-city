import { citys as citys_52_1413 } from "./1413_banska_bystrica/citys";
import { citys as citys_52_1414 } from "./1414_bratislava/citys";
import { citys as citys_52_1415 } from "./1415_kosice/citys";
import { citys as citys_52_1416 } from "./1416_nitra/citys";
import { citys as citys_52_1417 } from "./1417_presov/citys";
import { citys as citys_52_1418 } from "./1418_trencin/citys";
import { citys as citys_52_1419 } from "./1419_trnava/citys";
import { citys as citys_52_1420 } from "./1420_zilina/citys";
export const citys = [
    ...citys_52_1413,
    ...citys_52_1414,
    ...citys_52_1415,
    ...citys_52_1416,
    ...citys_52_1417,
    ...citys_52_1418,
    ...citys_52_1419,
    ...citys_52_1420,
];
