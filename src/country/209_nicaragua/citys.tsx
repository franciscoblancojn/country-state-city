import { citys as citys_209_1869 } from "./1869_boaco/citys";
import { citys as citys_209_1870 } from "./1870_carazo/citys";
import { citys as citys_209_1871 } from "./1871_chinandega/citys";
import { citys as citys_209_1872 } from "./1872_chontales/citys";
import { citys as citys_209_1873 } from "./1873_esteli/citys";
import { citys as citys_209_1874 } from "./1874_granada/citys";
import { citys as citys_209_1875 } from "./1875_jinotega/citys";
import { citys as citys_209_1876 } from "./1876_leon/citys";
import { citys as citys_209_1877 } from "./1877_madriz/citys";
import { citys as citys_209_1878 } from "./1878_managua/citys";
import { citys as citys_209_1879 } from "./1879_masaya/citys";
import { citys as citys_209_1880 } from "./1880_matagalpa/citys";
import { citys as citys_209_1881 } from "./1881_nueva_segovia/citys";
import { citys as citys_209_1882 } from "./1882_rio_san_juan/citys";
import { citys as citys_209_1883 } from "./1883_rivas/citys";
import { citys as citys_209_1884 } from "./1884_zelaya/citys";
export const citys = [
    ...citys_209_1869,
    ...citys_209_1870,
    ...citys_209_1871,
    ...citys_209_1872,
    ...citys_209_1873,
    ...citys_209_1874,
    ...citys_209_1875,
    ...citys_209_1876,
    ...citys_209_1877,
    ...citys_209_1878,
    ...citys_209_1879,
    ...citys_209_1880,
    ...citys_209_1881,
    ...citys_209_1882,
    ...citys_209_1883,
    ...citys_209_1884,
];
