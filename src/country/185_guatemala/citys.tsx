import { citys as citys_185_1676 } from "./1676_alta_verapaz/citys";
import { citys as citys_185_1677 } from "./1677_baja_verapaz/citys";
import { citys as citys_185_1678 } from "./1678_chimaltenango/citys";
import { citys as citys_185_1679 } from "./1679_chiquimula/citys";
import { citys as citys_185_1680 } from "./1680_el_progreso/citys";
import { citys as citys_185_1681 } from "./1681_escuintla/citys";
import { citys as citys_185_1682 } from "./1682_guatemala/citys";
import { citys as citys_185_1683 } from "./1683_huehuetenango/citys";
import { citys as citys_185_1684 } from "./1684_izabal/citys";
import { citys as citys_185_1685 } from "./1685_jalapa/citys";
import { citys as citys_185_1686 } from "./1686_jutiapa/citys";
import { citys as citys_185_1687 } from "./1687_peten/citys";
import { citys as citys_185_1688 } from "./1688_quetzaltenango/citys";
import { citys as citys_185_1689 } from "./1689_quiche/citys";
import { citys as citys_185_1690 } from "./1690_retalhuleu/citys";
import { citys as citys_185_1691 } from "./1691_sacatepequez/citys";
import { citys as citys_185_1692 } from "./1692_san_marcos/citys";
import { citys as citys_185_1693 } from "./1693_santa_rosa/citys";
import { citys as citys_185_1694 } from "./1694_solola/citys";
import { citys as citys_185_1695 } from "./1695_suchitepequez/citys";
import { citys as citys_185_1696 } from "./1696_totonicapan/citys";
import { citys as citys_185_1697 } from "./1697_zacapa/citys";
export const citys = [
    ...citys_185_1676,
    ...citys_185_1677,
    ...citys_185_1678,
    ...citys_185_1679,
    ...citys_185_1680,
    ...citys_185_1681,
    ...citys_185_1682,
    ...citys_185_1683,
    ...citys_185_1684,
    ...citys_185_1685,
    ...citys_185_1686,
    ...citys_185_1687,
    ...citys_185_1688,
    ...citys_185_1689,
    ...citys_185_1690,
    ...citys_185_1691,
    ...citys_185_1692,
    ...citys_185_1693,
    ...citys_185_1694,
    ...citys_185_1695,
    ...citys_185_1696,
    ...citys_185_1697,
];
