import { citys as citys_59_227 } from "./227_bartin/citys";
import { citys as citys_59_228 } from "./228_bayburt/citys";
import { citys as citys_59_229 } from "./229_karabuk/citys";
import { citys as citys_59_230 } from "./230_adana/citys";
import { citys as citys_59_231 } from "./231_aydin/citys";
import { citys as citys_59_232 } from "./232_amasya/citys";
import { citys as citys_59_233 } from "./233_ankara/citys";
import { citys as citys_59_234 } from "./234_antalya/citys";
import { citys as citys_59_235 } from "./235_artvin/citys";
import { citys as citys_59_236 } from "./236_afion/citys";
import { citys as citys_59_237 } from "./237_balikesir/citys";
import { citys as citys_59_238 } from "./238_bilecik/citys";
import { citys as citys_59_239 } from "./239_bursa/citys";
import { citys as citys_59_240 } from "./240_gaziantep/citys";
import { citys as citys_59_241 } from "./241_denizli/citys";
import { citys as citys_59_242 } from "./242_izmir/citys";
import { citys as citys_59_243 } from "./243_isparta/citys";
import { citys as citys_59_244 } from "./244_icel/citys";
import { citys as citys_59_245 } from "./245_kayseri/citys";
import { citys as citys_59_246 } from "./246_kars/citys";
import { citys as citys_59_247 } from "./247_kodjaeli/citys";
import { citys as citys_59_248 } from "./248_konya/citys";
import { citys as citys_59_249 } from "./249_kirklareli/citys";
import { citys as citys_59_250 } from "./250_kutahya/citys";
import { citys as citys_59_251 } from "./251_malatya/citys";
import { citys as citys_59_252 } from "./252_manisa/citys";
import { citys as citys_59_253 } from "./253_sakarya/citys";
import { citys as citys_59_254 } from "./254_samsun/citys";
import { citys as citys_59_255 } from "./255_sivas/citys";
import { citys as citys_59_256 } from "./256_istanbul/citys";
import { citys as citys_59_257 } from "./257_trabzon/citys";
import { citys as citys_59_258 } from "./258_corum/citys";
import { citys as citys_59_259 } from "./259_edirne/citys";
import { citys as citys_59_260 } from "./260_elazig/citys";
import { citys as citys_59_261 } from "./261_erzincan/citys";
import { citys as citys_59_262 } from "./262_erzurum/citys";
import { citys as citys_59_263 } from "./263_eskisehir/citys";
export const citys = [
    ...citys_59_227,
    ...citys_59_228,
    ...citys_59_229,
    ...citys_59_230,
    ...citys_59_231,
    ...citys_59_232,
    ...citys_59_233,
    ...citys_59_234,
    ...citys_59_235,
    ...citys_59_236,
    ...citys_59_237,
    ...citys_59_238,
    ...citys_59_239,
    ...citys_59_240,
    ...citys_59_241,
    ...citys_59_242,
    ...citys_59_243,
    ...citys_59_244,
    ...citys_59_245,
    ...citys_59_246,
    ...citys_59_247,
    ...citys_59_248,
    ...citys_59_249,
    ...citys_59_250,
    ...citys_59_251,
    ...citys_59_252,
    ...citys_59_253,
    ...citys_59_254,
    ...citys_59_255,
    ...citys_59_256,
    ...citys_59_257,
    ...citys_59_258,
    ...citys_59_259,
    ...citys_59_260,
    ...citys_59_261,
    ...citys_59_262,
    ...citys_59_263,
];
