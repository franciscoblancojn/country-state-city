import { citys as citys_34_104 } from "./104_issik_kulskaya_region/citys";
import { citys as citys_34_105 } from "./105_kyrgyzstan/citys";
import { citys as citys_34_106 } from "./106_narinskaya_region/citys";
import { citys as citys_34_107 } from "./107_oshskaya_region/citys";
import { citys as citys_34_108 } from "./108_tallaskaya_region/citys";
export const citys = [
    ...citys_34_104,
    ...citys_34_105,
    ...citys_34_106,
    ...citys_34_107,
    ...citys_34_108,
];
