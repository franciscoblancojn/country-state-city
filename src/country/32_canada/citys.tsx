import { citys as citys_32_2047 } from "./2047_newfoundland/citys";
import { citys as citys_32_2048 } from "./2048_nova_scotia/citys";
import { citys as citys_32_2049 } from "./2049_prince_edward_island/citys";
import { citys as citys_32_2050 } from "./2050_new_brunswick/citys";
import { citys as citys_32_2051 } from "./2051_quebec/citys";
import { citys as citys_32_2052 } from "./2052_ontario/citys";
import { citys as citys_32_2053 } from "./2053_manitoba/citys";
import { citys as citys_32_2054 } from "./2054_saskatchewan/citys";
import { citys as citys_32_2055 } from "./2055_alberta/citys";
import { citys as citys_32_2056 } from "./2056_british_columbia/citys";
import { citys as citys_32_2057 } from "./2057_nunavut/citys";
import { citys as citys_32_2058 } from "./2058_northwest_territories/citys";
import { citys as citys_32_2059 } from "./2059_yukon_territory/citys";
export const citys = [
    ...citys_32_2047,
    ...citys_32_2048,
    ...citys_32_2049,
    ...citys_32_2050,
    ...citys_32_2051,
    ...citys_32_2052,
    ...citys_32_2053,
    ...citys_32_2054,
    ...citys_32_2055,
    ...citys_32_2056,
    ...citys_32_2057,
    ...citys_32_2058,
    ...citys_32_2059,
];
