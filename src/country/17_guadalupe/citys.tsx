import { citys as citys_17_26 } from "./26_grande-terre/citys";
import { citys as citys_17_27 } from "./27_basse-terre/citys";
export const citys = [...citys_17_26, ...citys_17_27];
