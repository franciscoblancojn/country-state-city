import { citys as citys_111_1642 } from "./1642_artigas/citys";
import { citys as citys_111_1643 } from "./1643_canelones/citys";
import { citys as citys_111_1644 } from "./1644_cerro_largo/citys";
import { citys as citys_111_1645 } from "./1645_colonia/citys";
import { citys as citys_111_1646 } from "./1646_durazno/citys";
import { citys as citys_111_1647 } from "./1647_flores/citys";
import { citys as citys_111_1648 } from "./1648_florida/citys";
import { citys as citys_111_1649 } from "./1649_lavalleja/citys";
import { citys as citys_111_1650 } from "./1650_maldonado/citys";
import { citys as citys_111_1651 } from "./1651_montevideo/citys";
import { citys as citys_111_1652 } from "./1652_paysandu/citys";
import { citys as citys_111_1653 } from "./1653_rio_negro/citys";
import { citys as citys_111_1654 } from "./1654_rivera/citys";
import { citys as citys_111_1655 } from "./1655_rocha/citys";
import { citys as citys_111_1656 } from "./1656_salto/citys";
import { citys as citys_111_1657 } from "./1657_san_jose/citys";
import { citys as citys_111_1658 } from "./1658_soriano/citys";
import { citys as citys_111_1659 } from "./1659_tacuarembo/citys";
import { citys as citys_111_1660 } from "./1660_treinta_y_tres/citys";
export const citys = [
    ...citys_111_1642,
    ...citys_111_1643,
    ...citys_111_1644,
    ...citys_111_1645,
    ...citys_111_1646,
    ...citys_111_1647,
    ...citys_111_1648,
    ...citys_111_1649,
    ...citys_111_1650,
    ...citys_111_1651,
    ...citys_111_1652,
    ...citys_111_1653,
    ...citys_111_1654,
    ...citys_111_1655,
    ...citys_111_1656,
    ...citys_111_1657,
    ...citys_111_1658,
    ...citys_111_1659,
    ...citys_111_1660,
];
