import { citys as citys_69_305 } from "./305_cheju/citys";
import { citys as citys_69_306 } from "./306_chollabuk/citys";
import { citys as citys_69_307 } from "./307_chollanam/citys";
import { citys as citys_69_308 } from "./308_chungcheongbuk/citys";
import { citys as citys_69_309 } from "./309_chungcheongnam/citys";
import { citys as citys_69_310 } from "./310_incheon/citys";
import { citys as citys_69_311 } from "./311_kangweon/citys";
import { citys as citys_69_312 } from "./312_kwangju/citys";
import { citys as citys_69_313 } from "./313_kyeonggi/citys";
import { citys as citys_69_314 } from "./314_kyeongsangbuk/citys";
import { citys as citys_69_315 } from "./315_kyeongsangnam/citys";
import { citys as citys_69_316 } from "./316_pusan/citys";
import { citys as citys_69_317 } from "./317_seoul/citys";
import { citys as citys_69_318 } from "./318_taegu/citys";
import { citys as citys_69_319 } from "./319_taejeon/citys";
import { citys as citys_69_320 } from "./320_ulsan/citys";
import { citys as citys_69_404 } from "./404_south_korea/citys";
import { citys as citys_69_2188 } from "./2188_busan/citys";
import { citys as citys_69_2189 } from "./2189_daegu/citys";
import { citys as citys_69_2191 } from "./2191_gangwon/citys";
import { citys as citys_69_2192 } from "./2192_gwangju/citys";
import { citys as citys_69_2193 } from "./2193_gyeonggi/citys";
import { citys as citys_69_2194 } from "./2194_gyeongsangbuk/citys";
import { citys as citys_69_2195 } from "./2195_gyeongsangnam/citys";
import { citys as citys_69_2196 } from "./2196_jeju/citys";
export const citys = [
    ...citys_69_305,
    ...citys_69_306,
    ...citys_69_307,
    ...citys_69_308,
    ...citys_69_309,
    ...citys_69_310,
    ...citys_69_311,
    ...citys_69_312,
    ...citys_69_313,
    ...citys_69_314,
    ...citys_69_315,
    ...citys_69_316,
    ...citys_69_317,
    ...citys_69_318,
    ...citys_69_319,
    ...citys_69_320,
    ...citys_69_404,
    ...citys_69_2188,
    ...citys_69_2189,
    ...citys_69_2191,
    ...citys_69_2192,
    ...citys_69_2193,
    ...citys_69_2194,
    ...citys_69_2195,
    ...citys_69_2196,
];
