import { citys as citys_140_390 } from "./390_baghdad/citys";
import { citys as citys_140_391 } from "./391_basra/citys";
import { citys as citys_140_392 } from "./392_mosul/citys";
export const citys = [...citys_140_390, ...citys_140_391, ...citys_140_392];
