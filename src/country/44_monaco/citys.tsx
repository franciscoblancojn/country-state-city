import { citys as citys_44_1292 } from "./1292_la_condamine/citys";
import { citys as citys_44_1293 } from "./1293_monaco/citys";
import { citys as citys_44_1294 } from "./1294_monte-carlo/citys";
export const citys = [...citys_44_1292, ...citys_44_1293, ...citys_44_1294];
