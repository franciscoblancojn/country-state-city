import { citys as citys_57_221 } from "./221_ashgabat_region/citys";
import { citys as citys_57_222 } from "./222_krasnovodsk_region/citys";
import { citys as citys_57_223 } from "./223_mary_region/citys";
import { citys as citys_57_224 } from "./224_tashauz_region/citys";
import { citys as citys_57_225 } from "./225_chardzhou_region/citys";
export const citys = [
    ...citys_57_221,
    ...citys_57_222,
    ...citys_57_223,
    ...citys_57_224,
    ...citys_57_225,
];
