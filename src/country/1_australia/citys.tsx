import { citys as citys_1_2010 } from "./2010_australian_capital_territory/citys";
import { citys as citys_1_2011 } from "./2011_new_south_wales/citys";
import { citys as citys_1_2012 } from "./2012_northern_territory/citys";
import { citys as citys_1_2013 } from "./2013_queensland/citys";
import { citys as citys_1_2014 } from "./2014_south_australia/citys";
import { citys as citys_1_2015 } from "./2015_tasmania/citys";
import { citys as citys_1_2016 } from "./2016_victoria/citys";
import { citys as citys_1_2017 } from "./2017_western_australia/citys";
export const citys = [
    ...citys_1_2010,
    ...citys_1_2011,
    ...citys_1_2012,
    ...citys_1_2013,
    ...citys_1_2014,
    ...citys_1_2015,
    ...citys_1_2016,
    ...citys_1_2017,
];
