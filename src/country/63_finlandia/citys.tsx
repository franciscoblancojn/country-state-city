import { citys as citys_63_933 } from "./933_ð•land/citys";
import { citys as citys_63_934 } from "./934_lapland/citys";
import { citys as citys_63_935 } from "./935_oulu/citys";
import { citys as citys_63_936 } from "./936_southern_finland/citys";
import { citys as citys_63_937 } from "./937_eastern_finland/citys";
import { citys as citys_63_938 } from "./938_western_finland/citys";
export const citys = [
    ...citys_63_933,
    ...citys_63_934,
    ...citys_63_935,
    ...citys_63_936,
    ...citys_63_937,
    ...citys_63_938,
];
