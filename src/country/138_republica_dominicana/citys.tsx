import { citys as citys_138_387 } from "./387_santo_domingo/citys";
import { citys as citys_138_2073 } from "./2073_duarte/citys";
import { citys as citys_138_2074 } from "./2074_puerto_plata/citys";
import { citys as citys_138_2075 } from "./2075_valverde/citys";
import { citys as citys_138_2076 } from "./2076_maria_trinidad_sanchez/citys";
import { citys as citys_138_2077 } from "./2077_azua/citys";
import { citys as citys_138_2078 } from "./2078_santiago/citys";
import { citys as citys_138_2079 } from "./2079_san_cristobal/citys";
import { citys as citys_138_2080 } from "./2080_peravia/citys";
import { citys as citys_138_2081 } from "./2081_elias_pina/citys";
import { citys as citys_138_2082 } from "./2082_barahona/citys";
import { citys as citys_138_2083 } from "./2083_monte_plata/citys";
import { citys as citys_138_2084 } from "./2084_salcedo/citys";
import { citys as citys_138_2085 } from "./2085_la_altagracia/citys";
import { citys as citys_138_2086 } from "./2086_san_juan/citys";
import { citys as citys_138_2087 } from "./2087_monsenor_nouel/citys";
import { citys as citys_138_2088 } from "./2088_monte_cristi/citys";
import { citys as citys_138_2089 } from "./2089_espaillat/citys";
import { citys as citys_138_2090 } from "./2090_sanchez_ramirez/citys";
import { citys as citys_138_2091 } from "./2091_la_vega/citys";
import { citys as citys_138_2092 } from "./2092_san_pedro_de_macoris/citys";
import { citys as citys_138_2093 } from "./2093_independencia/citys";
import { citys as citys_138_2094 } from "./2094_dajabon/citys";
import { citys as citys_138_2095 } from "./2095_baoruco/citys";
import { citys as citys_138_2096 } from "./2096_el_seibo/citys";
import { citys as citys_138_2097 } from "./2097_hato_mayor/citys";
import { citys as citys_138_2098 } from "./2098_la_romana/citys";
import { citys as citys_138_2099 } from "./2099_pedernales/citys";
import { citys as citys_138_2100 } from "./2100_samana/citys";
import { citys as citys_138_2101 } from "./2101_santiago_rodriguez/citys";
import { citys as citys_138_2102 } from "./2102_san_jose_de_ocoa/citys";
export const citys = [
    ...citys_138_387,
    ...citys_138_2073,
    ...citys_138_2074,
    ...citys_138_2075,
    ...citys_138_2076,
    ...citys_138_2077,
    ...citys_138_2078,
    ...citys_138_2079,
    ...citys_138_2080,
    ...citys_138_2081,
    ...citys_138_2082,
    ...citys_138_2083,
    ...citys_138_2084,
    ...citys_138_2085,
    ...citys_138_2086,
    ...citys_138_2087,
    ...citys_138_2088,
    ...citys_138_2089,
    ...citys_138_2090,
    ...citys_138_2091,
    ...citys_138_2092,
    ...citys_138_2093,
    ...citys_138_2094,
    ...citys_138_2095,
    ...citys_138_2096,
    ...citys_138_2097,
    ...citys_138_2098,
    ...citys_138_2099,
    ...citys_138_2100,
    ...citys_138_2101,
    ...citys_138_2102,
];
