import { citys as citys_22_940 } from "./940_arhus/citys";
import { citys as citys_22_941 } from "./941_bornholm/citys";
import { citys as citys_22_942 } from "./942_frederiksborg/citys";
import { citys as citys_22_943 } from "./943_fyn/citys";
import { citys as citys_22_944 } from "./944_kobenhavn/citys";
import { citys as citys_22_945 } from "./945_staden_kobenhavn/citys";
import { citys as citys_22_946 } from "./946_nordjylland/citys";
import { citys as citys_22_947 } from "./947_ribe/citys";
import { citys as citys_22_948 } from "./948_ringkobing/citys";
import { citys as citys_22_949 } from "./949_roskilde/citys";
import { citys as citys_22_950 } from "./950_sonderjylland/citys";
import { citys as citys_22_951 } from "./951_storstrom/citys";
import { citys as citys_22_952 } from "./952_vejle/citys";
import { citys as citys_22_953 } from "./953_vestsjalland/citys";
import { citys as citys_22_954 } from "./954_viborg/citys";
export const citys = [
    ...citys_22_940,
    ...citys_22_941,
    ...citys_22_942,
    ...citys_22_943,
    ...citys_22_944,
    ...citys_22_945,
    ...citys_22_946,
    ...citys_22_947,
    ...citys_22_948,
    ...citys_22_949,
    ...citys_22_950,
    ...citys_22_951,
    ...citys_22_952,
    ...citys_22_953,
    ...citys_22_954,
];
