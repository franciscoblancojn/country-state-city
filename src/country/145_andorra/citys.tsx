import { citys as citys_145_984 } from "./984_canillo/citys";
import { citys as citys_145_985 } from "./985_encamp/citys";
import { citys as citys_145_986 } from "./986_la_massana/citys";
import { citys as citys_145_987 } from "./987_ordino/citys";
import { citys as citys_145_988 } from "./988_sant_julia_de_loria/citys";
import { citys as citys_145_989 } from "./989_andorra_la_vella/citys";
import { citys as citys_145_990 } from "./990_escaldes-engordany/citys";
export const citys = [
    ...citys_145_984,
    ...citys_145_985,
    ...citys_145_986,
    ...citys_145_987,
    ...citys_145_988,
    ...citys_145_989,
    ...citys_145_990,
];
