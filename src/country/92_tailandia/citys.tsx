import { citys as citys_92_398 } from "./398_thailand/citys";
import { citys as citys_92_2112 } from "./2112_amnat_charoen/citys";
import { citys as citys_92_2113 } from "./2113_ang_thong/citys";
import { citys as citys_92_2114 } from "./2114_bangkok/citys";
import { citys as citys_92_2115 } from "./2115_buri_ram/citys";
import { citys as citys_92_2116 } from "./2116_chachoengsao/citys";
import { citys as citys_92_2117 } from "./2117_chai_nat/citys";
import { citys as citys_92_2118 } from "./2118_chaiyaphum/citys";
import { citys as citys_92_2119 } from "./2119_chanthaburi/citys";
import { citys as citys_92_2120 } from "./2120_chiang_mai/citys";
import { citys as citys_92_2121 } from "./2121_chiang_rai/citys";
import { citys as citys_92_2122 } from "./2122_chon_buri/citys";
import { citys as citys_92_2124 } from "./2124_kalasin/citys";
import { citys as citys_92_2126 } from "./2126_kanchanaburi/citys";
import { citys as citys_92_2127 } from "./2127_khon_kaen/citys";
import { citys as citys_92_2128 } from "./2128_krabi/citys";
import { citys as citys_92_2129 } from "./2129_lampang/citys";
import { citys as citys_92_2131 } from "./2131_loei/citys";
import { citys as citys_92_2132 } from "./2132_lop_buri/citys";
import { citys as citys_92_2133 } from "./2133_mae_hong_son/citys";
import { citys as citys_92_2134 } from "./2134_maha_sarakham/citys";
import { citys as citys_92_2137 } from "./2137_nakhon_pathom/citys";
import { citys as citys_92_2139 } from "./2139_nakhon_ratchasima/citys";
import { citys as citys_92_2140 } from "./2140_nakhon_sawan/citys";
import { citys as citys_92_2141 } from "./2141_nakhon_si_thammarat/citys";
import { citys as citys_92_2143 } from "./2143_narathiwat/citys";
import { citys as citys_92_2144 } from "./2144_nong_bua_lam_phu/citys";
import { citys as citys_92_2145 } from "./2145_nong_khai/citys";
import { citys as citys_92_2146 } from "./2146_nonthaburi/citys";
import { citys as citys_92_2147 } from "./2147_pathum_thani/citys";
import { citys as citys_92_2148 } from "./2148_pattani/citys";
import { citys as citys_92_2149 } from "./2149_phangnga/citys";
import { citys as citys_92_2150 } from "./2150_phatthalung/citys";
import { citys as citys_92_2154 } from "./2154_phichit/citys";
import { citys as citys_92_2155 } from "./2155_phitsanulok/citys";
import { citys as citys_92_2156 } from "./2156_phra_nakhon_si_ayutthaya/citys";
import { citys as citys_92_2157 } from "./2157_phrae/citys";
import { citys as citys_92_2158 } from "./2158_phuket/citys";
import { citys as citys_92_2159 } from "./2159_prachin_buri/citys";
import { citys as citys_92_2160 } from "./2160_prachuap_khiri_khan/citys";
import { citys as citys_92_2162 } from "./2162_ratchaburi/citys";
import { citys as citys_92_2163 } from "./2163_rayong/citys";
import { citys as citys_92_2164 } from "./2164_roi_et/citys";
import { citys as citys_92_2165 } from "./2165_sa_kaeo/citys";
import { citys as citys_92_2166 } from "./2166_sakon_nakhon/citys";
import { citys as citys_92_2167 } from "./2167_samut_prakan/citys";
import { citys as citys_92_2168 } from "./2168_samut_sakhon/citys";
import { citys as citys_92_2169 } from "./2169_samut_songkhran/citys";
import { citys as citys_92_2170 } from "./2170_saraburi/citys";
import { citys as citys_92_2172 } from "./2172_si_sa_ket/citys";
import { citys as citys_92_2173 } from "./2173_sing_buri/citys";
import { citys as citys_92_2174 } from "./2174_songkhla/citys";
import { citys as citys_92_2175 } from "./2175_sukhothai/citys";
import { citys as citys_92_2176 } from "./2176_suphan_buri/citys";
import { citys as citys_92_2177 } from "./2177_surat_thani/citys";
import { citys as citys_92_2178 } from "./2178_surin/citys";
import { citys as citys_92_2180 } from "./2180_trang/citys";
import { citys as citys_92_2182 } from "./2182_ubon_ratchathani/citys";
import { citys as citys_92_2183 } from "./2183_udon_thani/citys";
import { citys as citys_92_2184 } from "./2184_uthai_thani/citys";
import { citys as citys_92_2185 } from "./2185_uttaradit/citys";
import { citys as citys_92_2186 } from "./2186_yala/citys";
import { citys as citys_92_2187 } from "./2187_yasothon/citys";
export const citys = [
    ...citys_92_398,
    ...citys_92_2112,
    ...citys_92_2113,
    ...citys_92_2114,
    ...citys_92_2115,
    ...citys_92_2116,
    ...citys_92_2117,
    ...citys_92_2118,
    ...citys_92_2119,
    ...citys_92_2120,
    ...citys_92_2121,
    ...citys_92_2122,
    ...citys_92_2124,
    ...citys_92_2126,
    ...citys_92_2127,
    ...citys_92_2128,
    ...citys_92_2129,
    ...citys_92_2131,
    ...citys_92_2132,
    ...citys_92_2133,
    ...citys_92_2134,
    ...citys_92_2137,
    ...citys_92_2139,
    ...citys_92_2140,
    ...citys_92_2141,
    ...citys_92_2143,
    ...citys_92_2144,
    ...citys_92_2145,
    ...citys_92_2146,
    ...citys_92_2147,
    ...citys_92_2148,
    ...citys_92_2149,
    ...citys_92_2150,
    ...citys_92_2154,
    ...citys_92_2155,
    ...citys_92_2156,
    ...citys_92_2157,
    ...citys_92_2158,
    ...citys_92_2159,
    ...citys_92_2160,
    ...citys_92_2162,
    ...citys_92_2163,
    ...citys_92_2164,
    ...citys_92_2165,
    ...citys_92_2166,
    ...citys_92_2167,
    ...citys_92_2168,
    ...citys_92_2169,
    ...citys_92_2170,
    ...citys_92_2172,
    ...citys_92_2173,
    ...citys_92_2174,
    ...citys_92_2175,
    ...citys_92_2176,
    ...citys_92_2177,
    ...citys_92_2178,
    ...citys_92_2180,
    ...citys_92_2182,
    ...citys_92_2183,
    ...citys_92_2184,
    ...citys_92_2185,
    ...citys_92_2186,
    ...citys_92_2187,
];
