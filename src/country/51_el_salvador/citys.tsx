import { citys as citys_51_206 } from "./206_ahuachapan/citys";
import { citys as citys_51_207 } from "./207_cuscatlan/citys";
import { citys as citys_51_208 } from "./208_la_libertad/citys";
import { citys as citys_51_209 } from "./209_la_paz/citys";
import { citys as citys_51_210 } from "./210_la_union/citys";
import { citys as citys_51_211 } from "./211_san_miguel/citys";
import { citys as citys_51_212 } from "./212_san_salvador/citys";
import { citys as citys_51_213 } from "./213_santa_ana/citys";
import { citys as citys_51_214 } from "./214_sonsonate/citys";
export const citys = [
    ...citys_51_206,
    ...citys_51_207,
    ...citys_51_208,
    ...citys_51_209,
    ...citys_51_210,
    ...citys_51_211,
    ...citys_51_212,
    ...citys_51_213,
    ...citys_51_214,
];
