import { citys as citys_23_32 } from "./32_al_qa„a�hira/citys";
import { citys as citys_23_33 } from "./33_aswan/citys";
import { citys as citys_23_34 } from "./34_asyut/citys";
import { citys as citys_23_35 } from "./35_beni_suef/citys";
import { citys as citys_23_36 } from "./36_gharbia/citys";
import { citys as citys_23_37 } from "./37_damietta/citys";
import { citys as citys_23_377 } from "./377_egypt/citys";
import { citys as citys_23_389 } from "./389_sinai/citys";
export const citys = [
    ...citys_23_32,
    ...citys_23_33,
    ...citys_23_34,
    ...citys_23_35,
    ...citys_23_36,
    ...citys_23_37,
    ...citys_23_377,
    ...citys_23_389,
];
