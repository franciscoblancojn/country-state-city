import { citys as citys_82_1699 } from "./1699_amazonas/citys";
import { citys as citys_82_1700 } from "./1700_antioquia/citys";
import { citys as citys_82_1701 } from "./1701_arauca/citys";
import { citys as citys_82_1702 } from "./1702_atlantico/citys";
import { citys as citys_82_1703 } from "./1703_caqueta/citys";
import { citys as citys_82_1704 } from "./1704_cauca/citys";
import { citys as citys_82_1705 } from "./1705_cesar/citys";
import { citys as citys_82_1706 } from "./1706_choco/citys";
import { citys as citys_82_1707 } from "./1707_cordoba/citys";
import { citys as citys_82_1708 } from "./1708_guaviare/citys";
import { citys as citys_82_1709 } from "./1709_guainia/citys";
import { citys as citys_82_1710 } from "./1710_huila/citys";
import { citys as citys_82_1711 } from "./1711_la_guajira/citys";
import { citys as citys_82_1712 } from "./1712_meta/citys";
import { citys as citys_82_1713 } from "./1713_narino/citys";
import { citys as citys_82_1714 } from "./1714_norte_de_santander/citys";
import { citys as citys_82_1715 } from "./1715_putumayo/citys";
import { citys as citys_82_1716 } from "./1716_quindio/citys";
import { citys as citys_82_1717 } from "./1717_risaralda/citys";
import { citys as citys_82_1718 } from "./1718_san_andres_y_providencia/citys";
import { citys as citys_82_1719 } from "./1719_santander/citys";
import { citys as citys_82_1720 } from "./1720_sucre/citys";
import { citys as citys_82_1721 } from "./1721_tolima/citys";
import { citys as citys_82_1722 } from "./1722_valle_del_cauca/citys";
import { citys as citys_82_1723 } from "./1723_vaupes/citys";
import { citys as citys_82_1724 } from "./1724_vichada/citys";
import { citys as citys_82_1725 } from "./1725_casanare/citys";
import { citys as citys_82_1726 } from "./1726_cundinamarca/citys";
import { citys as citys_82_1727 } from "./1727_distrito_capital/citys";
import { citys as citys_82_1730 } from "./1730_caldas/citys";
import { citys as citys_82_1731 } from "./1731_magdalena/citys";
import { citys as citys_82_2202 } from "./2202_bolivar/citys";
import { citys as citys_82_2203 } from "./2203_boyaca/citys";
export const citys = [
    ...citys_82_1699,
    ...citys_82_1700,
    ...citys_82_1701,
    ...citys_82_1702,
    ...citys_82_1703,
    ...citys_82_1704,
    ...citys_82_1705,
    ...citys_82_1706,
    ...citys_82_1707,
    ...citys_82_1708,
    ...citys_82_1709,
    ...citys_82_1710,
    ...citys_82_1711,
    ...citys_82_1712,
    ...citys_82_1713,
    ...citys_82_1714,
    ...citys_82_1715,
    ...citys_82_1716,
    ...citys_82_1717,
    ...citys_82_1718,
    ...citys_82_1719,
    ...citys_82_1720,
    ...citys_82_1721,
    ...citys_82_1722,
    ...citys_82_1723,
    ...citys_82_1724,
    ...citys_82_1725,
    ...citys_82_1726,
    ...citys_82_1727,
    ...citys_82_1730,
    ...citys_82_1731,
    ...citys_82_2202,
    ...citys_82_2203,
];
