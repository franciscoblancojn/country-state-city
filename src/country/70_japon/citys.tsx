import { citys as citys_70_321 } from "./321_aichi/citys";
import { citys as citys_70_322 } from "./322_akita/citys";
import { citys as citys_70_323 } from "./323_aomori/citys";
import { citys as citys_70_324 } from "./324_wakayama/citys";
import { citys as citys_70_325 } from "./325_gifu/citys";
import { citys as citys_70_326 } from "./326_gunma/citys";
import { citys as citys_70_327 } from "./327_ibaraki/citys";
import { citys as citys_70_328 } from "./328_iwate/citys";
import { citys as citys_70_329 } from "./329_ishikawa/citys";
import { citys as citys_70_330 } from "./330_kagawa/citys";
import { citys as citys_70_331 } from "./331_kagoshima/citys";
import { citys as citys_70_332 } from "./332_kanagawa/citys";
import { citys as citys_70_333 } from "./333_kyoto/citys";
import { citys as citys_70_334 } from "./334_kochi/citys";
import { citys as citys_70_335 } from "./335_kumamoto/citys";
import { citys as citys_70_336 } from "./336_mie/citys";
import { citys as citys_70_337 } from "./337_miyagi/citys";
import { citys as citys_70_338 } from "./338_miyazaki/citys";
import { citys as citys_70_339 } from "./339_nagano/citys";
import { citys as citys_70_340 } from "./340_nagasaki/citys";
import { citys as citys_70_341 } from "./341_nara/citys";
import { citys as citys_70_342 } from "./342_niigata/citys";
import { citys as citys_70_343 } from "./343_okayama/citys";
import { citys as citys_70_344 } from "./344_okinawa/citys";
import { citys as citys_70_345 } from "./345_osaka/citys";
import { citys as citys_70_346 } from "./346_saga/citys";
import { citys as citys_70_347 } from "./347_saitama/citys";
import { citys as citys_70_348 } from "./348_shiga/citys";
import { citys as citys_70_349 } from "./349_shizuoka/citys";
import { citys as citys_70_350 } from "./350_shimane/citys";
import { citys as citys_70_351 } from "./351_tiba/citys";
import { citys as citys_70_352 } from "./352_tokyo/citys";
import { citys as citys_70_353 } from "./353_tokushima/citys";
import { citys as citys_70_354 } from "./354_tochigi/citys";
import { citys as citys_70_355 } from "./355_tottori/citys";
import { citys as citys_70_356 } from "./356_toyama/citys";
import { citys as citys_70_357 } from "./357_fukui/citys";
import { citys as citys_70_358 } from "./358_fukuoka/citys";
import { citys as citys_70_359 } from "./359_fukushima/citys";
import { citys as citys_70_360 } from "./360_hiroshima/citys";
import { citys as citys_70_361 } from "./361_hokkaido/citys";
import { citys as citys_70_362 } from "./362_hyogo/citys";
import { citys as citys_70_363 } from "./363_yoshimi/citys";
import { citys as citys_70_364 } from "./364_yamagata/citys";
import { citys as citys_70_365 } from "./365_yamaguchi/citys";
import { citys as citys_70_366 } from "./366_yamanashi/citys";
import { citys as citys_70_2103 } from "./2103_chiba/citys";
import { citys as citys_70_2104 } from "./2104_ehime/citys";
import { citys as citys_70_2105 } from "./2105_oita/citys";
export const citys = [
    ...citys_70_321,
    ...citys_70_322,
    ...citys_70_323,
    ...citys_70_324,
    ...citys_70_325,
    ...citys_70_326,
    ...citys_70_327,
    ...citys_70_328,
    ...citys_70_329,
    ...citys_70_330,
    ...citys_70_331,
    ...citys_70_332,
    ...citys_70_333,
    ...citys_70_334,
    ...citys_70_335,
    ...citys_70_336,
    ...citys_70_337,
    ...citys_70_338,
    ...citys_70_339,
    ...citys_70_340,
    ...citys_70_341,
    ...citys_70_342,
    ...citys_70_343,
    ...citys_70_344,
    ...citys_70_345,
    ...citys_70_346,
    ...citys_70_347,
    ...citys_70_348,
    ...citys_70_349,
    ...citys_70_350,
    ...citys_70_351,
    ...citys_70_352,
    ...citys_70_353,
    ...citys_70_354,
    ...citys_70_355,
    ...citys_70_356,
    ...citys_70_357,
    ...citys_70_358,
    ...citys_70_359,
    ...citys_70_360,
    ...citys_70_361,
    ...citys_70_362,
    ...citys_70_363,
    ...citys_70_364,
    ...citys_70_365,
    ...citys_70_366,
    ...citys_70_2103,
    ...citys_70_2104,
    ...citys_70_2105,
];
