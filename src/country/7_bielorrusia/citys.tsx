import { citys as citys_7_5 } from "./5_brestskaya_obl./citys";
import { citys as citys_7_6 } from "./6_vitebskaya_obl./citys";
import { citys as citys_7_7 } from "./7_gomelskaya_obl./citys";
import { citys as citys_7_8 } from "./8_grodnenskaya_obl./citys";
import { citys as citys_7_9 } from "./9_minskaya_obl./citys";
import { citys as citys_7_10 } from "./10_mogilevskaya_obl./citys";
export const citys = [
    ...citys_7_5,
    ...citys_7_6,
    ...citys_7_7,
    ...citys_7_8,
    ...citys_7_9,
    ...citys_7_10,
];
