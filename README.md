# country-state-city-nextjs

This is a library for get all countrys, states and citys of world.

-   [Installing](#installing)
-   [Import](#import)
-   [Developer](#developer)
-   [Repositories](#repositories)

## Installing

Using npm:

```bash
npm i country-state-city-nextjs
```

## Import

```javascript
import {
    load,
    countryProps,
    stateProps,
    cityProps,
    loadCountrys,
    loadStates,
    loadCitys,
    loadCountrysWidthImg,
    loadStatesByCountry,
    loadCitysByStateAndCountry,
} from "country-state-city-nextjs/cjs/index";
```

## Developer

[Francisco Blanco](https://franciscoblanco.vercel.app/)

[Gitlab franciscoblancojn](https://gitlab.com/franciscoblancojn)

[Email blancofrancisco34@gmail.com](mailto:blancofrancisco34@gmail.com)

## Repositories

-   [Gitlab](https://gitlab.com/franciscoblancojn/country-state-city)
